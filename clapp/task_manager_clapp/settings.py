"""This module provides the functions to work with the app settings """

import configparser
import os
import uuid

from task_manager_lib.logger import get_logger
from task_manager_lib import exceptions


APP_DIR_DEFAULT_PATH = '~/.TaskManager'
APP_SETTINGS_DEFAULT_PATH = '~/.TaskManager/settings.ini'
APP_LOGGING_CONFIG_DEFAULT_PATH = '~/.TaskManager/logging_config.ini'

DEFAULT_SETTINGS_NAME = 'settings.ini'
LOGGING_LEVELS = ['DEBUG', 'INFO', 'WARN', 'WARNING', 'ERROR', 'CRITICAL']


def create_app_data_directory(app_dir_path=None):
    """ Creates the app data directory by dir_path in the system if it doesn't exist.
    If dir_path wasn't passed, the APP_DIR_DEFAULT_PATH is used.
    Returns the system path to it

    Keyword arguments:
    app_dir_path -- the path where to create the app data directory (by default, '~/.TaskManager')
    """

    if app_dir_path is None:
        app_dir_path = APP_DIR_DEFAULT_PATH

    # Create app dir if not exists
    project_directory_full_path = os.path.expanduser(app_dir_path)

    if not os.path.exists(project_directory_full_path):
        os.mkdir(project_directory_full_path)

    return project_directory_full_path


def add_data_storage_path(app_data_dir_path=None, settings_file_path=None):
    """ Adds the data storage path to the settings configuration.
    Creates the settings file if it doesn't exist

    Keyword arguments:
    app_data_dir_path -- the path where to store the data (by default, '~/.TaskManager')
    settings_file_path -- the path to the settings file (by default, '~/.TaskManager/settings.ini')

    Raises:
         PathNotFoundError -- when the storage path isn't valid
    """

    if app_data_dir_path is None:
        app_data_dir_path = APP_DIR_DEFAULT_PATH

    if settings_file_path is None:
        # Create the default app settings
        app_dir_path = create_app_data_directory()
        settings_file_path = os.path.join(app_dir_path, DEFAULT_SETTINGS_NAME)

    storage_path = os.path.expanduser(app_data_dir_path)

    if not os.path.exists(storage_path):
        raise exceptions.PathNotFoundError("The path '{}' doesn't exist".format(storage_path))

    config = configparser.RawConfigParser()
    config.read(settings_file_path)

    config['DEFAULT'] = {'DataPath': storage_path}

    with open(settings_file_path, 'w') as settings_file:
        config.write(settings_file)


def add_cur_user_id(user_id, settings_file_path=None):
    """ Adds current user id to the settings. Creates the settings file
    if it doesn't exist

    Keyword arguments:
    user_id -- current user id
    settings_file_path -- the path to the settings file (by default, '~/.TaskManager/settings.ini')

    Raises:
         TypeError -- when the user id isn't valid
    """

    if not (isinstance(user_id, uuid.UUID) or user_id is None):
        raise TypeError("{} is not valid id".format(user_id))

    if settings_file_path is None:
        # Create the default app settings
        app_dir_path = create_app_data_directory()
        settings_file_path = os.path.join(app_dir_path, DEFAULT_SETTINGS_NAME)

    config = configparser.RawConfigParser()

    config.read(settings_file_path)

    config['CURRENT'] = {'User': str(user_id)}

    with open(settings_file_path, 'w') as settings_file:
            config.write(settings_file)


def change_logger_level(level, settings_file_path=None):
    """Changes or adds (if the level doesn't exist) the logging level in the settings.
    Creates the settings file if it doesn't exist

    Keyword arguments:
    user_id -- current user id
    settings_file_path -- the path to the settings file (by default, '~/.TaskManager/settings.ini')

    Raises:
         NotValidParameterError -- when the level isn't valid
    """
    if settings_file_path is None:
        # Create the default app settings
        app_dir_path = create_app_data_directory()
        settings_file_path = os.path.join(app_dir_path, DEFAULT_SETTINGS_NAME)

    level = level.upper()

    if level not in LOGGING_LEVELS:
        raise exceptions.NotValidParameterError(level, "level")

    logger = get_logger()
    logger.setLevel(level)

    config = configparser.RawConfigParser()

    config.read(settings_file_path)

    config['LOGGER'] = {'level': level}

    with open(settings_file_path, 'w') as settings_file:
        config.write(settings_file)


def get_storage_path(settings_file_path=None):
    """ Returns path to the data storage directory

    Keyword arguments:
    settings_file_path -- the path to the settings file (by default, '~/.TaskManager/settings.ini')

    Raises:
        KeyError -- when there's no '[DEFAULT] DataPath' in the settings
        PathNotFoundError -- when the default data path from the settings isn't valid
        SettingsNotFoundError -- when the settings file doesn't exist
    """

    if settings_file_path is None:
        settings_file_path = os.path.expanduser(APP_SETTINGS_DEFAULT_PATH)

    if not os.path.exists(settings_file_path):
        raise exceptions.SettingsNotFoundError()

    # Read settings and return default path if it exists
    config = configparser.ConfigParser()

    config.read(settings_file_path)
    if 'DEFAULT' in config and 'DataPath' in config['DEFAULT']:
        default_data_path = config['DEFAULT']['DataPath']
        if os.path.exists(default_data_path):
            return default_data_path
        else:
            raise exceptions.PathNotFoundError("Not valid data path in the settings")

    else:
        raise KeyError("'[DEFAULT] DataPath' doesn't exist in the settings")


def get_cur_user_id(settings_file_path=None):
    """ Returns current user id

    Keyword arguments:
    settings_file_path -- the path to the settings file (by default, '~/.TaskManager/settings.ini')

    Raises:
        KeyError -- when there's no '[CURRENT] User' in the settings
        SettingsNotFoundError -- when the settings file doesn't exist
        ValueError -- when the current user id from the settings isn't valid
    """

    if settings_file_path is None:
        settings_file_path = os.path.expanduser(APP_SETTINGS_DEFAULT_PATH)

    # Reads the settings if they exist
    if not os.path.isfile(settings_file_path):
        raise exceptions.SettingsNotFoundError()

    config = configparser.ConfigParser()
    config.read(settings_file_path)

    # If current user is in the settings and is not None, return the user id
    if 'CURRENT' in config and 'User' in config['CURRENT']:
        current_user_id = config['CURRENT']['User']
        if current_user_id != "None":
            try:
                return uuid.UUID(current_user_id)
            except (ValueError, TypeError):
                raise ValueError("Not valid current user id in the settings")
    else:
        raise KeyError("'[CURRENT] User' doesn't exist in the settings")


def get_logging_level(settings_file_path=None):
    """ Returns the name of the app logger level from the settings

    Keyword arguments:
    settings_file_path -- the path to the settings file (by default, '~/.TaskManager/settings.ini')

    Raises:
        KeyError -- when there's no '[LOGGER] level' in the settings
        NotValidParameterError -- when the logging level from the settings isn't valid
        SettingsNotFoundError -- when the settings file doesn't exist
    """

    if settings_file_path is None:
        settings_file_path = os.path.expanduser(APP_SETTINGS_DEFAULT_PATH)

    # Reads the settings if they exist
    if not os.path.isfile(settings_file_path):
        raise exceptions.SettingsNotFoundError()

    config = configparser.ConfigParser()
    config.read(settings_file_path)

    # If logger level is in the settings and is valid, return the level
    if 'LOGGER' in config and 'level' in config['LOGGER']:
        level = config['LOGGER']['level']

        if level not in LOGGING_LEVELS:
            raise exceptions.NotValidParameterError(level, "level")

        return level

    raise KeyError("'[LOGGER] level' doesn't exist in the settings")
