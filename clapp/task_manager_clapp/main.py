import logging
import os
from datetime import datetime

from task_manager_clapp.commands.handlers.main import CommandsHandler
from task_manager_clapp.commands.parser import get_parser
from task_manager_clapp.settings import create_app_data_directory, get_storage_path, get_logging_level
from task_manager_lib.exceptions import PathNotFoundError, SettingsNotFoundError
from task_manager_lib.logger import get_logger

APP_DATA_DIR = '~/.TaskManager'
APP_SETTINGS_PATH = '~/.TaskManager/settings.ini'
LOGGER_DIR_PATH = '~/.TaskManager/Logger'


def set_default_settings(app_data_dir=None, settings_file_path=None):
    if app_data_dir is None:
        app_data_dir = APP_DATA_DIR

    create_app_data_directory(app_data_dir)

    if settings_file_path is None:
        settings_file_path = APP_SETTINGS_PATH

    settings_file = os.path.expanduser(settings_file_path)

    try:
        _ = get_storage_path(settings_file)

    except SettingsNotFoundError:
        # Create empty settings file
        settings = os.path.expanduser(settings_file)
        with open(settings, 'w'):
            pass
        print("The settings weren't found. An empty settings file was created by '{}'\n".format(settings_file))

    except (KeyError, PathNotFoundError):
        pass


def set_logger(log_dir=None, log_file_name=None):
    if log_dir is None:
        log_dir = LOGGER_DIR_PATH

    log_dir = os.path.expanduser(log_dir)

    if not os.path.exists(log_dir):
        os.mkdir(log_dir)

    if log_file_name is None:
        log_file_name = str(datetime.now().date())

    log_file = os.path.join(log_dir, log_file_name)

    logger = get_logger()

    # Get logging level from the settings and set it
    level = get_logging_level()
    logger.setLevel(level)

    # Create console handler
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)

    # Create console formatter
    console_formatter = logging.Formatter('%(asctime)s | %(levelname)s : %(message)s', datefmt='%d-%m-%y %H:%M:%S')
    console_handler.setFormatter(console_formatter)

    # Create file handler
    file_handler = logging.FileHandler(log_file)
    file_handler.setLevel(logging.DEBUG)

    # Create file formatter
    file_formatter = logging.Formatter('%(asctime)s | module: %(name)s ; function: %(funcName)s | %(levelname)s : '
                                       '%(message)s', datefmt='%d-%m-%y %H:%M:%S')
    file_handler.setFormatter(file_formatter)

    # Add handlers to logger
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)


def main():
    set_default_settings()
    set_logger()

    parser = get_parser()
    args_dict = vars(parser.parse_args())

    handler = CommandsHandler(args_dict)
    handler.handle_actions()


if __name__ == "__main__":
    main()
