def show_full_user_info(user):
    print('\n-----------------------------------------------------------')

    print("ID: {}\nLogin: {}\nEmail: {}".format(user.id, user.login, user.email))
    name = user.first_name + " " + user.last_name
    if name != " ":
        print("User name: {}".format(name))
    if user.date_of_birth is not None:
        print("The date of birth: {}".format(user.date_of_birth))

    print('-----------------------------------------------------------\n')


def show_short_info(user):
    print("{} (email: {}, id: {})".format(user.login, user.email, user.id))
