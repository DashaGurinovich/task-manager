from task_manager_lib.func.connector import DataConnector
from task_manager_clapp.commands.printers import task, user


def show_full_group(group, storage):
    print('\n-----------------------------------------------------------')

    print("ID: {}\nName: {}".format(group.id, group.name))

    obj_getter = DataConnector(storage)

    users = obj_getter.get_group_users(group)
    print("\nUsers: ")
    for u in users:
        user.show_short_info(u)

    tasks = obj_getter.get_group_tasks(group.id)
    print("\nTasks: ")
    if tasks:
        for t in tasks:
            task.show_short_info(t)
    else:
        print("No tasks")

    print('-----------------------------------------------------------\n')


def show_short_info(group):
    print("{} (id: {})".format(group.name, group.id))
