from task_manager_lib.entities.task import Status
from task_manager_lib.func.connector import DataConnector
from task_manager_clapp.commands.printers import group, user
from task_manager_lib.func.task import TaskManager


def show_full_task_info(task, storage):
    print('\n-----------------------------------------------------------')

    if task.status == Status.FINISHED:
        print("---COMPLETED---")

    obj_getter = DataConnector(storage)

    if task.is_blocked:
        print("!!!BLOCKED!!!")
        blocking_task = obj_getter.storage.find_task_by_id(task.parent)
        print("BLOCKING TASK:")
        show_short_info(blocking_task)
        print()

    elif task.parent is not None:
        print("!!!SUB TASK!!!")
        main_task = obj_getter.storage.find_task_by_id(task.parent)
        print("MAIN TASK:")
        show_short_info(main_task)
        print()

    print("ID: {}".format(task.id))
    print("Name: {}\nThe date of creation: {}\nStatus: {}\nPriority: {}\n".
          format(task.name, task.creation_date, task.status.name, task.priority.name))

    show_percentage_bar(task.completion_percentage)

    if task.definition != "":
        print("\nThe definition: {}".format(task.definition))

    if task.finishing_date is not None:
        print("The date of finishing: {}".format(task.finishing_date))

    if task.deadline is not None:
        print("The deadline: {}".format(task.deadline))

    if task.period_start_date:
        print("\nThe period start date: {}".format(task.period_start_date))

        if task.period is not None:
            print_period(task.period)

            if task.period_end_date is not None:
                print("The period end date: {}".format(task.period_end_date))

            nearest_dates = TaskManager.get_nearest_periodic_task_dates(task)

            print("\nTask nearest periodic dates:")
            if len(nearest_dates) == 0:
                print("There's no periodic tasks until deadline")
            else:
                for date in nearest_dates:
                    print(date)

    users = obj_getter.get_task_users(task)
    if users:
        print("\nUsers:")
        for u in users:
            user.show_short_info(u)

    groups = obj_getter.get_task_groups(task)
    if groups:
        print("\nGroups:")
        for g in groups:
            group.show_short_info(g)

    sub_task_tree = TaskManager(storage).get_task_tree(task.id)
    if len(sub_task_tree) > 1:
        print("\nSub tasks:")
        for sub_task_vertex in sub_task_tree:
            if sub_task_vertex[1] != task.id:
                sub_task = storage.find_task_by_id(sub_task_vertex[1])
                show_short_info(sub_task, tab=sub_task_vertex[0])

    dependent_tasks = obj_getter.get_task_dependent_tasks(task.id)
    if dependent_tasks:
        print("\nDependent task ids:")
        for dependent_task in dependent_tasks:
            show_short_info(dependent_task)

    print('-----------------------------------------------------------\n')


def print_period(period):
    days = period.days - period.weeks * 7

    print("The period: {} years, {} months, {} weeks, {} days, {} hours, {} minutes"
          .format(period.years, period.months, period.weeks, days, period.hours, period.minutes))


def show_short_info(task, tab=0):
    print("    " * tab + "{} (id: {}, status: {}, priority: {}, percentage: {})".format(task.name, task.id,
                                                                                        task.status.name,
                                                                                        task.priority.name,
                                                                                        task.completion_percentage))


def show_percentage_bar(percentage):
    filled_bar = round(percentage * 0.5)
    print("The percentage of completion:")
    print('\r', '#' * filled_bar + '~' * (50 - filled_bar), '[{}%]'.format(percentage))
