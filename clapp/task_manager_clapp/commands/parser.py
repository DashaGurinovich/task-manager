import argparse
import sys


class TaskManagerParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


def get_parser():
    parser = TaskManagerParser(description="Task manager is a programme for helping you not to forget "
                                           "important (or maybe not so important) things to do. "
                                           "Have fun exploring!")
    subparsers = parser.add_subparsers(help='The objects with which some actions can be done', dest='object_name')
    subparsers.required = True

    add_task_parser(subparsers)
    add_user_parser(subparsers)
    add_group_parser(subparsers)
    add_settings_parser(subparsers)

    return parser


# region The task object parsers
def add_task_parser(subparsers):
    task_parser = subparsers.add_parser('task', help='Performs actions with tasks')
    task_parser_subparsers = task_parser.add_subparsers(help='The actions that can be performed',
                                                        dest='action_name')
    task_parser_subparsers.required = True

    add_task_creation_parser(task_parser_subparsers)
    add_task_view_parser(task_parser_subparsers)
    add_task_change_parser(task_parser_subparsers)
    add_task_adding_parser(task_parser_subparsers)
    add_task_deletion_parser(task_parser_subparsers)


def add_task_creation_parser(task_parser_subparsers):
    task_creation_parser = task_parser_subparsers.add_parser('create', help='Creates a task with the written '
                                                                            'parameters')
    task_creation_parser.add_argument('name', type=str, help='Task name')
    task_creation_parser.add_argument('-g', '--group', type=str, help='Task group id', metavar='')
    task_creation_parser.add_argument('-d', '--deadline', type=str, help='Task deadline date in H-M d-m-Y or d-m-Y '
                                                                         'format', metavar='')
    task_creation_parser.add_argument('-s', '--status', type=str, help='Task status (number or name, for example, '
                                                                       'created)', metavar='')
    task_creation_parser.add_argument('-p', '--priority', type=str, help='Task priority (number or name, for example, '
                                                                         'average)', metavar='')


def add_task_view_parser(task_parser_subparsers):
    task_view_parser = task_parser_subparsers.add_parser('show', help='Shows all tasks info or the tasks that meets '
                                                                      'the written params')
    task_view_parser.add_argument('--id', type=str, help='The id of the task to show info of', metavar='')
    task_view_parser.add_argument('-s', '--status', type=str, help='Tasks status (number or name, for example, '
                                                                   'created)', metavar='')
    task_view_parser.add_argument('-p', '--priority', type=str, help='Tasks priority (number or name, for example, '
                                                                     'average)', metavar='')
    task_view_parser.add_argument('-b', '--blocking', type=str, help='Task blocking (True or False)', metavar='')
    task_view_parser.add_argument('-g', '--group', type=str, help='Task group id', metavar='')
    task_view_parser.add_argument('-n', '--name', type=str, help='Task name', metavar='')
    task_view_parser.add_argument('--creation-date', type=str, help='Task creation date in H-M d-m-Y '
                                                                    'or d-m-Y format', metavar='')
    task_view_parser.add_argument('-d', '--deadline', type=str, help='Task deadline date in H-M d-m-Y or d-m-Y format',
                                  metavar='')
    task_view_parser.add_argument('--finishing-date', type=str, help='Task finishing date in H-M d-m-Y '
                                                                     'or d-m-Y format', metavar='')
    task_view_parser.add_argument('--percentage', type=str, help='Task percentage of completion', metavar='')


def add_task_change_parser(task_parser_subparsers):
    task_change_parser = task_parser_subparsers.add_parser('change', help='Changes that task info that id and user '
                                                                          'login is written afterwards')
    task_change_parser.add_argument('id', type=str, help='The id of task to be changed')
    task_change_parser.add_argument('-n', '--name', type=str, help='New task name', metavar='')
    task_change_parser.add_argument('--definition', type=str, help='New task definition', metavar='')
    task_change_parser.add_argument('-d', '--deadline', type=str, help='New task deadline date in H-M d-m-Y or d-m-Y '
                                                                       'format', metavar='')
    task_change_parser.add_argument('-s', '--status', type=str, help='New task status (number or name, for example, '
                                                                     'created)', metavar='')
    task_change_parser.add_argument('--percentage', type=str, help='New task percentage of completion', metavar='')
    task_change_parser.add_argument('-p', '--priority', type=str, help='New task priority (number or name, for example,'
                                                                       ' average)', metavar='')

    task_change_subparsers = task_change_parser.add_subparsers(help='The parameters that can be changed',
                                                               dest='parameter_name')
    period_parser = task_change_subparsers.add_parser('period', help="Task period")

    period_parser.add_argument('--period-start-date', type=str, help='New date of the task period start in '
                                                                     'H-M d-m-Y or d-m-Y format', metavar='')
    period_parser.add_argument('--period-end-date', type=str, help='New date of the task period end in H-M d-m-Y '
                                                                   'or d-m-Y format', metavar='')
    add_period_parser(period_parser)


def add_period_parser(period_parser):
    period_arguments = period_parser.add_argument_group('task period arguments')
    period_arguments.add_argument('--minutes', type=int, help='Task period in minutes', metavar='')
    period_arguments.add_argument('--hours', type=int, help='Task period in hours', metavar='')
    period_arguments.add_argument('-d', '--days', type=int, help='Task period in days', metavar='')
    period_arguments.add_argument('-w', '--weeks', type=int, help='Task period in weeks', metavar='')
    period_arguments.add_argument('-m, --months', type=int, help='Task period in months', metavar='')
    period_arguments.add_argument('-y, --years', type=int, help='Task period in years', metavar='')


def add_task_adding_parser(task_parser_subparsers):
    task_adding_parser = task_parser_subparsers.add_parser('add', help='Adds different objects to task lists or adds '
                                                                       'period to the task')
    task_adding_parser.add_argument('id', type=str, help='The id of the task to add something to')

    task_adding_subparsers = task_adding_parser.add_subparsers(help='The objects that can be added to the task',
                                                               dest='parameter_name')

    dependent_task_parser = task_adding_subparsers.add_parser('dependent-task', help="Task that depends on current "
                                                                                     "task")
    sub_task_parser = task_adding_subparsers.add_parser('sub-task', help="Add sub task to the current task")
    period_parser = task_adding_subparsers.add_parser('period', help="Make the chosen task a periodic")
    user_parser = task_adding_subparsers.add_parser('user', help='The user to be added to the task')
    group_parser = task_adding_subparsers.add_parser('group', help='The group to be added to the task')

    add_arguments_for_task_parsers(dependent_task_parser)
    add_arguments_for_task_parsers(sub_task_parser)

    add_period_parser(period_parser)
    period_parser.add_argument('period-start-date', type=str, help='New date of the task period start in '
                                                                   'H-M d-m-Y or d-m-Y format')
    period_parser.add_argument('--period-end-date', type=str, help='New date of the task period end in H-M d-m-Y '
                                                                   'or d-m-Y format', metavar='')

    group_parser.add_argument('group_id', type=str, help='The id of the group to be added to the task')
    user_parser.add_argument('login', type=str, help='The login of the user to be added to the task')


def add_arguments_for_task_parsers(task_parser):
    task_parser.add_argument('name', type=str, help='Task name')
    task_parser.add_argument('-g', '--group', type=str, help='Task group id', metavar='')
    task_parser.add_argument('-d', '--deadline', type=str, help='Task deadline date in H-M d-m-Y or d-m-Y format',
                             metavar='')
    task_parser.add_argument('-s', '--status', type=str, help='Task status (number or name, for example, '
                                                              'created)', metavar='')
    task_parser.add_argument('-p', '--priority', type=str, help='Task priority (number or name, for example, '
                                                                'average)', metavar='')


def add_task_deletion_parser(task_parser_subparsers):
    task_deletion_parser = task_parser_subparsers.add_parser('delete', help='Deletes the written task parameters'
                                                                            ' or the whole task if nothing is written')
    task_deletion_parser.add_argument('id', type=str, help='The id of the task to delete something (or to be'
                                                           ' deleted if nothing is written afterwards')
    task_deletion_parser.add_argument('-g', '--group', type=str, help='The id of the group to be deleted ',
                                      metavar='')
    task_deletion_parser.add_argument('-u', '--user', type=str, help='The login of the user to be deleted ',
                                      metavar='')

    task_deletion_subparsers = task_deletion_parser.add_subparsers(help='The parameters that can be deleted',
                                                                   dest='parameter_name')
    task_deletion_subparsers.add_parser('deadline', help="Task deadline")
    task_deletion_subparsers.add_parser('definition', help="Task definition")
    task_deletion_subparsers.add_parser('period_start_date', help="Task period start date")
    task_deletion_subparsers.add_parser('period', help="Task period")
    task_deletion_subparsers.add_parser('period_end_date', help="Task period end date")

# endregion


# region The user objects parsers
def add_user_parser(subparsers):
    user_parser = subparsers.add_parser('user', help='Performs actions with users')
    user_parser_subparsers = user_parser.add_subparsers(help='The actions that can be performed', dest='action_name')

    user_parser_subparsers.required = True

    add_user_creation_parser(user_parser_subparsers)
    add_user_view_parser(user_parser_subparsers)
    add_user_change_parser(user_parser_subparsers)
    add_user_deletion_parser(user_parser_subparsers)
    add_user_authentication_parser(user_parser_subparsers)
    add_user_log_off_parser(user_parser_subparsers)


def add_user_creation_parser(user_parser_subparsers):
    user_creation_parser = user_parser_subparsers.add_parser('create', help="Creates a user with the written login,"
                                                                            " and email if user with such "
                                                                            "login or email doesn't exist")
    user_creation_parser.add_argument('login', type=str, help='User login')
    user_creation_parser.add_argument('email', type=str, help='User email')
    user_creation_parser.add_argument('--first-name', type=str, help='User first name', metavar='')
    user_creation_parser.add_argument('--last-name', type=str, help='User last name', metavar='')
    user_creation_parser.add_argument('--date-of-birth', type=str, help='User date of birth in d-m-Y format',
                                      metavar='')


def add_user_view_parser(user_parser_subparsers):
    user_view_parser = user_parser_subparsers.add_parser('show',
                                                         help='Shows all users info or a single user info if '
                                                              'the user id, login or email is written')
    user_view_parser.add_argument('--id', type=str, help='The id of the user to show info of', metavar='')
    user_view_parser.add_argument('-l', '--login', type=str, help='The login of the user to show info of', metavar='')
    user_view_parser.add_argument('-e', '--email', type=str, help='The email of the user to show info of', metavar='')
    user_view_parser.add_argument('--first-name', type=str, help='User first name', metavar='')
    user_view_parser.add_argument('--full-name', type=str, help='User full name', metavar='')
    user_view_parser.add_argument('--last-name', type=str, help='User last name', metavar='')
    user_view_parser.add_argument('--date-of-birth', type=str, help='User date of birth in d-m-Y format', metavar='')


def add_user_change_parser(user_parser_subparsers):
    user_change_parser = user_parser_subparsers.add_parser('change', help='Changes that user info whose login is '
                                                                          'written afterwards')
    user_change_parser.add_argument('old-login', type=str, help='User login')
    user_change_parser.add_argument('-l', '--login', type=str, help='New user login (login should be unique)',
                                    metavar='')
    user_change_parser.add_argument('-e', '--email', type=str, help='New user email (email should be unique)',
                                    metavar='')
    user_change_parser.add_argument('--first-name', type=str, help='New user first name', metavar='')
    user_change_parser.add_argument('--last-name', type=str, help='New user last name', metavar='')
    user_change_parser.add_argument('--date-of-birth', type=str, help='New user date of birth in d-m-Y format',
                                    metavar='')


def add_user_deletion_parser(user_parser_subparsers):
    user_deletion_parser = user_parser_subparsers.add_parser('delete', help='Deletes written user parameters'
                                                                            ' or whole user if nothing is written')
    user_deletion_parser.add_argument('login', type=str, help='The login of the user to delete something (or to be'
                                                              ' deleted if nothing is written afterwards')
    user_deletion_subparsers = user_deletion_parser.add_subparsers(help='The parameters that can be deleted',
                                                                   dest='parameter_name')
    user_deletion_subparsers.add_parser('first_name', help="User first name")
    user_deletion_subparsers.add_parser('last_name', help="User last name")
    user_deletion_subparsers.add_parser('date_of_birth', help="User date of birth")


def add_user_authentication_parser(user_parser_subparsers):
    user_creation_parser = user_parser_subparsers.add_parser('authenticate', help="Authenticates the user if the "
                                                                                  "written login is correct")
    user_creation_parser.add_argument('login', type=str, help='User login')


def add_user_log_off_parser(user_parser_subparsers):
    user_creation_parser = user_parser_subparsers.add_parser('log off', help="Logs off the user if the written login is"
                                                                             " correct")
    user_creation_parser.add_argument('login', type=str, help='User login')
# endregion


# region The group objects parsers
def add_group_parser(subparsers):
    group_parser = subparsers.add_parser('group', help='Performs actions with groups')
    group_parser_subparsers = group_parser.add_subparsers(help='The actions that can be performed',
                                                          dest='action_name')
    group_parser_subparsers.required = True

    add_group_creation_parser(group_parser_subparsers)
    add_group_view_parser(group_parser_subparsers)
    add_group_change_parser(group_parser_subparsers)
    add_group_adding_parser(group_parser_subparsers)
    add_group_deletion_parser(group_parser_subparsers)


def add_group_creation_parser(group_parser_subparsers):
    group_creation_parser = group_parser_subparsers.add_parser('create', help="Creates a group with the written name")
    group_creation_parser.add_argument('name', type=str, help='Group name')


def add_group_view_parser(group_parser_subparsers):
    group_view_parser = group_parser_subparsers.add_parser('show', help='Shows all current user groups info or the '
                                                                        'groups that meet the written params')
    group_view_parser.add_argument('--id', type=str, help='The id of the group to show info of', metavar='')
    group_view_parser.add_argument('-n', '--name', type=str, help='Group name ', metavar='')


def add_group_change_parser(group_parser_subparsers):
    group_change_parser = group_parser_subparsers.add_parser('change', help="Changes that group info that id and user"
                                                                            " login is written afterwards")
    group_change_parser.add_argument('id', type=str, help='The id of group to be changed')
    group_change_parser.add_argument('-n', '--name', type=str, help='New group name', metavar='')


def add_group_adding_parser(group_parser_subparsers):
    group_adding_parser = group_parser_subparsers.add_parser('add', help='Add different objects to group lists')
    group_adding_parser.add_argument('id', type=str, help='The id of the group to add something to')
    group_adding_parser.add_argument('-u', '--user', type=str, help='The login of the user to be added '
                                                                    'to the group')


def add_group_deletion_parser(group_parser_subparsers):
    group_deletion_parser = group_parser_subparsers.add_parser('delete', help='Deletes written group parameters'
                                                                              ' or whole group if nothing is written')
    group_deletion_parser.add_argument('id', type=str, help='The id of the group to delete something (or to be'
                                                            ' deleted if nothing is written afterwards')
    group_deletion_parser.add_argument('-u', '--user', type=str, help='The login of the user to be deleted '
                                                                      'from the group')
# endregion


# region The settings parser
def add_settings_parser(subparsers):
    settings_parser = subparsers.add_parser('settings', help='Performs actions with the settings')
    settings_parser_subparsers = settings_parser.add_subparsers(help='The actions that can be performed',
                                                                dest='action_name')
    settings_parser_subparsers.required = True

    add_settings_change_parser(settings_parser_subparsers)
    add_settings_view_parser(settings_parser_subparsers)


def add_settings_change_parser(settings_parser_subparsers):
    settings_change_parser = settings_parser_subparsers.add_parser('add', help='Changes the written params to the '
                                                                               'settings')

    settings_change_parser.add_argument('-d', '--data-path', type=str, help='Sets the default data storage path',
                                        metavar='')

    settings_change_parser.add_argument('-l', '--level', type=str, help='Sets the logging level', metavar='')


def add_settings_view_parser(settings_parser_subparsers):
    settings_view_parser = settings_parser_subparsers.add_parser('show', help='Shows the settings params')

    settings_view_subparsers = settings_view_parser.add_subparsers(help='The parameters that can be shown',
                                                                   dest='parameter_name')

    settings_view_subparsers.add_parser('data-path', help='Shows the default data storage path')
    settings_view_subparsers.add_parser('level', help='Shows the logging level')
    settings_view_subparsers.add_parser('cur-user', help='Shows the current user id')
# endregion
