from datetime import datetime

from task_manager_clapp import authentication
from task_manager_clapp.commands.printers import user as user_view
from task_manager_clapp.settings import add_cur_user_id, get_cur_user_id
from task_manager_lib import exceptions
from task_manager_lib.func import filter
from task_manager_lib.func.user import UserManager


MISSING_DATE = datetime.min


def handle_user_ex(func):
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)

        except exceptions.NotFoundObjectError:
            print("The user with such login, email or id doesn't exist")

        except exceptions.ExistingUserError:
            print("The user with such login or email already exists. The user wasn't created or changed")

        except (ValueError, exceptions.NotValidParameterError):
            print("Not valid date of birth. The user wasn't created or changed")

        except Exception as e:
            print(e)

    return wrapper


class UserCommandsHandler:
    def __init__(self, args_dict, storage):
        self.user_params = args_dict
        self.storage = storage
        self.user_manager = UserManager(storage)

    def handle_actions(self):
        action_name = self.user_params['action_name']

        user_actions = {
            "authenticate": self.handle_authentication,
            "change": self.handle_change,
            "create": self.handle_creation,
            "delete": self.handle_deletion,
            "log off": self.handle_log_off,
            "show": self.handle_view
        }

        if action_name in user_actions:
            user_actions[action_name]()

    @handle_user_ex
    def handle_creation(self):
        new_user = self.user_manager.create_user(login=self.user_params['login'],
                                                 email=self.user_params['email'],
                                                 first_name=self.user_params['first_name'],
                                                 last_name=self.user_params['last_name'],
                                                 date_of_birth=self.user_params['date_of_birth'])

        print('\nThe new user was successfully created')
        user_view.show_full_user_info(new_user)

    @handle_user_ex
    def handle_view(self):
        users = self.storage.get_users_from_db()

        if len(users) == 0:
            print("There's no users in database")
            return

        if self.user_params['id'] is not None:
            user = self.user_manager.storage.find_user_by_id(self.user_params['id'])
            user_view.show_full_user_info(user)
            return

        view_actions = {
            "date_of_birth": filter.get_users_with_date_of_birth,
            "email": filter.get_users_with_email,
            "first_name": filter.get_users_with_first_name,
            "full_name": filter.get_users_with_full_name,
            "last_name": filter.get_users_with_last_name,
            "login": filter.get_users_with_login
        }

        was_filtered = False
        filtered_users = users
        for param in self.user_params:
            if param in view_actions and self.user_params[param] is not None:
                filtered_users = view_actions[param](filtered_users, self.user_params[param])
                was_filtered = True

        if not was_filtered:
            for u in users:
                user_view.show_full_user_info(u)
            return

        if len(filtered_users) == 0:
            print("\nThere're no users with such criteria")

        else:
            for u in filtered_users:
                user_view.show_full_user_info(u)

    @handle_user_ex
    def handle_change(self):
        changing_user = self.user_manager.storage.find_user_by_login(self.user_params['old-login'])

        date_of_birth = self.user_params["date_of_birth"]
        if date_of_birth is None:
            date_of_birth = MISSING_DATE

        changed_user = self.user_manager.change_user(user_id=changing_user.id,
                                                     date_of_birth=date_of_birth,
                                                     email=self.user_params["email"],
                                                     first_name=self.user_params["first_name"],
                                                     last_name=self.user_params["last_name"],
                                                     login=self.user_params["login"])

        user_view.show_full_user_info(changed_user)

    @handle_user_ex
    def handle_deletion(self):
        user = self.user_manager.storage.find_user_by_login(self.user_params['login'])

        deletion_object = self.user_params['parameter_name']

        if deletion_object is not None:

            deletion_param_value = {
                "date_of_birth": None,
                "first_name": "",
                "last_name": ""
            }

            changed_user = self.user_manager.change_user(user.id,
                                                         **{deletion_object: deletion_param_value[deletion_object]})
            user_view.show_full_user_info(changed_user)
            return

        self.user_manager.delete_user(user.id)
        print('The user was successfully deleted')

        try:
            cur_user_id = get_cur_user_id()

            if cur_user_id == user.id:
                add_cur_user_id(None)

                print("The deleted user was the current user of the app. So to continue "
                      "working authenticate other user")

        except (KeyError, ValueError):
            pass

    def handle_authentication(self):
        user_params = self.user_params
        authentication.authenticate_user(login=user_params['login'], storage=self.storage)

    def handle_log_off(self):
        user_params = self.user_params
        authentication.exit_user_profile(login=user_params['login'], storage=self.storage)
