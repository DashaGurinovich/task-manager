from task_manager_clapp.commands.handlers.group import GroupCommandsHandler
from task_manager_clapp.commands.handlers.settings import SettingsCommandsHandler
from task_manager_clapp.commands.handlers.task import TaskCommandsHandler
from task_manager_clapp.commands.handlers.user import UserCommandsHandler
from task_manager_clapp.settings import get_storage_path
from task_manager_lib.db.json.manager import JsonDataManager
from task_manager_lib.db.manager import DataManager
from task_manager_lib.exceptions import PathNotFoundError

SETTINGS = "settings"


class CommandsHandler:
    def __init__(self, args_dict):
        self.params = args_dict

    def handle_actions(self):
        object_name = self.params['object_name']

        objects_handlers = {
            "group": GroupCommandsHandler,
            "task": TaskCommandsHandler,
            "user": UserCommandsHandler
        }

        if object_name in objects_handlers:

            try:
                storage_path = get_storage_path()
                storage = DataManager(JsonDataManager(storage_path))
                objects_handlers[object_name](self.params, storage).handle_actions()

            except (PathNotFoundError, KeyError) as e:
                print(e, "\n The action wasn't performed cause there's no valid data storage in the settings"
                         "\n Use 'task_manager settings add --data-path ...' command to set a default data "
                         "storage path")

        elif object_name == SETTINGS:
            SettingsCommandsHandler(self.params).handle_actions()
