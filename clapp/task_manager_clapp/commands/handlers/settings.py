from task_manager_lib import exceptions
from task_manager_clapp import settings


class SettingsCommandsHandler:
    def __init__(self, args_dict):
        self.settings_params = args_dict

    def handle_actions(self):
        action_name = self.settings_params['action_name']

        settings_actions = {
            "add": self.handle_addition,
            "show": self.handle_view
        }

        if action_name in settings_actions:
            settings_actions[action_name]()

    def handle_addition(self):
        addition_actions = {
            "data_path": settings.add_data_storage_path,
            "level": settings.change_logger_level,
        }

        for param in addition_actions:
            if param in self.settings_params and self.settings_params[param] is not None:
                try:
                    addition_actions[param](self.settings_params[param])
                    print("The action was completed successfully")

                except exceptions.PathNotFoundError:
                    print("'{}' is not valid path".format(self.settings_params[param]))

                except (exceptions.LoggerConfigNotFoundError, KeyError, exceptions.NotValidParameterError) as e:
                    print(e)

    def handle_view(self):
        view_object = self.settings_params['parameter_name']
        view_actions = {
            "cur-user": settings.get_cur_user_id,
            "data-path": settings.get_storage_path,
            "level": settings.get_logging_level
        }

        if view_object in view_actions:
            try:
                param = view_actions[view_object]()
                print(param)

            except (KeyError, exceptions.PathNotFoundError, exceptions.SettingsNotFoundError,
                    exceptions.NotValidParameterError) as e:
                print(e)
