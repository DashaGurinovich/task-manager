from datetime import datetime

from dateutil.relativedelta import relativedelta

from task_manager_clapp.authentication import get_cur_user
from task_manager_clapp.commands.printers import task as task_view
from task_manager_lib import exceptions
from task_manager_lib.func import filter, task as task_func
from task_manager_lib.func.connector import DataConnector

MISSING_DATE = datetime.min
MISSING_PERIOD = relativedelta(minutes=0, hours=0, days=0, weeks=0, months=0, years=0)


def handle_task_ex(func):
    def wrapper(self, *args, **kwargs):

        if self.user_id is None:
            print("There's no current user. Authenticate the user to perform actions")
            return

        try:
            func(self, *args, **kwargs)

        except exceptions.NotFoundObjectError as e:
            if e.object_name == 'Group':
                print("\nThe group with such id wasn't found")

            elif e.object_name == 'User':
                print("\nThe user with such login doesn't exist")

            elif e.object_name == 'Task':
                print("\nThe task with such id doesn't exist")

            print("\nThe task wasn't created or changed!")

        except exceptions.NotValidParameterError as e:
            print(e)
            print("\nThe task wasn't created, filtered or changed!")

        except ValueError:
            print("\nThe tasks weren't filtered")

        except (exceptions.NotExistingPeriodStartDateError, exceptions.NotExistingPeriodError,
                exceptions.NotValidPeriodDatesError):
            print("\nThe task period wasn't added or changed")

        except exceptions.ObjectAlreadyInListError as e:
            if e.object_name == 'User':
                print("\nThe user with such login is already in this task")
            elif e.object_name == 'Group':
                print("\nThe group with such id is already in this task")

        except exceptions.ObjectNotInListError as e:
            if e.object_name == 'User':
                print("\nThe user with such login isn't in the task")
            elif e.object_name == 'Group':
                print("\nThe group with such id isn't in the task")

        except exceptions.BlockedTaskChangeError:
            print("\nThe task wasn't changed 'cause it's blocked")

        except exceptions.PermissionsError:
            print("\nYou can't modify this task")

        except exceptions.OnlyUserDeletionError:
            print("\nThe only user can't be deleted from the task")

        except Exception as e:
            print(e)

    return wrapper


class TaskCommandsHandler:
    def __init__(self, args_dict, storage):
        self.task_params = args_dict
        self.user_id = get_cur_user()
        self.storage = storage
        self.task_manager = task_func.TaskManager(storage)

    def handle_actions(self):
        action_name = self.task_params['action_name']

        task_actions = {
            "add": self.handle_addition,
            "change": self.handle_change,
            "create": self.handle_creation,
            "delete": self.handle_deletion,
            "show": self.handle_view
        }

        if action_name in task_actions:
            task_actions[action_name]()

    @handle_task_ex
    def handle_creation(self):

        new_task = self.task_manager.create_task(name=self.task_params['name'],
                                                 user_id=self.user_id,
                                                 adding_group_id=self.task_params['group'],
                                                 deadline=self.task_params['deadline'],
                                                 status=self.task_params['status'],
                                                 priority=self.task_params['priority'])

        print('\nThe new task was successfully created')
        task_view.show_full_task_info(new_task, self.storage)

    @handle_task_ex
    def handle_view(self):
        if self.task_params['id'] is not None:
            task = self.task_manager.storage.find_task_by_id(self.task_params['id'])
            task_view.show_full_task_info(task, self.storage)
            return

        tasks = DataConnector(self.storage).get_user_tasks(self.user_id)
        if len(tasks) == 0:
            print("\nThere's no tasks for such user in the database")
            return

        view_actions = {
            "blocking": filter.get_tasks_with_blocking,
            "creation_date": filter.get_tasks_with_creation_date,
            "deadline": filter.get_tasks_with_deadline,
            "finishing_date": filter.get_tasks_with_finishing_date,
            "group": filter.get_tasks_with_group_id,
            "name": filter.get_tasks_with_name,
            "percentage": filter.get_tasks_with_completion_percentage,
            "priority": filter.get_tasks_with_priority,
            "status": filter.get_tasks_with_status
        }

        was_filtered = False
        filtered_tasks = tasks
        for param in self.task_params:
            if param in view_actions and self.task_params[param] is not None:
                filtered_tasks = view_actions[param](filtered_tasks, self.task_params[param])
                was_filtered = True

        if not was_filtered:
            for t in tasks:
                task_view.show_full_task_info(t, self.storage)
            return

        if len(filtered_tasks) == 0:
            print("\nThere're no tasks with such criteria")
        else:
            for t in filtered_tasks:
                task_view.show_full_task_info(t, self.storage)

    @handle_task_ex
    def handle_change(self):
        if self.task_params['parameter_name'] is None:

            deadline = self.task_params["deadline"]
            if deadline is None:
                deadline = MISSING_DATE

            task = self.task_manager.change_task(self.task_params['id'], user_id=self.user_id,
                                                 deadline=deadline,
                                                 definition=self.task_params["definition"],
                                                 name=self.task_params["name"],
                                                 percentage=self.task_params["percentage"],
                                                 priority=self.task_params["priority"],
                                                 status=self.task_params["status"])

            task_view.show_full_task_info(task, self.storage)
            return

        if self.task_params['parameter_name'] == 'period':
            period_start_date = self.task_params["period_start_date"]
            if not period_start_date:
                period_start_date = MISSING_DATE

            period = self.get_period()

            period_end_date = self.task_params["period_end_date"]
            if not period_end_date:
                period_end_date = MISSING_DATE

            task = self.task_manager.change_task(self.task_params['id'], user_id=self.user_id,
                                                 period_start_date=period_start_date,
                                                 period=period,
                                                 period_end_date=period_end_date)

            task_view.show_full_task_info(task, self.storage)

    @handle_task_ex
    def handle_addition(self):
        adding_object = self.task_params['parameter_name']

        adding_task_actions = {
            "dependent-task": self.task_manager.create_dependent_task,
            "sub-task": self.task_manager.create_sub_task
        }

        if adding_object == "group" and self.task_params["group_id"] is not None:
            task = self.task_manager.add_group_to_task(self.task_params['id'], self.user_id,
                                                       self.task_params["group_id"])

            task_view.show_full_task_info(task, self.storage)

            return

        if adding_object == "user" and self.task_params["login"] is not None:
            task = self.task_manager.add_user_to_task(self.task_params['id'], self.user_id, self.task_params["login"])

            task_view.show_full_task_info(task, self.storage)

            return

        if adding_object in adding_task_actions:

            new_task = adding_task_actions[adding_object](self.task_params['id'], self.user_id,
                                                          self.task_params['name'],
                                                          self.task_params['group'],
                                                          self.task_params['deadline'],
                                                          self.task_params['status'], self.task_params['priority'])

            task_view.show_full_task_info(new_task, self.storage)
            return

        if adding_object == "period":
            period = self.get_period()

            if period == MISSING_PERIOD:
                print("\nThe task period arguments weren't passed. The period wasn't added")
                return

            period_end_date = self.task_params["period_end_date"]
            if not period_end_date:
                period_end_date = MISSING_DATE

            periodic_task = self.task_manager.make_task_periodic(self.task_params['id'], self.user_id,
                                                                 self.task_params["period-start-date"],
                                                                 period, period_end_date)

            task_view.show_full_task_info(periodic_task, self.storage)
            return

        print("\nNothing was added")

    @handle_task_ex
    def handle_deletion(self):

        deletion_object = self.task_params['parameter_name']
        if deletion_object is not None:

            changed_task = self.task_manager.change_task(self.task_params['id'], user_id=self.user_id,
                                                         **{deletion_object: None})

            task_view.show_full_task_info(changed_task, self.storage)
            return

        deletion_actions = {
            "group": self.task_manager.delete_group_from_task,
            "user": self.task_manager.delete_user_from_task
        }

        param_been_deleted = False
        for param in self.task_params:
            if param in deletion_actions and self.task_params[param] is not None:
                deletion_actions[param](self.task_params['id'], self.user_id, self.task_params[param])
                param_been_deleted = True

        if param_been_deleted:
            task = self.task_manager.storage.find_task_by_id(self.task_params['id'])
            task_view.show_full_task_info(task, self.storage)
            return

        self.task_manager.delete_task(self.task_params['id'], user_id=self.user_id)
        print("\nThe task was successfully deleted")

    def get_period(self):
        period_values = {param: value if value is not None else 0 for param, value in self.task_params.items()}

        period = relativedelta(minutes=period_values["minutes"], hours=period_values["hours"],
                               days=period_values["days"], weeks=period_values["weeks"],
                               months=period_values["m, __months"], years=period_values["y, __years"])

        return period
