from task_manager_lib.func.group import GroupManager
from task_manager_clapp.commands.printers import group as group_view
from task_manager_clapp.authentication import get_cur_user
from task_manager_lib import exceptions
from task_manager_lib.func.connector import DataConnector
from task_manager_lib.func.filter import get_groups_with_name


def handle_group_ex(func):
    def wrapper(self, *args, **kwargs):

        if self.user_id is None:
            print("There's no current user. Authenticate the user to perform actions")
            return

        try:
            func(self, *args, **kwargs)

        except exceptions.NotFoundObjectError as e:
            if e.object_name == 'Group':
                print("\nThe group with such id wasn't found")

            elif e.object_name == 'User':
                print("\nThe user with such login doesn't exist")

        except exceptions.ObjectAlreadyInListError:
            print("\nThe user with such login is already in this group")

        except exceptions.ObjectNotInListError:
            print("\nThe user with such login isn't in the group")

        except exceptions.PermissionsError:
            print("\nYou can't modify this group")

        except exceptions.OnlyUserDeletionError:
            print("\nThe only user can't be deleted from the group")

        except Exception as e:
            print(e)

    return wrapper


class GroupCommandsHandler:
    def __init__(self, args_dict, storage):
        self.group_params = args_dict
        self.storage = storage
        self.user_id = get_cur_user()
        self.group_manager = GroupManager(storage)

    def handle_actions(self):
        action_name = self.group_params['action_name']

        group_actions = {
            "add": self.handle_addition,
            "change": self.handle_change,
            "create": self.handle_creation,
            "delete": self.handle_deletion,
            "show": self.handle_view
        }

        if action_name in group_actions:
            group_actions[action_name]()

    @handle_group_ex
    def handle_creation(self):
        new_group = self.group_manager.create_group(name=self.group_params['name'], user_id=self.user_id)

        print('\nThe new group was successfully created')
        group_view.show_full_group(new_group, self.storage)

    @handle_group_ex
    def handle_view(self):
        if self.group_params['id'] is not None:
            group = self.group_manager.storage.find_group_by_id(self.group_params['id'])
            group_view.show_full_group(group, self.storage)
            return

        groups = DataConnector(self.storage).get_user_groups(self.user_id)
        if len(groups) == 0:
            print("\nThere's no groups for such user in the database")
            return

        view_actions = {
            "name": get_groups_with_name
        }

        was_filtered = False
        filtered_groups = groups
        for param in view_actions:
            if param in view_actions and self.group_params[param] is not None:
                filtered_groups = view_actions[param](filtered_groups, self.group_params[param])
                was_filtered = True

        if not was_filtered:
            for g in groups:
                group_view.show_full_group(g, self.storage)
            return

        if len(filtered_groups) == 0:
            print("\nThere're no tasks with such criteria")

        else:
            for g in filtered_groups:
                group_view.show_full_group(g, self.storage)

    @handle_group_ex
    def handle_change(self):
        changed_group = self.group_manager.change_group(self.group_params['id'],
                                                        user_id=self.user_id,
                                                        new_name=self.group_params['name'])

        group_view.show_full_group(changed_group, self.storage)

    @handle_group_ex
    def handle_deletion(self):

        delete_actions = {
            "user": self.group_manager.delete_user_from_group
        }

        par_been_deleted = False
        for obj in self.group_params:
            if obj in delete_actions and self.group_params[obj] is not None:
                delete_actions[obj](self.group_params['id'], self.user_id, self.group_params[obj])
                par_been_deleted = True

        if par_been_deleted:
            group = self.group_manager.storage.find_group_by_id(self.group_params['id'])
            group_view.show_full_group(group, self.storage)
            return

        self.group_manager.delete_group(self.group_params['id'], user_id=self.user_id)
        print('\nThe group was successfully deleted')

    @handle_group_ex
    def handle_addition(self):

        addition_actions = {
            "user": self.group_manager.add_user_to_group
        }

        for obj in self.group_params:
            if obj in addition_actions and self.group_params[obj] is not None:
                addition_actions[obj](self.group_params['id'], self.user_id, self.group_params[obj])

        group = self.group_manager.storage.find_group_by_id(self.group_params['id'])
        group_view.show_full_group(group, self.storage)
