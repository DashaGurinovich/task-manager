from task_manager_lib.db.manager import DataManager
from task_manager_clapp.settings import add_cur_user_id, get_cur_user_id
from task_manager_lib.exceptions import NotFoundObjectError


def get_cur_user():
    """Returns the current user"""

    try:
        # Get user id from the settings
        # If it's not valid return None
        user_id = get_cur_user_id()

        # if user_id is not None:
        #     # If id is valid, find the user_id by id
        #     try:
        #         current_user = DataManager(storage).find_user_by_id(user_id)
        #     except NotFoundObjectError:
        #         print("The current user_id from the settings doesn't exist in the database.")
        #         current_user = None
        # else:
        #     # If the user_id hasn't been found, return None
        #     current_user = None

    except (KeyError, ValueError):
        user_id = None

    return user_id


def authenticate_user(login, storage):
    """Authenticates the user

    Keyword arguments:
    login -- login of the user
    """

    try:
        authenticated_user = DataManager(storage).find_user_by_login(login)

    except NotFoundObjectError:
        print('The user with login "{}" does not exist'.format(login))
        return None

    # Show the result
    print('The user with login "{}" was successfully authenticated'.format(login))

    # Add current user_id to the settings
    add_cur_user_id(authenticated_user.id)

    # Return authenticated user_id
    return authenticated_user


def exit_user_profile(login, storage):
    """Exits the user profile

    Keyword arguments:
    login -- login of the user
    """

    try:
        user_id = get_cur_user_id()

    except (KeyError, ValueError) as e:
        print(e)
        return

    if user_id is None:
        print("There's no current user to log off")
        return

    try:
        _ = DataManager(storage).find_user_by_login(login)

    except NotFoundObjectError:
        print('The user with login "{}" does not exist'.format(login))
        return

    add_cur_user_id(None)

    # Show the result
    print('The user with login "{}" was successfully exited'.format(login))
