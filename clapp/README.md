#Tasks Manager

## Description
This app is created to help managing the tasks. You can create, update, complete the tasks, union them into the groups.
 You can even create and complete the tasks together with your friends. Have fun exploring it :)

## How to install?
####From the bitbucket.org
+ The project requires the task_manager_lib. To use the app install task_manager_lib:
~~~
user$ pip3 install jsonpickle
user$ pip3 install python-dateutil
~~~
+ Clone the project from the git repository:
~~~
user$ git clone https://username@bitbucket.org/DashaGurinovich/task-manager.git
~~~
+ Go to the cloned project folder 'task-manager/lib':
~~~
user$ cd task-manager/lib
~~~
+ And install the lib:
~~~
user$ python3 setup.py install
~~~
+ Then go to the cloned project folder 'task-manager/cl_app':
~~~
user$ cd task-manager/cl_app
~~~
+ And install the lib:
~~~
user$ python3 setup.py install
~~~

###The Usage
After the installation you can use the terminal app with the command 'task_manager':
~~~
task_manager [-h] {task,user,group,settings} ... 
~~~
The example of the usage:
~~~
user$ task_manager user create login email

24-06-18 17:23:25 | INFO : The user with login login and email email was created under 28047a4c-77ba-11e8-9534-80a5895bd4b9 id

The new user was successfully created

-----------------------------------------------------------
ID: 28047a4c-77ba-11e8-9534-80a5895bd4b9
Login: login
Email: email
-----------------------------------------------------------

user$ task_manager user authenticate login
The user with login "login" was successfully authenticated

user$ task_manager task create "New task"
24-06-18 17:26:58 | INFO : The task with name 'New task' was added under id a6cdb8b6-77ba-11e8-9534-80a5895bd4b9

The new task was successfully created

-----------------------------------------------------------
ID: a6cdb8b6-77ba-11e8-9534-80a5895bd4b9
Name: New task
The date of creation: 2018-06-24 17:26:58.092293
Status: CREATED
Priority: NO_PRIORITY

The percentage of completion:
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ [0.0%]

Users:
login (email: email, id: 28047a4c-77ba-11e8-9534-80a5895bd4b9)
-----------------------------------------------------------
~~~

####Author: Darya Gurinovich
####Version: 1.6