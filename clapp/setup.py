from setuptools import setup
from setuptools import find_packages


with open("README.md", "r") as fh:
    long_description = fh.read()


setup(
    name='Task_manager_cl_app',
    version='1.6',
    description='The command line app to test Tasks library. Created to help managing the tasks',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='Darya Gurinovich',
    url='https://bitbucket.org/DashaGurinovich/task-manager',
    packages=find_packages(),
    install_requires=["Task_manager_lib"],

    entry_points={
        'console_scripts': [
            'task_manager=task_manager_clapp.main:main'
        ]
    },
    include_package_data=True
)
