"""This module includes validation functions for the entities fields"""

from datetime import datetime

from task_manager_lib.entities import task
from task_manager_lib.exceptions import NotValidParameterError


# region Validation for the task fields
def validate_task_status(status):
    """ Checks and returns task status if it's valid else raises the error
    Possible statuses: task.Status.CREATED, in process, NOT_RELEVANT, 3

    Keyword arguments:
    status -- task status

    Raises:
        NotValidParameterError -- when the status is not valid
    """
    # Return value if it's enums.Status
    if isinstance(status, task.Status):
        return status

    # Return value if value is str, that becomes number between 0 and 3
    elif isinstance(status, str):
        try:
            status_number = int(status)
            if 0 <= status_number <= 3:
                return task.Status(status_number)

        except ValueError:
            # Return value if such string is in status enum
            status = status.upper()
            try:
                return task.Status[status]
            except KeyError:
                pass

    # Return value if status is number between 0 and 3
    elif isinstance(status, int) and 0 <= status <= 3:
        return task.Status(status)

    # Else raise the error
    raise NotValidParameterError(status, 'status')


def validate_task_priority(priority):
    """ Checks and returns task priority if it's valid else raises the error
    Possible priorities: task.Priority.LOW, high, 2

    Keyword arguments:
    priority -- task priority

    Raises:
        NotValidParameterError -- when the priority is not valid
    """
    # Return value if it's enums.Priority
    if isinstance(priority, task.Priority):
        return priority

    elif isinstance(priority, str):

        # Return value if value is str, that becomes number between 0 and 3
        try:
            priority_number = int(priority)
            if 0 <= priority_number <= 3:
                return task.Priority(priority_number)

        except ValueError:
            # Return value if such string is in priority enum
            priority = priority.upper()
            try:
                return task.Priority[priority]
            except KeyError:
                pass

    # Return value if status is number between 0 and 3
    elif isinstance(priority, int) and 0 <= priority <= 3:
        return task.Priority(priority)

    # Else raise the error
    raise NotValidParameterError(priority, 'priority')


def validate_task_percentage(percentage):
    """ Checks and returns task percentage of completion if it's valid else raises the error

    Keyword arguments:
    percentage -- task percentage

    Raises:
        NotValidParameterError -- when the percentage is not valid

    """
    # Return value if it's float between 0 and 100
    if isinstance(percentage, float):
        if 0 <= percentage <= 100:
            return percentage

    # Return value if percentage is str that becomes float between 0 and 100
    elif isinstance(percentage, str):
        try:
            percentage = float(percentage)
            if 0 <= percentage <= 100:
                return percentage
        except ValueError:
            pass

    # Else raise the error
    raise NotValidParameterError(percentage, 'percentage of completion')


def validate_date(date):
    """ Checks and returns date if it's valid else raises the error

    Keyword arguments:
    date -- date

    Raises:
        NotValidParameterError -- when the date is not valid

    """
    if date is not None:
        try:
            # Trying to parse deadline in H-M d-m-Y format
            # Return if success
            date = datetime.strptime(date, "%H-%M %d-%m-%Y")
            return date

        except (ValueError, TypeError):
            try:
                # Trying to parse deadline in d-m-Y format
                # Return if success
                date = datetime.strptime(date, "%d-%m-%Y")
                return date

            # Else raise the error
            except (ValueError, TypeError):
                raise NotValidParameterError(date, 'date')
# endregion
