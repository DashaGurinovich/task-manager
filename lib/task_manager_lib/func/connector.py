"""This module includes the DataConnector class that returns the objects of the other classes"""

from task_manager_lib.db.manager import DataManager
from task_manager_lib.logger import write_log


class DataConnector:
    """Returns the chosen objects of the written object

    For example, returns the tasks of the written user
    >>>import task_manager_lib.task_manager_lib.func.connector
    >>>obj = task_manager_lib.task_manager_lib.func.connector.DataConnector(storage)
    >>>obj.get_user_tasks(user_id)
    """

    def __init__(self, storage):
        """Initializes the object

        Fields:
        storage -- the data storage object
        """
        self.storage = DataManager(storage)

    # region Get Task objects func
    @write_log
    def get_task_groups(self, task):
        """ Returns all groups of the task

        Keyword arguments:
        task -- the task to find groups of
        """

        groups = []
        all_groups = self.storage.get_groups_from_db()

        for gr in all_groups:
            if gr.id in task.groups_list:
                groups.append(gr)

        return groups

    @write_log
    def get_task_users(self, task):
        """ Returns all users of the task

        Keyword arguments:
        task -- the task to find users of
        """

        users = []
        all_users = self.storage.get_users_from_db()

        for u in all_users:
            if u.id in task.users_list:
                users.append(u)

        return users

    @write_log
    def get_task_sub_tasks(self, task_id):
        """ Returns all sub_tasks of task that now are stored in the database

        Keyword arguments:
        task_id -- id of the task to find sub_tasks of
        """

        sub_tasks = []

        all_tasks = self.storage.get_tasks_from_db()

        for st in all_tasks:
            if st.parent == task_id and not st.is_blocked:
                sub_tasks.append(st)

        return sub_tasks

    @write_log
    def get_task_dependent_tasks(self, task_id):
        """ Returns all dependent_tasks of task that now are stored in the database

        Keyword arguments:
        task_id -- id of the task to find dependent_tasks of
        """

        dependent_tasks = []

        all_tasks = self.storage.get_tasks_from_db()

        for dt in all_tasks:
            if dt.parent == task_id and dt.is_blocked:
                dependent_tasks.append(dt)

        return dependent_tasks

    # endregion

    # region Get Group objects func
    @write_log
    def get_group_users(self, group):
        """Returns group users

        Keyword arguments:
        group_id-- group id to return users of
        """

        users = []
        all_users = self.storage.get_users_from_db()

        for u in all_users:
            if u.id in group.users_list:
                users.append(u)

        return users

    @write_log
    def get_group_tasks(self, group_id):
        """Returns group tasks

        Keyword arguments:
        group_id-- group id to return tasks of
        """

        tasks = []
        all_tasks = self.storage.get_tasks_from_db()

        for t in all_tasks:
            if group_id in t.groups_list:
                tasks.append(t)

        return tasks
    # endregion

    # region Get User objects func
    @write_log
    def get_user_groups(self, user_id):
        """Returns user groups

        Keyword arguments:
        user_id-- user to return groups of
        """

        groups = []
        all_groups = self.storage.get_groups_from_db()

        for g in all_groups:
            if user_id in g.users_list:
                groups.append(g)

        return groups

    @write_log
    def get_user_tasks(self, user_id):
        """Returns user tasks
        Keyword arguments:
        user_id-- user to return tasks of
        """

        tasks = []
        all_tasks = self.storage.get_tasks_from_db()

        for t in all_tasks:
            if user_id in t.users_list:
                tasks.append(t)

        return tasks
    # endregion
