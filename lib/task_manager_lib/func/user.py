"""This module includes the main logic for the User entities

The data storage can be passed when the manager object is initialized. If the storage wasn't passed, but storage_path
was passed the library creates json-storage by this path and uses it. The passed storage should contain such methods
as clear(), get_*_from_db(), add_*_to_db(new_object), change_*_in_db(changed_object), delete_*_from_db(object_id)
where * is group, task and user. Otherwise the AttributeError will be raised while trying to perform some managers
functions.

For example:

>>> import os
>>> import lib.task_manager_lib.db
>>> import lib.task_manager_lib.db.json
>>> import lib.task_manager_lib.func.user

>>> path = os.path.expanduser('~/.TaskManager/data')
>>> json_storage = task_manager_lib.db.json.manager.JsonDataManager(path)
>>> storage = task_manager_lib.db.manager.DataManager(json_storage)
>>> user_manager = task_manager_lib.func.user.UserManager(storage)
>>> user = user_manager.create_user('login', 'email@sample.com')

The functions that can be performed with the user:
create user, change user, delete user
"""

from datetime import datetime

from task_manager_lib import exceptions
from task_manager_lib.db.manager import DataManager
from task_manager_lib.db.json.manager import JsonDataManager
from task_manager_lib.entities import user
from task_manager_lib.func import connector
from task_manager_lib.func import group as group_func, task as task_func
from task_manager_lib.logger import get_logger, write_log

MISSING_DATE = datetime.min


class UserManager:
    """Performs different actions with the User objects

    Fields:
    storage -- the DataManager object, that's used to work with the db
    logger -- the app logger
    """

    def __init__(self, storage=None, storage_path=None):
        """Initializes the object

        Keyword arguments:
        storage -- the storage to save data (if storage_path is passed too, the passed storage will be used
        not default json-storage)
        storage_path -- the path where to store data with default json-storage
        """

        self.logger = get_logger()

        if storage is None and storage_path is None:
            raise exceptions.NotPassedParameterError()

        if storage and not isinstance(storage, DataManager):
            raise TypeError("The storage isn't of the type DataManager")

        if storage:
            self.storage = storage
            return

        self.storage = DataManager(JsonDataManager(storage_path))

    @write_log
    def create_user(self, login, email, user_id=None, first_name=None, last_name=None, date_of_birth=None):
        """ Creates the user and stores in the database

        Keyword arguments:
        login -- login of the user
        email -- user email
        first_name -- first name
        last_name -- last name
        date_of_birth -- date of birth

        Raises:
            ExistingUserError -- when the user with such login or email already exists
            TypeError -- when the current user isn't the User object

        Example:
            >>>import task_manager_lib.task_manager_lib.func.user
            >>>u = task_manager_lib.task_manager_lib.func.user.UserManager(storage)
            >>>u.create_user("User login", "email@sample.com")
        """

        users = self.storage.get_users_from_db()

        # Checks if there's other users with such login or email, if so, raise the error
        if any(x.login == login for x in users) or any(x.email == email for x in users):
            raise exceptions.ExistingUserError(login, email)

        valid_date_of_birth = None
        if date_of_birth is not None:
            # Trying to parse date of birth
            try:
                valid_date_of_birth = datetime.strptime(date_of_birth, "%d-%m-%Y").date()

            except (ValueError, TypeError):
                raise ValueError("Not valid the date of birth in d-m-Y format.")

        new_user = user.User(login, email, user_id, first_name, last_name, valid_date_of_birth)

        # Save to db
        self.storage.add_user_to_db(new_user)

        # Log the result
        self.logger.info("The user with login {} and email {} was created under {} id".format(new_user.login,
                                                                                              new_user.email,
                                                                                              new_user.id))
        # Return new user
        return new_user

    # region User change func
    @write_log
    def change_user(self, user_id, date_of_birth=MISSING_DATE, email=None, first_name=None,
                    last_name=None, login=None):
        """Makes changes in the attributes of the passed user, returns the changed user and changes the user
        in the database

        Keyword arguments:
        user_id -- user to be changed
        date_of_birth -- new user date_of_birth
        email -- new user email
        first_name -- new user first name
        last_name -- new user last name
        login -- new user login

        Raises:
            NotExistingObjectError -- when the user is None
            TypeError -- when the user or new_params aren't the objects of the expected types

        Example:
            >>>import task_manager_lib.task_manager_lib.func.user
            >>>u = task_manager_lib.task_manager_lib.func.user.UserManager(storage)
            >>>u.change_user(the_user_id, login="New login", email="new_email@sample.com")
        """

        # If the user_id doesn't exist, raise the error
        if user_id is None:
            raise exceptions.NotExistingObjectError(user.User)

        try:
            # If user_id isn't the id of the User object, raise the error
            changing_user = self.storage.find_user_by_id(user_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed user isn't the id of the User object")

        if date_of_birth != MISSING_DATE:
            changing_user = self._change_date_of_birth(changing_user, date_of_birth)

        if email is not None:
            changing_user = self._change_email(changing_user, email)

        if first_name is not None:
            changing_user = self._change_first_name(changing_user, first_name)

        if last_name is not None:
            changing_user = self._change_last_name(changing_user, last_name)

        if login is not None:
            changing_user = self._change_login(changing_user, login)

        self.storage.change_user_in_db(changing_user)

        return changing_user

    @write_log
    def _change_login(self, changed_user, new_login):
        """Changes the user login. Returns the changed user. The object isn't saved in the db.

        Keyword arguments:
        changed_user -- user to be changed
        new_login -- new user login

        Raises:
            ExistingUserError -- when the user with such login already exists
        """

        users = self.storage.get_users_from_db()

        # Raise the error if there's the user with new_login
        if any(x.login == new_login for x in users):
            raise exceptions.ExistingUserError(new_login, changed_user.email)

        # Change the login
        changed_user.login = new_login

        # Log the result
        self.logger.info("The login of the user with {} id was changed to {}".format(changed_user.id, new_login))

        return changed_user

    @write_log
    def _change_email(self, changed_user, new_email):
        """Changes the user email. Returns the changed user. The object isn't saved in the db.

        Keyword arguments:
        changed_user -- user to be changed
        new_email -- new user email

        Raises:
            ExistingUserError -- when the user with such email already exists
        """

        users = self.storage.get_users_from_db()

        # Raise the error if there's the user with new_email
        if any(x.email == new_email for x in users):
            raise exceptions.ExistingUserError(changed_user.login, new_email)

        # Change the email
        changed_user.email = new_email

        # Log the result
        self.logger.info("The email of the user with {} id was changed to {}".format(changed_user.id, new_email))

        return changed_user

    @write_log
    def _change_first_name(self, changed_user, new_first_name):
        """Changes the user first_name. Returns the changed user. The object isn't saved in the db.

        Keyword arguments:
        changed_user -- user to be changed
        new_first_name -- new user first_name
        """

        if new_first_name is None:
            new_first_name = ""

            # Log the result
            self.logger.info("The first name of the user with {} id was deleted".format(changed_user.id))

        else:
            # Log the result
            self.logger.info("The first name of the user with {} id was changed to {}".format(changed_user.id,
                                                                                              new_first_name))
        # Change the first name
        changed_user.first_name = new_first_name

        return changed_user

    @write_log
    def _change_last_name(self, changed_user, new_last_name):
        """Changes the user last_name. Returns the changed user. The object isn't saved in the db.

        Keyword arguments:
        changed_user -- user to be changed
        new_last_name -- new user last_name
        """

        if new_last_name is None:
            new_last_name = ""

            # Log the result
            self.logger.info("The last name of the user with {} id was deleted".format(changed_user.id))

        else:
            # Log the result
            self.logger.info("The last name of the user with {} id was changed to {}".format(changed_user.id,
                                                                                             new_last_name))
        changed_user.last_name = new_last_name

        return changed_user

    @write_log
    def _change_date_of_birth(self, changed_user, new_date_of_birth):
        """Changes the user date_of_birth. Returns the changed user. The object isn't saved in the db.

        Keyword arguments:
        changed_user -- user to be changed
        new_date_of_birth -- new user date_of_birth
        """

        if new_date_of_birth is None:

            # Deletes the birthday
            changed_user.date_of_birth = None

            # Log the result
            self.logger.info("The date of birth of the user with {} id was deleted".format(changed_user.id))

        else:

            # Trying to parse date_of_birth
            try:
                date_of_birth = datetime.strptime(new_date_of_birth, "%d-%m-%Y").date()
                changed_user.date_of_birth = date_of_birth

                # Log the result
                self.logger.info("The date of birth of the user with {} id was changed to {}".format(changed_user.id,
                                                                                                     date_of_birth))

            except (ValueError, TypeError):
                raise exceptions.NotValidParameterError(new_date_of_birth, 'date of birth')

        return changed_user
    # endregion

    @write_log
    def delete_user(self, user_id):
        """Deletes the user

        Keyword arguments:
        user_id -- user to be deleted

        Raises:
            NotExistingObjectError -- when the user is None
            TypeError -- when the user isn't the User object
        """

        # If the user_id doesn't exist, raise the error
        if user_id is None:
            raise exceptions.NotExistingObjectError(user.User)

        try:
            # If user_id isn't the id of the User object, raise the error
            _ = self.storage.find_user_by_id(user_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed user isn't the id of the User object")

        # Gets all user objects and delete info from them about him
        # If the user is the only user, object gets deleted too
        obj_getter = connector.DataConnector(self.storage)

        tasks = obj_getter.get_user_tasks(user_id)
        for task in tasks:
            try:
                task = self.storage.find_task_by_id(task.id)
                task_users = obj_getter.get_task_users(task)

                if len(task_users) == 1:
                    task_manager = task_func.TaskManager(self.storage)
                    task_manager.delete_task(task.id, user_id)

                else:
                    task.remove_user(user_id)
                    self.storage.change_task_in_db(task)

            except exceptions.NotFoundObjectError:
                pass

        groups = obj_getter.get_user_groups(user_id)
        for group in groups:
            group_users = obj_getter.get_group_users(group)

            if len(group_users) == 1:
                group_manager = group_func.GroupManager(self.storage)
                group_manager.delete_group(group.id, user_id)

            else:
                group.remove_user(user_id)
                self.storage.change_group_in_db(group)

        # Delete from the db
        self.storage.delete_user_from_db(user_id)

        # Log the result
        self.logger.info("The user with {} id was deleted".format(user_id))
