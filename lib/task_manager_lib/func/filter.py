"""This module includes the functions that filter the passed parameters by some values"""

from uuid import UUID

from dateutil.relativedelta import relativedelta

from task_manager_lib import exceptions
from task_manager_lib.entities import group, task, user
from task_manager_lib.func import validation
from task_manager_lib.logger import write_log


# region Task filters
@write_log
def get_tasks_with_deadline(tasks, deadline):
    """ Returns all tasks with the given deadline

    Keyword arguments:
    tasks -- tasks to be filtered
    deadline -- the task deadline to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        NotValidParameterError -- when deadline isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """
    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    # Validates deadline
    deadline = validation.validate_date(deadline)

    try:
        if deadline.hour == 0 and deadline.minute == 0:
            return [t for t in tasks if t.deadline.date() == deadline.date()]
        else:
            return [t for t in tasks if t.deadline == deadline]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_creation_date(tasks, creation_date):
    """ Returns all tasks with the given creation_date

    Keyword arguments:
    tasks -- tasks to be filtered
    creation_date -- the task creation_date to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        NotValidParameterError -- when creation date isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    # Validates creation_date
    creation_date = validation.validate_date(creation_date)

    try:
        if creation_date.hour == 0 and creation_date.minute == 0:
            return [t for t in tasks if t.creation_date.date() == creation_date.date()]
        else:
            return [t for t in tasks if t.creation_date == creation_date]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_finishing_date(tasks, finishing_date):
    """ Returns all tasks with the given finishing_date

    Keyword arguments:
    tasks -- tasks to be filtered
    finishing_date -- the task finishing_date to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        NotValidParameterError -- when finishing date isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    # Validates finishing_date
    finishing_date = validation.validate_date(finishing_date)

    # Returns finishing_date, raises the error, if the tasks isn't the list of tasks
    try:
        if finishing_date.hour == 0 and finishing_date.minute == 0:
            return [t for t in tasks if t.finishing_date is not None
                    and t.finishing_date.date() == finishing_date.date()]
        else:
            return [t for t in tasks if t.finishing_date == finishing_date]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_blocking(tasks, blocking):
    """ Returns all tasks with the given blocking

    Keyword arguments:
    tasks -- tasks to be filtered
    blocking -- the task blocking to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        ValueError -- when the blocking isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    # Validates blocking
    if blocking.lower() in ('yes', 'true', 't', 'y', '1'):
        blocking = True
    elif blocking.lower() in ('no', 'false', 'f', 'n', '0'):
        blocking = False
    else:
        raise ValueError("{} is not valid boolean".format(blocking))

    # Returns blocking if it's valid
    try:
        return [t for t in tasks if t.is_blocked == blocking]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_status(tasks, status_name):
    """ Returns all tasks with the given status

    Keyword arguments:
    tasks -- tasks to be filtered
    status_name -- the task status to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        NotValidParameterError -- when the status isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    # Validates status
    status = validation.validate_task_status(status_name)

    # Returns status if it's valid
    try:
        return [t for t in tasks if t.status == task.Status(status)]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_priority(tasks, priority_name):
    """ Returns all tasks with the given priority

    Keyword arguments:
    tasks -- tasks to be filtered
    priority_name -- the task priority to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        NotValidParameterError -- when the priority isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    # Validates priority
    priority = validation.validate_task_priority(priority_name)

    # Returns priority if it's valid
    try:
        return [t for t in tasks if t.priority == task.Priority(priority)]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_completion_percentage(tasks, completion_percentage):
    """ Returns all tasks with the given completion_percentage

    Keyword arguments:
    tasks -- tasks to be filtered
    completion_percentage -- the task completion_percentage to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        NotValidParameterError -- when the percentage isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    # Validates completion_percentage
    completion_percentage = validation.validate_task_percentage(completion_percentage)

    # Returns completion_percentage if it's valid
    try:
        return [t for t in tasks if t.completion_percentage == completion_percentage]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_group_id(tasks, group_id):
    """ Returns all tasks with the given group

    Keyword arguments:
    tasks -- tasks to be filtered
    group_id -- task group id to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        NotValidParameterError -- when the group id isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    try:
        # Return tasks that have such id in their groups_list
        if isinstance(group_id, str):
            return [t for t in tasks if UUID(group_id) in t.groups_list]

        elif isinstance(group_id, UUID):
            return [t for t in tasks if group_id in t.groups_list]

        else:
            raise exceptions.NotValidParameterError(group_id, 'group id')

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_name(tasks, name):
    """ Returns all tasks that start with the given name

    Keyword arguments:
    tasks -- tasks to be filtered
    name -- the task name to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    try:
        # Return tasks with such name
        return [t for t in tasks if t.name.lower().startswith(name.lower())]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_period(tasks, period):
    """ Returns all tasks with the given period

    Keyword arguments:
    tasks -- tasks to be filtered
    period -- the task period to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        NotValidParameterError -- when the period isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    # Validates period
    if not isinstance(period, relativedelta):
        raise exceptions.NotValidParameterError("period", period)

    # Returns period if it's valid
    try:
        return [t for t in tasks if t.period == period]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")


@write_log
def get_tasks_with_period_end_date(tasks, period_end_date):
    """ Returns all tasks with the given period_end_date

    Keyword arguments:
    tasks -- tasks to be filtered
    period_end_date -- the task period_end_date to be filtered by

    Raises:
        NotExistingObjectError -- when 'tasks' is None
        NotValidParameterError -- when the period end date isn't valid
        TypeError -- when 'tasks' isn't the list of tasks
    """

    # If no tasks were passed, raise the error
    if tasks is None:
        raise exceptions.NotExistingObjectError([task.Task])

    # Validates period_end_date
    period_end_date = validation.validate_date(period_end_date)

    # Returns period_end_date if it's valid
    try:
        if period_end_date.hour == 0 and period_end_date.minute == 0:
            return [t for t in tasks if t.period_end_date.date() == period_end_date.date()]
        else:
            return [t for t in tasks if t.period_end_date == period_end_date]

    except (AttributeError, TypeError):
        raise TypeError("The tasks parameter isn't the list of tasks")

# endregion


# region Group filters
@write_log
def get_groups_with_name(groups, name):
    """ Returns all groups that start with the given name

    Keyword arguments:
    groups -- groups to be filtered
    name -- the group name to be filtered by

    Raises:
        NotExistingObjectError -- when 'groups' is None
        TypeError -- when 'groups' isn't the list of groups
    """

    # If no groups were passed, raise the error
    if groups is None:
        raise exceptions.NotExistingObjectError([group.Group])

    try:
        # Return groups with such name
        return [g for g in groups if g.name.lower().startswith(name.lower())]

    except (AttributeError, TypeError):
        raise TypeError("The groups parameter isn't the list of groups")
# endregion


# region User filters
@write_log
def get_users_with_email(users, email):
    """ Returns all users that emails start with the given email

    Keyword arguments:
    users -- users to be filtered
    email -- the user email to be filtered by

    Raises:
        NotExistingObjectError -- when 'users' is None
        TypeError -- when 'users' isn't the list of users
    """

    # If no users were passed, raise the error
    if users is None:
        raise exceptions.NotExistingObjectError([user.User])

    try:
        # Return user with such email
        return [u for u in users if u.email.lower().startswith(email.lower())]

    except (AttributeError, TypeError):
        raise TypeError("The users parameter isn't the list of users")


@write_log
def get_users_with_first_name(users, first_name):
    """ Returns all users that first name starts with the given first_name

    Keyword arguments:
    users -- users to be filtered
    first_name -- the user first_name to be filtered by

    Raises:
        NotExistingObjectError -- when 'users' is None
        TypeError -- when 'users' isn't the list of users
    """

    # If no users were passed, raise the error
    if users is None:
        raise exceptions.NotExistingObjectError([user.User])

    try:
        # Return user with such first_name
        return [u for u in users if u.first_name.lower().startswith(first_name.lower())]

    except (AttributeError, TypeError):
        raise TypeError("The users parameter isn't the list of users")


@write_log
def get_users_with_last_name(users, last_name):
    """ Returns all users that last_name starts with the given last_name

    Keyword arguments:
    users -- users to be filtered
    last_name -- the user last_name to be filtered by

    Raises:
        NotExistingObjectError -- when 'users' is None
        TypeError -- when 'users' isn't the list of users
    """

    # If no users were passed, raise the error
    if users is None:
        raise exceptions.NotExistingObjectError([user.User])

    try:
        # Return user with such last_name
        return [u for u in users if u.last_name.lower().startswith(last_name.lower())]

    except (AttributeError, TypeError):
        raise TypeError("The users parameter isn't the list of users")


@write_log
def get_users_with_full_name(users, full_name):
    """ Returns all users that full_name starts with the given full_name

    Keyword arguments:
    users -- users to be filtered
    last_name -- the user full_name to be filtered by

    Raises:
        NotExistingObjectError -- when 'users' is None
        TypeError -- when 'users' isn't the list of users
    """

    # If no users were passed, raise the error
    if users is None:
        raise exceptions.NotExistingObjectError([user.User])

    try:
        # Return user with such full_name
        return [u for u in users if (u.first_name + ' ' + u.last_name).lower().startswith(full_name.lower())]

    except (AttributeError, TypeError):
        raise TypeError("The users parameter isn't the list of users")


@write_log
def get_users_with_login(users, login):
    """ Returns all users that login start with the given login

    Keyword arguments:
    users -- users to be filtered
    login -- the user login to be filtered by

    Raises:
        NotExistingObjectError -- when 'users' is None
        TypeError -- when 'users' isn't the list of users
    """

    # If no users were passed, raise the error
    if users is None:
        raise exceptions.NotExistingObjectError([user.User])

    try:
        # Return user with such login
        return [u for u in users if u.login.lower().startswith(login.lower())]

    except (AttributeError, TypeError):
        raise TypeError("The users parameter isn't the list of users")


@write_log
def get_users_with_date_of_birth(users, date):
    """ Returns all users with the given date_of_birth

    Keyword arguments:
    users -- users to be filtered
    date -- the user date_of_birth to be filtered by

    Raises:
        NotExistingObjectError -- when 'users' is None
        TypeError -- when 'users' isn't the list of users
    """

    # If no users were passed, raise the error
    if users is None:
        raise exceptions.NotExistingObjectError([user.User])

    # Validates date_of_birth
    valid_date = validation.validate_date(date)

    try:
        # Return user with such date_of_birth
        return [u for u in users if u.date_of_birth == valid_date.date()]

    except (AttributeError, TypeError):
        raise TypeError("The users parameter isn't the list of users")

# endregion
