"""This package includes main library logic. It has classes and functions to perform different actions like the
creation, update, deletion, filtration and other with the main library entities

    Modules:
    validation -- includes validation functions for the entities fields
    filter -- includes functions that filter the passed lists by different params
    connecter -- includes the class DataConnector that helps to get objects connected to other objects (for example,
    get all tasks of the passed user)
    group -- includes the class GroupManager that has logic for working with the Group entities
    task -- includes the class TaskManager that has logic for working with the Task entities
    user -- includes the class UserManager that has logic for working with the User entities
"""