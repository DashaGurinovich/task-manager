"""This module includes the main logic for the Task entities

The data storage can be passed when the manager object is initialized. If the storage wasn't passed,
the library uses json-storage. The passed storage should contain such methods as clear(), get_*_from_db(),
add_*_to_db(new_object), change_*_in_db(changed_object), delete_*_from_db(object_id) where * is group, task and
user. Otherwise the AttributeError will be raised while trying to perform some managers functions. For example:

>>> import os
>>> import lib.task_manager_lib.db
>>> import lib.task_manager_lib.db.json
>>> import lib.task_manager_lib.func.task

>>> path = os.path.expanduser('~/.TaskManager/data')
>>> json_storage = task_manager_lib.db.json.manager.JsonDataManager(path)
>>> storage = task_manager_lib.db.manager.DataManager(json_storage)
>>> task_manager = task_manager_lib.func.task.TaskManager(storage)
>>> task = task_manager.create_user('Task name', user)

The functions that can be performed with the task:
create task, change task, delete task, add/delete user from task, add/delete group from task
"""

from datetime import datetime

from dateutil.relativedelta import relativedelta

from task_manager_lib import exceptions
from task_manager_lib.db.manager import DataManager
from task_manager_lib.db.json.manager import JsonDataManager
from task_manager_lib.entities import task, user
from task_manager_lib.entities.permissions import check_if_object_user
from task_manager_lib.func import validation
from task_manager_lib.func.connector import DataConnector
from task_manager_lib.logger import get_logger, write_log

MISSING_DATE = datetime.min
MISSING_PERIOD = relativedelta(minutes=0, hours=0, days=0, weeks=0, months=0, years=0)

PERIOD_START_DATE = 'period_start_date'
PERIOD = 'period'
PERIOD_END_DATE = 'period_end_date'


class TaskManager:
    """Performs different actions with the Tasks objects

    Fields:
    storage -- the DataManager object, that's used to work with the db
    logger -- the app logger
    """

    def __init__(self, storage=None, storage_path=None):
        """Initializes the object

        Keyword arguments:
        storage -- the storage to save data (if storage_path is passed too, the passed storage will be used
        not default json-storage)
        storage_path -- the path where to store data with default json-storage
        """

        self.logger = get_logger()

        if storage is None and storage_path is None:
            raise exceptions.NotPassedParameterError()

        if storage and not isinstance(storage, DataManager):
            raise TypeError("The storage isn't of the type DataManager")

        if storage:
            self.storage = storage
            return

        self.storage = DataManager(JsonDataManager(storage_path))

    # region The creation of different types of tasks
    @write_log
    def create_task(self, name, user_id, adding_group_id=None, task_id=None, deadline=None,
                    status=task.Status.CREATED, priority=task.Priority.NO_PRIORITY):
        """ Creates the task and stores in the database

        Keyword arguments:
        name -- name of new task
        user_id -- id of the user to create task
        adding_group_id -- id of the group to add (None)
        deadline -- task deadline (None)
        status -- task status (CREATED)
        priority -- task priority (NO_PRIORITY)

        Raises:
            NotExistingObjectError -- when the current user is None
            NotValidParameterError -- when the passed parameters aren't valid
            TypeError -- when the current user or the adding group isn't of the expected object types

        Example:
            >>>import task_manager_lib.task_manager_lib.func.task
            >>>t = task_manager_lib.task_manager_lib.func.task.TaskManager(storage)
            >>>t.create_task("Task name", the_user_id)


        """
        # If the user id wasn't passed, raise the error
        if user_id is None:
            raise exceptions.NotExistingObjectError(user.User)

        try:
            # If user_id isn't the id of the User object, raise the error
            _ = self.storage.find_user_by_id(user_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed user_id isn't the id of the User object")

        # If there's no group with such id, the error will be raised in find function
        adding_group = None
        if adding_group_id is not None:
            try:
                # If group_id isn't the id of the Group object, raise the error
                adding_group = self.storage.find_group_by_id(adding_group_id)
            except exceptions.NotFoundObjectError:
                raise TypeError("The passed group_id isn't the id of the Group object")

        # Create Task instance
        new_task = task.Task(name, task_id=task_id)

        # Add connections between task and user
        new_task.add_user(user_id)

        # Set task deadline, validating it through function
        if deadline is not None:
            valid_deadline = validation.validate_date(deadline)
            new_task.change_task_deadline(valid_deadline)

        # Set task status, validating it through function
        if status != task.Status.CREATED and status is not None:
            status = validation.validate_task_status(status)
            new_task.change_task_status(status)

        # Set task priority, validating it through function
        if priority != task.Priority.NO_PRIORITY and priority is not None:
            priority = validation.validate_task_priority(priority)
            new_task.priority = priority

        # If group is valid
        if adding_group is not None:

            # Add connection between group and task
            new_task.add_group(adding_group.id)

        # Save the task
        self.storage.add_task_to_db(new_task)

        # Log the result
        self.logger.info("The task with name '{}' was added under id {}".format(name, new_task.id))

        # Return task
        return new_task

    @check_if_object_user
    @write_log
    def create_sub_task(self, main_task_id, user_id, sub_task_name, sub_task_group_id=None, sub_task_id=None,
                        sub_task_deadline=None, sub_task_status=None, sub_task_priority=None):
        """ Creates the sub_task and stores in the database

            Keyword arguments:
            main_task_id -- parent task
            sub_task_name -- name of new task
            user_id -- user to create task
            sub_task_adding_group -- group to add
            sub_task_deadline -- task deadline
            sub_task_status -- task status
            sub_task_priority -- task priority

            Raises:
            NotExistingObjectError -- when the current user or the main task is None
            NotValidParameterError -- when the passed parameters aren't valid
            TypeError -- when the current user, the main task or the adding group isn't of the expected object types

            Example:
                >>>import task_manager_lib.task_manager_lib.func.task
                >>>t = task_manager_lib.task_manager_lib.func.task.TaskManager(storage)
                >>>main_task_id = t.create_task("Task name", the_user_object)
                >>>sub_task = t.create_sub_task(main_task_id, the_user_object, "Sub task name")
            """

        try:
            # If main_task_id isn't the id of the Task object, raise the error
            main_task = self.storage.find_task_by_id(main_task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed main_task_id isn't the id of the Task object")

        # Create Task instance
        sub_task = self.create_task(sub_task_name, user_id, sub_task_group_id, sub_task_id, sub_task_deadline,
                                    sub_task_status, sub_task_priority)

        # Add sub_task connections
        sub_task.add_parent(main_task.id)

        # Log the result
        self.logger.info("The task with name '{}' became sub task of task with id {}"
                         .format(sub_task_name, main_task_id))

        # Save to db
        self.storage.change_task_in_db(sub_task)

        # Return sub_task
        return sub_task

    @check_if_object_user
    @write_log
    def create_dependent_task(self, main_task_id, user_id, dependent_task_name, dependent_task_id=None,
                              dependent_task_group=None, dependent_task_deadline=None, dependent_task_status=None,
                              dependent_task_priority=None):
        """ Creates the dependent_task and stores in the database

        Keyword arguments:
        main_task_id -- parent task
        dependent_task_name -- name of new task
        user_id -- user to create task
        dependent_task_adding_group -- group to add
        dependent_task_deadline -- task deadline
        dependent_task_status -- task status
        dependent_task_priority -- task priority

        Raises:
            NotExistingObjectError -- when the current user or the main task is None
            NotValidParameterError -- when the passed parameters aren't valid
            TypeError -- when the current user, the main task or the adding group isn't of the expected object types

        Example:
            >>>import task_manager_lib.task_manager_lib.func.task
            >>>t = task_manager_lib.task_manager_lib.func.task.TaskManager(storage)
            >>>main_task_id = t.create_task("Task name", the_user_object)
            >>>dep_task = t.create_dependent_task(main_task_id, the_user_object, "Dep task name")

        """

        try:
            # If main_task_id isn't the id of the Task object, raise the error
            main_task = self.storage.find_task_by_id(main_task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed main_task_id isn't the id of the Task object")

        # Create Task instance
        dependent_task = self.create_task(dependent_task_name, user_id, dependent_task_group, dependent_task_id,
                                          dependent_task_deadline, dependent_task_status, dependent_task_priority)

        # If main task is finished, the dependent task is unblocked
        if main_task.status != task.Status.FINISHED:
            # Add dependent_task connections
            dependent_task.add_parent(main_task.id)
            dependent_task.is_blocked = True

            # Log the result
            self.logger.info("The task with name '{}' became dependent task on task with id {}".format(
                dependent_task_name, main_task.id))
            # Save to db
            self.storage.change_task_in_db(dependent_task)

        else:
            self.logger.warning("The main task with id '{}' is already finished. The dependent task with id '{}' is "
                                "unblocked".format(main_task_id, dependent_task.id))

        # Return dependent_task
        return dependent_task
    # endregion

    # region The change of different task fields
    @check_if_object_user
    @write_log
    def change_task(self, task_id, user_id, deadline=MISSING_DATE, definition=None, name=None, percentage=None,
                    period_start_date=MISSING_DATE, period=MISSING_PERIOD, period_end_date=MISSING_DATE,
                    priority=None, status=None):
        """Makes changes in the attributes of the passed task, returns the changed task and changes the task
        in the database

        Keyword arguments:
        task_id -- task to be changed
        user_id -- user that changes the task
        deadline -- new task deadline
        definition -- new task definition
        name -- new task name
        percentage -- new task percentage
        period_start_date -- new task period start date
        period -- new task period
        period_end_date -- new task period end date
        priority -- new task priority
        status -- new task status

        Raises:
            NotExistingObjectError -- when the current user is None
            NotExistingPeriodStartDateError -- when there's period or period end date but no period start date
            NotExistingPeriodError -- when there's period end date but no period
            NotValidParameterError -- when the passed parameters aren't valid
            NotValidPeriodDatesError -- when the start period date is later than end period date
            TypeError -- when the current user or the adding group isn't of the expected object types

        Example:
            >>>import task_manager_lib.task_manager_lib.func.task
            >>>t = task_manager_lib.task_manager_lib.func.task.TaskManager(storage)
            >>>t.change_task(the_task_id, the_user_id, name="New task name", status=task.Status.IN_PROCESS)

        """

        try:
            # If task_id isn't the id of the Task object, raise the error
            changing_task = self.storage.find_task_by_id(task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed task_id isn't the id of the Task object")

        if not isinstance(period, relativedelta) and period is not None:
            raise exceptions.NotValidParameterError(period, "period")

        if deadline != MISSING_DATE:
            changing_task = self._change_deadline(changing_task, deadline)

        if definition is not None:
            changing_task = self._change_definition(changing_task, definition)

        if name is not None:
            changing_task = self._change_name(changing_task, name)

        if percentage is not None:
            changing_task = self._change_completion_percentage(changing_task, percentage)

        if period_start_date != MISSING_DATE:
            changing_task = self._change_period_start_date(changing_task, period_start_date)

        if period is None or period != MISSING_PERIOD:
            changing_task = self._change_period(changing_task, period)

        if period_end_date != MISSING_DATE:
            changing_task = self._change_period_end_date(changing_task, period_end_date)

        if priority is not None:
            changing_task = self._change_priority(changing_task, priority)

        if status is not None:
            changing_task = self._change_status(changing_task, status)

        self.storage.change_task_in_db(changing_task)

        return changing_task

    @write_log
    def _change_name(self, changed_task, new_name):
        """Changes the task name. Returns the changed task. The object isn't saved in the db.
        (helping function for the change function)

        Keyword arguments:
        changed_task -- task to be changed
        new_name -- new task name

        Raises:
            NotValidParameterError -- when the passed name isn't valid

        """
        # Raise the error if new name is None
        if new_name is None:
            raise exceptions.NotValidParameterError(None, "task name")

        # Change name
        changed_task.name = new_name

        # Log the result
        self.logger.info("The name of the task with id '{}' was changed to {}".format(changed_task.id,
                                                                                      changed_task.name))

        return changed_task

    @write_log
    def _change_definition(self, changed_task, new_definition):
        """Changes the task definition. Returns the changed task. The object isn't saved in the db.
         (helping function for the change function)

        Keyword arguments:
        changed_task -- task to be changed
        new_definition -- new task definition
        """

        # Change definition
        changed_task.definition = new_definition

        # Log the result
        self.logger.info("The definition of the task with id '{}' was changed to {}"
                         .format(changed_task.id, changed_task.definition))

        return changed_task

    @write_log
    def _change_deadline(self, changed_task, new_deadline):
        """Changes the task deadline. Returns the changed task. The object isn't saved in the db.
         (helping function for the change function)

        Keyword arguments:
        changed_task -- task to be changed
        new_deadline -- new task deadline

        Raises:
            NotValidParameterError -- when the passed deadline isn't valid

        """

        if new_deadline is None:

            # Delete the deadline
            changed_task.change_task_deadline(None)

            # Log the result
            self.logger.info("The deadline of the task with id '{}' was deleted".format(changed_task.id))

        else:

            # Check and set the deadline
            deadline = validation.validate_date(new_deadline)

            changed_task.change_task_deadline(deadline)

            # Log the result
            self.logger.info("The deadline of the task with id '{}' was changed to {}".format(changed_task.id,
                                                                                              deadline))

        return changed_task

    @write_log
    def _change_period(self, changed_task, new_period):
        """Changes the task period. Returns the changed task. The object isn't saved in the db.
         (helping function for the change function)

        Keyword arguments:
        changed_task -- task to be changed
        new_period -- new task period (relativedelta object)

        Raises:
            NotExistingPeriodStartDateError -- when there's period or period end date but no period start date
            NotValidParameterError -- when the passed period isn't valid

        """

        if changed_task.period_start_date is None:
            raise exceptions.NotExistingPeriodStartDateError()

        # If period end date exist, delete it while deleting period
        if new_period is None:

            # Delete
            changed_task.period_end_date = None
            changed_task.period = new_period

            # Log the result
            self.logger.info("The period of the task with id '{}' was deleted".format(changed_task.id))

            if changed_task.period_end_date is not None:
                self.logger.warn("The period end date of the task with id '{}' was deleted".
                                 format(changed_task.id))

        else:

            # Check and set period
            if not isinstance(new_period, relativedelta):
                raise exceptions.NotValidParameterError("period", new_period)

            changed_task.period = new_period
            # Log the result
            self.logger.info("The period of the task with id '{}' was changed to {}".format(changed_task.id,
                                                                                            new_period))

        return changed_task

    @write_log
    def _change_period_start_date(self, changed_task, new_period_start_date):
        """Changes the task period_start_date. Returns the changed task. The object isn't saved in the db.
         (helping function for the change function)

        Keyword arguments:
        changed_task -- task to be changed
        new_period_start_date -- new task period_start_date

        Raises:
            NotValidParameterError -- when the passed period start date isn't valid

        """

        if new_period_start_date is None:

            period = changed_task.period
            period_end_date = changed_task.period_end_date

            # Delete
            changed_task.change_period_start_date(None)

            # Log the result
            self.logger.info("The period start date of the task with id '{}' was deleted".format(changed_task.id))

            if period is not None or period_end_date is not None:
                self.logger.warn("The period and period end date of the task with id '{}' was deleted".
                                 format(changed_task.id))

        else:

            # Check and set period_start_date
            period_start_date = validation.validate_date(new_period_start_date)

            changed_task.change_period_start_date(period_start_date)

            # Log the result
            self.logger.info("The period start date of the task with id '{}' was changed to {}".format(
                changed_task.id, period_start_date))

        return changed_task

    @write_log
    def _change_period_end_date(self, changed_task, new_period_end_date):
        """Changes the task period_end_date. Returns the changed task. The object isn't saved in the db.
         (helping function for the change function)

        Keyword arguments:
        changed_task -- task to be changed
        new_period_end_date -- new task period_end_date

        Raises:
            NotExistingPeriodStartDateError -- when there's period or period end date but no period start date
            NotExistingPeriodError -- when there's period end date but no period
            NotValidParameterError -- when the passed period end date isn't valid

        """

        # If there's no period start date, raise the error
        if changed_task.period_start_date is None:
            raise exceptions.NotExistingPeriodStartDateError()

        # If there's no period, raise the error
        if changed_task.period is None or changed_task.period == 0:
            raise exceptions.NotExistingPeriodError()

        if new_period_end_date is None:

            # Delete
            changed_task.change_period_end_date(None)

            # Log the result
            self.logger.info("The period end date of the task with id '{}' was deleted".format(changed_task.id))

        else:

            # Check and set period_end_date
            period_end_date = validation.validate_date(new_period_end_date)

            # Can't change period end date if it's sooner than period start date
            if changed_task.period_start_date > period_end_date:
                raise exceptions.NotValidPeriodDatesError(changed_task.period_start_date, period_end_date)

            else:
                changed_task.change_period_end_date(period_end_date)

                # Log the result
                self.logger.info("The period end date of the task with id '{}' was changed to {}".format(
                    changed_task.id, period_end_date))

        return changed_task

    @write_log
    def _change_status(self, changed_task, new_status):
        """Changes the task status. Returns the changed task. The object isn't saved in the db.
         (helping function for the change function)

        Keyword arguments:
        changed_task -- task to be changed
        new_status -- new task status

        Raises:
            BlockedTaskChangeError -- when the changing task is blocked
            NotValidParameterError -- when the passed status isn't valid

        """

        # If the changing task is blocked, raise the error
        if changed_task.is_blocked:
            raise exceptions.BlockedTaskChangeError()

        # Check and set status
        new_status = validation.validate_task_status(new_status)

        changed_task.change_task_status(new_status)

        # Log the result
        self.logger.info("The status of the task with id '{}' was changed to {}".format(changed_task.id, new_status))

        # If status is finished
        if changed_task.status == task.Status.FINISHED:

            obj_getter = DataConnector(self.storage)
            dependent_tasks = obj_getter.get_task_dependent_tasks(changed_task.id)

            # Unblock all dependent tasks
            for dependent_task in dependent_tasks:
                dependent_task.is_blocked = False
                dependent_task.parent = None
                self.storage.change_task_in_db(dependent_task)

            # Finish all sub tasks
            sub_tasks = obj_getter.get_task_sub_tasks(changed_task.id)
            for sub_task in sub_tasks:
                sub_task.change_task_status(task.Status.FINISHED)
                self.storage.change_task_in_db(sub_task)

        return changed_task

    @write_log
    def _change_priority(self, changed_task, new_priority):
        """Changes the task priority. Returns the changed task. The object isn't saved in the db.
         (helping function for the change function)

        Keyword arguments:
        changed_task -- task to be changed
        new_priority -- new task priority

        Raises:
            NotValidParameterError -- when the passed priority isn't valid
        """

        # Check and set priority
        new_priority = validation.validate_task_priority(new_priority)

        changed_task.priority = new_priority

        # Log the result
        self.logger.info("The priority of the task with id '{}' was changed to {}".format(changed_task.id,
                                                                                          new_priority))

        return changed_task

    @write_log
    def _change_completion_percentage(self, changed_task, new_completion_percentage):
        """Changes the task completion_percentage. Returns the changed task. The object isn't saved in the db.
         (helping function for the change function)

        Keyword arguments:
        changed_task -- task to be changed
        new_completion_percentage -- new task completion_percentage

        Raises:
            BlockedTaskChangeError -- when the changing task is blocked
            NotValidParameterError -- when the passed percentage isn't valid
        """

        # If the changing task is blocked, raise the error
        if changed_task.is_blocked:
            raise exceptions.BlockedTaskChangeError()

        # Check and set new_completion_percentage
        new_completion_percentage = validation.validate_task_percentage(new_completion_percentage)

        changed_task.change_completion_percentage(new_completion_percentage)

        # Log the result
        self.logger.info("The percentage of completion of the task with id '{}' was changed to {}".format(
            changed_task.id, new_completion_percentage))

        # If percentage equals 100
        if new_completion_percentage == 100:

            obj_getter = DataConnector(self.storage)
            # Unblock all dependent tasks
            dependent_tasks = obj_getter.get_task_dependent_tasks(changed_task.id)

            for dependent_task in dependent_tasks:
                dependent_task.is_blocked = False
                dependent_task.parent = None
                self.storage.change_task_in_db(dependent_task)

            # Finish all sub tasks
            sub_tasks = obj_getter.get_task_sub_tasks(changed_task.id)
            for sub_task in sub_tasks:
                sub_task.change_task_status(task.Status.FINISHED)
                self.storage.change_task_in_db(sub_task)

        return changed_task
    # endregion

    # region The deletion of tasks
    @check_if_object_user
    @write_log
    def delete_task(self, task_id, user_id):
        """Deletes the task

        Keyword arguments:
        deleting_task -- task to be deleted
        user_id -- user that deleted the task

        Raises:
            NotExistingObjectError -- when the task or the current user is None
            TypeError -- when the current_user or the task aren't the objects of the expected types

        """

        try:
            # If task_id isn't the id of the Task object, raise the error
            _ = self.storage.find_task_by_id(task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed task_id isn't the id of the Task object")

        connector = DataConnector(self.storage)

        # If task has sub_tasks, delete them recursively
        sub_tasks = connector.get_task_sub_tasks(task_id)
        if len(sub_tasks) != 0:
            for sub_task in sub_tasks:
                self.delete_task(sub_task.id, user_id)

        # If task has dependent tasks, they become independent
        dependent_tasks = connector.get_task_dependent_tasks(task_id)
        for dependent_task in dependent_tasks:
            dependent_task.is_blocked = False
            dependent_task.parent = None
            self.storage.change_task_in_db(dependent_task)

        # Delete task from db
        self.storage.delete_task_from_db(task_id)

        # Log the result
        self.logger.info("The task with id '{}' was deleted".format(task_id))

    # endregion

    # Task period
    @check_if_object_user
    @write_log
    def make_task_periodic(self, task_id, user_id, period_start_date, period, period_end_date=MISSING_DATE):
        """Makes the passed task a periodic task. Returns a periodic task

        Keyword arguments:
        task_id-- task to become periodic
        user_id -- user that adds period to the task
        period_start_date -- the date of the task period start
        period -- task period
        period_end_date -- the date of the task period finish (None)

        Raises:
            NotExistingObjectError -- when the task or the current user is None
            NotValidParameterError -- when the passed parameters aren't valid
            NotValidPeriodDatesError -- when the start period date is later than end period date
            TypeError -- when the current user, task or the period isn't of the expected object types
        """

        try:
            # If task_id isn't the id of the Task object, raise the error
            cur_task = self.storage.find_task_by_id(task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed task_id isn't the id of the Task object")

        # If the period is not the relativedelta object, raise the error
        if not isinstance(period, relativedelta):
            raise exceptions.NotValidParameterError(period, "period")

        # Validate the dates
        valid_period_start_date = validation.validate_date(period_start_date)

        if period_end_date == MISSING_DATE:
            valid_period_end_date = cur_task.period_end_date
        else:
            valid_period_end_date = validation.validate_date(period_end_date)

        # Raise the error if the start period date is later than end period date
        if valid_period_end_date is not None and valid_period_start_date > valid_period_end_date:
            raise exceptions.NotValidPeriodDatesError(valid_period_start_date, valid_period_end_date)

        # Set the values
        cur_task.change_period_start_date(valid_period_start_date)
        cur_task.period = period
        cur_task.change_period_end_date(valid_period_end_date)

        # Save in the db
        self.storage.change_task_in_db(cur_task)

        return cur_task

    @write_log
    def get_next_periodic_task_date(self, task_id):
        """Returns the next periodic task date if the task is periodic"""

        try:
            # If task_id isn't the id of the Task object, raise the error
            cur_task = self.storage.find_task_by_id(task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed task_id isn't the id of the Task object")

        if cur_task.period is not None:
            periodic_date = cur_task.period_start_date

            while datetime.now() > periodic_date:
                periodic_date += cur_task.period

            periodic_date += cur_task.period
            if (cur_task.period_end_date is not None and periodic_date <= cur_task.period_end_date)\
                    or cur_task.period_end_date is None:
                return periodic_date

            return None

    @write_log
    def get_nearest_periodic_task_dates(self, task_id):
        """Returns nearest periodic task dates if there's task period

        If there's no periodicity end date, 5 nearest dates will be returned
        If there's lots of dates till the period end date, 5 nearest dates will be returned too
        """

        try:
            # If task_id isn't the id of the Task object, raise the error
            cur_task = self.storage.find_task_by_id(task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed task_id isn't the id of the Task object")

        if cur_task.period is not None:
            dates = []
            periodic_date = cur_task.period_start_date

            while datetime.now() > periodic_date:
                periodic_date += cur_task.period

            if cur_task.period_end_date is not None:
                dates_number = 0

                while periodic_date <= cur_task.period_end_date and dates_number < 5:
                    dates.append(periodic_date)
                    periodic_date += cur_task.period
                    dates_number = dates_number + 1

            else:

                for index in range(5):
                    dates.append(periodic_date)
                    periodic_date += cur_task.period

            return dates

        return []
    # endregion

    @write_log
    def get_task_tree(self, task_id):
        """Returns task tree as the list of tuples
        where the first element is depth, the second is task id

        Keyword arguments:
        task_id -- the root of the tree
        """

        stack = [(0, task_id)]
        task_tree = []
        connector = DataConnector(self.storage)

        while stack:
            cur = stack.pop()
            task_tree.append(cur)
            for sub_task in connector.get_task_sub_tasks(cur[1]):
                stack.append((cur[0] + 1, sub_task.id))

        return task_tree

    @check_if_object_user
    @write_log
    def add_user_to_task(self, task_id, user_id, user_login):
        """Adds user to the task

        Keyword arguments:
        task_id-- task to be added to
        user_id -- user that adds to the task
        user_login -- login of new user

        Raises:
            NotExistingObjectError -- when the task or the current user is None
            NotFoundObjectError -- when the user wasn't found by such login
            ObjectAlreadyInListError -- when the user with such login is already in the list
            TypeError -- when the current_user or the task aren't the objects of the expected types

        """

        try:
            # If task_id isn't the id of the Task object, raise the error
            cur_task = self.storage.find_task_by_id(task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed task_id isn't the id of the Task object")

        # Find user_id object by login
        found_user = self.storage.find_user_by_login(user_login)

        users_ids = cur_task.users_list

        # If the user_id is already in this task, raise the error
        if found_user.id in users_ids:
            raise exceptions.ObjectAlreadyInListError('User', 'Task')

        # Add connections between task and user_id
        cur_task.add_user(found_user.id)

        # Change in the db
        self.storage.change_task_in_db(cur_task)

        # Log the result
        self.logger.info("The user with login '{}' was successfully added to task with id '{}'"
                         .format(user_login, cur_task.id))

        return cur_task

    @check_if_object_user
    @write_log
    def delete_user_from_task(self, task_id, user_id, user_login):
        """Deletes user from the group

        Keyword arguments:
        task_id -- task to be deleted from
        user_id -- user that deletes from the task
        user_login -- login of deleting user

        Raises:
            NotExistingObjectError -- when the task or the current user is None
            NotFoundObjectError -- when the user wasn't found by such login
            ObjectNotInListError -- when the user with such login isn't in the list
            OnlyUserDeletionError -- when the only user is trying to be deleted
            TypeError -- when the current_user or the task aren't the objects of the expected types

        """

        try:
            # If task_id isn't the id of the Task object, raise the error
            cur_task = self.storage.find_task_by_id(task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed task_id isn't the id of the Task object")

        # Find user_id object by login
        found_user = self.storage.find_user_by_login(user_login)

        users_ids = cur_task.users_list

        # If the user_id is not in this task, raise the error
        if found_user.id not in users_ids:
            raise exceptions.ObjectNotInListError('User', 'Task')

        # If he's the only user_id, he won't be deleted
        if len(users_ids) == 1:
            raise exceptions.OnlyUserDeletionError('Task')

        # Remove information between task and user_id
        cur_task.remove_user(found_user.id)

        # Change in the db
        self.storage.change_task_in_db(cur_task)

        # Log the result
        self.logger.info("The user with login '{}' was successfully deleted from group with id '{}'"
                         .format(user_login, cur_task.id))
        return cur_task

    @check_if_object_user
    @write_log
    def add_group_to_task(self, task_id, user_id, group_id):
        """Adds group to the task

        Keyword arguments:
        task_id-- task to be added to
        user_id -- user that adds to the task
        group_id -- id of new group

        Raises:
            NotExistingObjectError -- when the task or the current user is None
            NotFoundObjectError -- when the group wasn't found by such id
            ObjectAlreadyInListError -- when the group with such id is already in the list
            TypeError -- when the current_user or the task aren't the objects of the expected types

        """

        try:
            # If task_id isn't the id of the Task object, raise the error
            cur_task = self.storage.find_task_by_id(task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed task_id isn't the id of the Task object")

        # Find group by id
        found_group = self.storage.find_group_by_id(group_id)

        groups_ids = cur_task.groups_list

        # If the group is already in this task, raise the error
        if found_group.id in groups_ids:
            raise exceptions.ObjectAlreadyInListError('Group', 'Task')

        # Add connection between task and group
        cur_task.add_group(found_group.id)

        # Change in the db
        self.storage.change_task_in_db(cur_task)

        # Log the result
        self.logger.info("The group with id '{}' was successfully added to the task with id '{}'"
                         .format(group_id, cur_task.id))

        return cur_task

    @check_if_object_user
    @write_log
    def delete_group_from_task(self, task_id, user_id, group_id):
        """Deletes group from the task

        Keyword arguments:
        task_id -- task to be deleted from
        user_id -- user that deletes from the task
        group_id -- id of deleting group

        Raises:
            NotExistingObjectError -- when the task or the current user is None
            NotFoundObjectError -- when the group wasn't found by such id
            ObjectNotInListError -- when the group with such id isn't in the list
            TypeError -- when the current_user or the task aren't the objects of the expected types

        """

        try:
            # If task_id isn't the id of the Task object, raise the error
            cur_task = self.storage.find_task_by_id(task_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed task_id isn't the id of the Task object")

        # Find group by id
        found_group = self.storage.find_group_by_id(group_id)

        groups_ids = cur_task.groups_list

        # If the group is not in this task, raise the error
        if found_group.id not in groups_ids:
            raise exceptions.ObjectNotInListError('Group', 'Task')

        # Remove information
        cur_task.remove_group(found_group.id)

        # Change in the db
        self.storage.change_task_in_db(cur_task)

        # Log the result
        self.logger.info("The group with id '{}' was successfully deleted from task with id '{}'"
                         .format(group_id, cur_task.id))

        return cur_task
