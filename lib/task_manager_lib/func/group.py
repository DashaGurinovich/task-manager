"""This module includes the main logic for the Group entities

The data storage can be passed when the manager object is initialized. If the storage wasn't passed,
the library uses json-storage. The passed storage should contain such methods as clear(), get_*_from_db(),
add_*_to_db(new_object), change_*_in_db(changed_object), delete_*_from_db(object_id) where * is group, task and
user. Otherwise the AttributeError will be raised while trying to perform some managers functions. For example:

>>> import os
>>> import lib.task_manager_lib.db
>>> import lib.task_manager_lib.db.json
>>> import lib.task_manager_lib.func.group

>>> path = os.path.expanduser('~/.TaskManager/data')
>>> json_storage = task_manager_lib.db.json.manager.JsonDataManager(path)
>>> storage = task_manager_lib.db.manager.DataManager(json_storage)
>>> group_manager = task_manager_lib.func.group.GroupManager(storage)
>>> group = group_manager.create_user('Group name', user)

The functions that can be performed with the group:
create group, change group, delete group, add/delete user from group
"""

from task_manager_lib import exceptions
from task_manager_lib.db.manager import DataManager
from task_manager_lib.db.json.manager import JsonDataManager
from task_manager_lib.entities import group, user
from task_manager_lib.entities.permissions import check_if_object_user
from task_manager_lib.func.connector import DataConnector
from task_manager_lib.logger import get_logger, write_log


class GroupManager:
    """Performs different actions with the Group objects

    Fields:
    storage -- the DataManager object, that's used to work with the db
    logger -- the app logger
    """

    def __init__(self, storage=None, storage_path=None):
        """Initializes the object

        Keyword arguments:
        storage -- the storage to save data (if storage_path is passed too, the passed storage will be used
        not default json-storage)
        storage_path -- the path where to store data with default json-storage
        """

        self.logger = get_logger()

        if storage is None and storage_path is None:
            raise exceptions.NotPassedParameterError()

        if storage and not isinstance(storage, DataManager):
            raise TypeError("The storage isn't of the type DataManager")

        if storage:
            self.storage = storage
            return

        self.storage = DataManager(JsonDataManager(storage_path))

    @write_log
    def create_group(self, name, user_id, group_id=None):
        """ Creates the group and stores in the database

        Keyword arguments:
        name -- name of new group
        user_id -- user to create task

        Raises:
            NotExistingObjectError -- when the current user is None
            TypeError -- when the current user isn't the User object

        Example:
            >>>import task_manager_lib.task_manager_lib.func.group
            >>>gr = task_manager_lib.task_manager_lib.func.group.GroupManager(storage)
            >>>gr.create_group("Group name", the_user_object)
        """

        # If the user doesn't exist, raise the error
        if user_id is None:
            raise exceptions.NotExistingObjectError(user.User)

        try:
            # If user isn't the User object, raise the error
            _ = self.storage.find_user_by_id(user_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed user_id isn't the id of the User object")

        # Create Group instance
        new_group = group.Group(name, group_id=group_id)

        # Add connections between group and current_user
        new_group.add_user(user_id)

        # Add the group in the db
        self.storage.add_group_to_db(new_group)

        # Log the result
        self.logger.info("The group with name '{}' was created under {} id".format(new_group.name, new_group.id))

        # Return created group
        return new_group

    @check_if_object_user
    @write_log
    def change_group(self, group_id, user_id, new_name=None):
        """Makes changes in the attributes of the passed group, returns the changed group and changes the group
        in the database

        Keyword arguments:
        group_id -- group to be changed
        user_id -- user that changes the group
        new_name -- new group name

        Raises:
            NotExistingObjectError -- when the group or the current user is None
            TypeError -- when the current_user, the group or new params aren't the objects of the expected types

        Example:
            >>>import task_manager_lib.task_manager_lib.func.group
            >>>gr = task_manager_lib.task_manager_lib.func.group.GroupManager(storage)
            >>>gr.change_group(the_group_id, the_user_id, new_name="New group name")
        """

        try:
            # If group_id isn't the id of the Group object, raise the error
            changing_group = self.storage.find_group_by_id(group_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed group_id isn't the id of the Group object")

        if new_name:
            changing_group = self._change_name(changing_group, new_name)

        self.storage.change_group_in_db(changing_group)

        return changing_group

    @write_log
    def _change_name(self, changed_group, new_name):
        """Changes the group name. Returns the changed group. The object isn't saved in the db.

        Keyword arguments:
        changed_group -- group to be changed
        new_name -- new group name
        """

        # Change name
        changed_group.name = new_name

        # Log the result
        self.logger.info("The name of the group with id '{}' was changed to {}".format(changed_group.id,
                                                                                       changed_group.name))

        return changed_group

    @check_if_object_user
    @write_log
    def delete_group(self, group_id, user_id):
        """Deletes the group

        Keyword arguments:
        del_group -- group to be deleted
        current_user -- user that deletes the group

        Raises:
            NotExistingObjectError -- when the group or the current user is None
            TypeError -- when the current_user or the group aren't the objects of the expected types
        """

        try:
            # If group_id isn't the id of the Group object, raise the error
            _ = self.storage.find_group_by_id(group_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed group_id isn't the id of the Group object")

        # Get all group tasks
        tasks = DataConnector(self.storage).get_group_tasks(group_id)

        # Remove info about group from tasks
        # and save it in the db
        for task in tasks:
            task.remove_group(group_id)
            self.storage.change_task_in_db(task)

        # Delete group from db
        self.storage.delete_group_from_db(group_id)

        # Log the result
        self.logger.info("The group with id '{}' was successfully deleted".format(group_id))

    @check_if_object_user
    @write_log
    def add_user_to_group(self, group_id, user_id, user_login):
        """Adds user to the group

        Keyword arguments:
        group_id -- group to be added to
        user_id -- user that adds to the group
        user_login -- login of new user

        Raises:
            NotExistingObjectError -- when the group or the current user is None
            NotFoundObjectError -- when the user wasn't found by such login
            ObjectAlreadyInListError -- when the user with such login is already in the list
            TypeError -- when the current_user or the group aren't the objects of the expected types
        """

        try:
            # If group_id isn't the id of the Group object, raise the error
            cur_group = self.storage.find_group_by_id(group_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed group_id isn't the id of the Group object")

        # Find user_id object by login
        found_user = self.storage.find_user_by_login(user_login)

        users_ids = cur_group.users_list

        # If the user_id is already in this group, raise the error
        if found_user.id in users_ids:
            raise exceptions.ObjectAlreadyInListError(user.User, group.Group)

        # Add a connection between group and user_id
        cur_group.add_user(found_user.id)

        # Save to the db
        self.storage.change_group_in_db(cur_group)

        # Log the result
        self.logger.info("The user with login '{}' was successfully added to group with id '{}'".format(
            user_login, cur_group.id))

        return cur_group

    @check_if_object_user
    @write_log
    def delete_user_from_group(self, group_id, user_id, user_login):
        """Deletes user from the group

        Keyword arguments:
        group_id -- group to be deleted from
        user_id -- user that deletes from the group
        user_login -- login of deleting user

        Raises:
            NotExistingObjectError -- when the group or the current user is None
            NotFoundObjectError -- when the user wasn't found by such login
            ObjectNotInListError -- when the user with such login isn't in the list
            OnlyUserDeletionError -- when the only user is trying to be deleted
            TypeError -- when the current_user or the group aren't the objects of the expected types
        """

        try:
            # If group_id isn't the id of the Group object, raise the error
            cur_group = self.storage.find_group_by_id(group_id)
        except exceptions.NotFoundObjectError:
            raise TypeError("The passed group_id isn't the id of the Group object")

        # Find user_id object by login
        found_user = self.storage.find_user_by_login(user_login)

        users_ids = cur_group.users_list

        # If he isn't a group user_id, raise the error
        if found_user.id not in users_ids:
            raise exceptions.ObjectNotInListError(user.User, group.Group)

        # If he's the only user_id, he won't be deleted
        if len(users_ids) == 1:
            raise exceptions.OnlyUserDeletionError(group.Group)

        # Remove information between group and user_id
        cur_group.remove_user(found_user.id)

        # Save to db
        self.storage.change_group_in_db(cur_group)

        # Log the result
        self.logger.info("The user with login '{}' was successfully deleted from group with id '{}'"
                         .format(user_login, cur_group.id))

        return cur_group
