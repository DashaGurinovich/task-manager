"""This package includes enums and classes that represent the main entities of this library

    The tasks library has its own entities like every library. The main entities are group, task and user.

    ---TASK---
    Task entity is a task that a user wants to complete. The task necessarily has a name, it can have a definition,
    percentage of completion when the task was started. The task also has such characteristics as priority and status.
    The priorities are NO_PRIORITY, LOW, AVERAGE and HIGH. The user can select the priority that he thinks suites
    the task more. The statuses are CREATED (used when the task was created and the completion of it wasn't started),
    IN_PROCESS (used when the task completion of started), FINISHED (used when the task was completed) and NOT_RELEVANT
    (used when the task completion isn't actual any more). Task also knows when it was created and finished,

    Tasks can have sub tasks and dependent tasks. Sub tasks are the tasks that should be completed to
    complete the main task. When the main task is finished, it means that all sub tasks are finished too.
    Dependent tasks are the tasks the completion of that can't be started while the main task isn't completed.
    For example, you can't read a book if you haven't get it. So the main task is to get a book. The dependent task
    is to read a book.

    Task can be periodic. What does it mean? It means that the user should complete the task periodically (every day,
    every six hours). For example, go the gym every two days at 7 p.m.

    ---GROUP---
    Group entity is a group that unites some tasks. The task can be in one group, in several groups or in no groups
    at all. The only group characteristic is its name. For example, you can store work tasks in the group with the
    name Work.

    ---User---
    User entity is a user that works with tasks and their groups. The user has two necessary parameters: login and
    email. These parameters should be unique. That means that you can create two users with the same logins or emails.
    The user has such optional parameters as first name, last name and the date of birth.

    Modules:
    group -- includes the class that represents the Group
    permissions -- includes the decorator that checks the users permissions (will have the Permissions class
    in the future versions)
    task -- includes the class that represents the task, also includes Status and Priority enums for the tasks
    user -- includes the class that represents the User
"""
