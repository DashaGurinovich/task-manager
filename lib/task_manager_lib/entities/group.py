"""This module includes the class that represents the Group
Group entity is a group that unites some tasks. The task can be in one group, in several groups or in no groups
at all. The only group characteristic is its name. For example, you can store work tasks in the group with the
name Work.
"""
import uuid


class Group:
    """ Class that represents the Group entity

    Fields:
    name -- name of the group
    id -- identifier of the group
    users_list -- ids of users of the current group

    Keyword arguments to initialize an instance:
    name -- name of the group

    Example:
        >>> import task_manager_lib.task_manager_lib.entities.group
        >>> gr = task_manager_lib.task_manager_lib.entities.group.Group("Group name")
    """

    def __init__(self, name, group_id=None):
        self.name = name

        if group_id is None:
            self.id = uuid.uuid1()
        else:
            self.id = group_id

        self.users_list = []

    def __eq__(self, other):
        return self.id == other.id

    def add_user(self, adding_user_id):
        """Adds the user to the group list

        Keyword arguments:
            adding_user_id -- id of the adding user
        """
        self.users_list.append(adding_user_id)

    def remove_user(self, removing_user_id):
        """Removes the user from the group list

        Keyword arguments:
            removing_user_id -- id of the removing user
        """
        self.users_list.remove(removing_user_id)
