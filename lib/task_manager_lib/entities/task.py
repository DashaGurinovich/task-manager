"""This module includes the Task class that represents the task entity. It also has
Status and Priority enums to help working with the task entities

Task entity is a task that a user wants to complete. The task necessarily has a name, it can have a definition,
percentage of completion when the task was started. The task also has such characteristics as priority and status.
The priorities are NO_PRIORITY, LOW, AVERAGE and HIGH. The user can select the priority that he thinks suites
the task more. The statuses are CREATED (used when the task was created and the completion of it wasn't started),
IN_PROCESS (used when the task completion of started), FINISHED (used when the task was completed) and NOT_RELEVANT
(used when the task completion isn't actual any more). Task also knows when it was created and finished,

Tasks can have sub tasks and dependent tasks. Sub tasks are the tasks that should be completed to
complete the main task. When the main task is finished, it means that all sub tasks are finished too.
Dependent tasks are the tasks the completion of that can't be started while the main task isn't completed.
For example, you can't read a book if you haven't get it. So the main task is to get a book. The dependent task
is to read a book.

Task can be periodic. What does it mean? It means that the user should complete the task periodically (every day,
every six hours). For example, go the gym every two days at 7 p.m.
"""

import uuid
from datetime import datetime
from enum import Enum


class Priority(Enum):
    """ Enum for task priorities

    Priorities: NO_PRIORITY, LOW. AVERAGE and HIGH
    """
    NO_PRIORITY = 0
    LOW = 1
    AVERAGE = 2
    HIGH = 3


class Status(Enum):
    """ Enum for task statuses

    Statuses: CREATED (used when the task was created and the completion of it wasn't started),
    IN_PROCESS (used when the task completion of started), FINISHED (used when the task was completed) and NOT_RELEVANT
    (used when the task completion isn't actual any more)
    """
    CREATED = 0
    IN_PROCESS = 1
    FINISHED = 2
    NOT_RELEVANT = 3


class Task:
    """ Class that represents Task entity

    Fields:
    name -- name of the task
    id -- identifier of the task
    definition -- task definition
    creation_date -- date when the task was created
    finishing_date -- date when the task was finished
    period_start_date -- date when period starts
    period -- period of the task
    period_end_date -- date when period ends
    completion_percentage -- task percentage of completion
    parent -- id of the parent task
    groups_list -- ids of groups that current task is in
    users_list -- ids of users of the current task
    status -- status of the task
    priority -- priority of the task
    is_blocked -- if completion of the task is blocked by other task

    Keyword arguments to initialize an instance:
    name -- name of the task

    Example:
        >>> import task_manager_lib.task_manager_lib.entities.task
        >>> t = task_manager_lib.task_manager_lib.entities.task.Task("Task name")
    """
    def __init__(self, name, task_id=None, definition="", deadline=None, period_start_date=None,
                 period=None, period_end_date=None, completion_percentage=0.0, status=None, priority=None):
        self.name = name

        if task_id is None:
            self.id = uuid.uuid1()
        else:
            self.id = task_id

        self.definition = definition
        self.creation_date = datetime.now()
        self.finishing_date = None
        self.deadline = deadline
        self.period_start_date = period_start_date
        self.period = period
        self.period_end_date = period_end_date
        self.completion_percentage = completion_percentage
        self.groups_list = []
        self.users_list = []
        self.parent = None
        self.is_blocked = False

        if status is None:
            self.status = Status.CREATED
        else:
            self.status = status

        if priority is None:
            self.priority = Priority.NO_PRIORITY
        else:
            self.priority = priority

    def __eq__(self, other):
        return self.id == other.id

    # region Working with task groups
    def add_group(self, adding_group_id):
        """Adds the group to the task list

        Keyword arguments:
            adding_group_id -- id of the adding group
        """
        self.groups_list.append(adding_group_id)

    def remove_group(self, removing_group_id):
        """Removes the group from the task list

        Keyword arguments:
            removing_group_id -- id of the removing group
        """
        if removing_group_id in self.groups_list:
            self.groups_list.remove(removing_group_id)
    # endregion

    # region Working with task users
    def add_user(self, adding_user_id):
        """Adds the user to the task list

        Keyword arguments:
            adding_user_id -- id of the adding task
        """
        self.users_list.append(adding_user_id)

    def remove_user(self, removing_user_id):
        """Removes the user from the task list

        Keyword arguments:
            removing_user_id -- id of the removing user
        """
        if removing_user_id in self.users_list:
            self.users_list.remove(removing_user_id)
    # endregion

    # region Working with task parent
    def add_parent(self, parent_id):
        """Adds the task parent

        Keyword arguments:
            parent_id -- id of the parent task
        """
        if parent_id != self.id:
            self.parent = parent_id
    # endregion

    def change_task_status(self, status):
        """Changes the task status if the task isn't blocked

        If the new status is FINISHED, percentage equals 100 and the date of finishing is added
        """
        # If task is not blocked
        if self.is_blocked:
            return

        self.status = status

        # If status is FINISHED,
        # make percentage of completion 100
        # and add date of finishing
        if self.status == Status.FINISHED:
            self.completion_percentage = 100
            self.finishing_date = datetime.now()

    def change_task_deadline(self, deadline_date):
        """Changes the task deadline

         If deadline is sooner then period end date, then period end date equals deadline
         If deadline was before time now, then the task is not actual
        """
        if deadline_date is None:
            self.deadline = None
            return

        self.deadline = deadline_date

        # If deadline is sooner then period end date,
        # then period end date equals deadline
        if self.period_end_date is not None and self.period_end_date > self.deadline:
            self.period_end_date = self.deadline

        # If deadline was before time now,
        # then the task is not actual
        if self.deadline < datetime.now():
            self.status = Status.NOT_RELEVANT

    def change_completion_percentage(self, percentage):
        """Changes the task percentage of completion if the task isn't blocked

        If the percentage is 100, than the task is finished and the date of finishing is added
        """

        # If the task is not blocked
        if self.is_blocked:
            return

        self.completion_percentage = percentage
        self.status = Status.IN_PROCESS

        # If the percentage is 100, than the task is finished
        # and the date of finishing is added
        if percentage == 100:
            self.status = Status.FINISHED
            self.finishing_date = datetime.now()

    def change_period_start_date(self, period_start_date):
        """Changes the task period start date

        If new period_start_date is None, then period and period_end_date get deleted
        """

        # If there's no period_start_date, there's no period
        if period_start_date is None:
            self.period_start_date = None
            self.period = None
            self.period_end_date = None
            return

        self.period_start_date = period_start_date

    def change_period_end_date(self, period_end_date):
        """Changes the task period end date if there's period start date and period"""
        if period_end_date is None:
            self.period_end_date = None
            return

        # Can't set period_end_date if there's no period
        elif self.period is None:
            return

        if self.deadline is not None and period_end_date > self.deadline:
            self.period_end_date = self.deadline
        else:
            self.period_end_date = period_end_date
