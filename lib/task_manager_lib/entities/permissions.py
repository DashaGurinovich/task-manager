"""This module includes the decorator that checks the user permissions.
In the future versions the Permissions class will be created
"""

from functools import wraps

from task_manager_lib.entities import group, task, user
from task_manager_lib.exceptions import NotExistingObjectError, PermissionsError, NotFoundObjectError
from task_manager_lib.func.connector import DataConnector


def check_if_object_user(func):
    """ Decorator that checks if the user can modify or delete objects

    Keyword arguments:
    func -- function to decorate

    The expected func structure:
    func_name(manager_class, obj_id, user_id, ...), where
    manager_class -- the class with attribute storage that represents the data storage
    obj_id -- id of the Group or Task object to check user permissions for
    user_id -- the user that's trying to perform an action with the object

    Raises:
        NotExistingObjectError -- when the object or the user wasn't passed
        PermissionsError -- when the user doesn't have a permission to work with the object
        TypeError -- when one of the passed arguments isn't of the expected type
    """

    @wraps(func)
    def wrapper(self, obj_id, user_id, *args, **kwargs):

        # If object is None, raise the error
        if obj_id is None:
            raise NotExistingObjectError('Task or Group')

        # If user_id doesn't exist, raise the error
        if user_id is None:
            raise NotExistingObjectError(user.User)

        try:
            connector = DataConnector(self.storage)
        except (AttributeError, TypeError):
            raise TypeError("The class object doesn't have attribute storage")

        try:
            # If user isn't the User object, raise the error
            current_user = self.storage.find_user_by_id(user_id)
        except NotFoundObjectError:
            raise TypeError("The passed user_id isn't the id of the User object")

        # If the object wasn't found in the tasks or groups, raise the error
        try:
            obj = self.storage.find_task_by_id(obj_id)
        except NotFoundObjectError:
            try:
                obj = self.storage.find_group_by_id(obj_id)
            except NotFoundObjectError:
                raise TypeError("The passed obj_id isn't the id of the Group or Task object")
        
        if isinstance(obj, task.Task):
            users = connector.get_task_users(obj)
        elif isinstance(obj, group.Group):
            users = connector.get_group_users(obj)
        else:
            raise TypeError("The object is not of type Group or Task")

        # When the user was found, check his permission for the object
        is_object_user = False
        for u in users:
            if user_id == u.id:
                is_object_user = True
                break

        # If user has permission, execute func
        if is_object_user:
            return func(self, obj_id, user_id, *args, **kwargs)

        # else raise the permissions error
        else:
            raise PermissionsError(current_user.login)

    return wrapper


class Permissions:
    def __init__(self, obj_id, user_id):
        self.obj_id = obj_id
        self.user_id = user_id
