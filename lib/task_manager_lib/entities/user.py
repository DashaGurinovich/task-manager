"""This module includes the User class
User entity is a user that works with tasks and their groups. The user has two necessary parameters: login and
email. These parameters should be unique. That means that you can create two users with the same logins or emails.
The user has such optional parameters as first name, last name and the date of birth.

"""

import uuid


class User:
    """ Class that represents User entity

    Fields:
    login -- login of the user
    id -- identifier of the user
    email -- user email
    first_name -- first name
    last_name -- last name
    date_of_birth -- date of birth

    Keyword arguments to initialize an instance:
    login -- login of the user
    email -- user email

    Example:
        >>> import task_manager_lib.task_manager_lib.entities.user
        >>> u = task_manager_lib.task_manager_lib.entities.user.User("User login", "User email")
    """
    def __init__(self, login, email, user_id=None, first_name=None, last_name=None, date_of_birth=None):
        self.login = login
        self.email = email

        if user_id is None:
            self.id = uuid.uuid1()
        else:
            self.id = user_id

        if first_name is None:
            self.first_name = ""
        else:
            self.first_name = first_name

        if last_name is None:
            self.last_name = ""
        else:
            self.last_name = last_name

        self.date_of_birth = date_of_birth

    def __eq__(self, other):
        return self.id == other.id
