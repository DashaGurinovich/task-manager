import os
import shutil
import unittest

from task_manager_lib import exceptions
from task_manager_lib.db.json.manager import JsonDataManager
from task_manager_lib.db.manager import DataManager
from task_manager_lib.func import group as group_func, task as task_func, user as user_func
from task_manager_lib.func.connector import DataConnector
from task_manager_lib.logger import disable_logger, enable_logger


class TestUserFunctions(unittest.TestCase):
    def setUp(self):
        disable_logger()

        # Create temporary dir to store tests data
        self.temp_dir = os.path.join(os.path.abspath(os.curdir), 'temp_dir')
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir, ignore_errors=True)
        os.mkdir(self.temp_dir)

        self.storage = DataManager(JsonDataManager(self.temp_dir))

        self.group_manager = group_func.GroupManager(self.storage)
        self.user_manager = user_func.UserManager(self.storage)
        self.task_manager = task_func.TaskManager(self.storage)
        self.obj_getter = DataConnector(self.storage)

        self.user = self.user_manager.create_user("user_login", "user_email")

    # region User creation tests
    def test_create_user_with_unique_login_and_email(self):
        user = self.user_manager.create_user("new_user_login", "new_user_email")
        created_user = self.user_manager.storage.find_user_by_id(user.id)

        self.assertIsNotNone(created_user)
        self.assertEqual(created_user.login, "new_user_login")
        self.assertEqual(created_user.email, "new_user_email")

    def test_create_user_with_existing_login_and_email(self):
        self.assertRaises(exceptions.ExistingUserError, self.user_manager.create_user, "user_login", "user_email")
        self.assertRaises(exceptions.ExistingUserError, self.user_manager.create_user, "another_user_login",
                          "user_email")
        self.assertRaises(exceptions.ExistingUserError, self.user_manager.create_user, "user_login",
                          "another_user_email")

    def test_create_user_with_unique_login_and_email_and_extra_info(self):
        user = self.user_manager.create_user("new_user_login", "new_user_email", user_id=1,
                                             first_name="first_name", last_name="last_name", date_of_birth="01-01-2000")
        created_user = self.user_manager.storage.find_user_by_id(user.id)

        self.assertIsNotNone(created_user)
        self.assertEqual(created_user.login, "new_user_login")
        self.assertEqual(created_user.email, "new_user_email")
        self.assertEqual(created_user.first_name, "first_name")
        self.assertEqual(created_user.last_name, "last_name")
        self.assertEqual(created_user.date_of_birth.strftime("%d-%m-%Y"), "01-01-2000")

    def test_create_user_with_unique_login_and_email_and_not_valid_date_of_birth(self):
        self.assertRaises(ValueError, self.user_manager.create_user, "new_user_login", "new_user_email", 1,
                          "first_name", "last_name", "465123")
    # endregion

    # region User change tests
    def test_change_user_login_to_unique_login(self):
        self.user_manager.change_user(self.user.id, login="new_change_login")

        changed_user = self.user_manager.storage.find_user_by_id(self.user.id)

        self.assertIsNotNone(changed_user)
        self.assertEqual(changed_user.login, "new_change_login")

    def test_change_user_login_to_existing_login(self):
        another_user = self.user_manager.create_user("another_change_login", "another_change_email")

        self.assertIsNotNone(another_user)
        self.assertRaises(exceptions.ExistingUserError, self.user_manager.change_user, self.user.id,
                          login="another_change_login")

        changed_user = self.user_manager.storage.find_user_by_id(self.user.id)

        self.assertIsNotNone(changed_user)
        self.assertEqual(changed_user.login, "user_login")

    def test_change_user_email_to_unique_email(self):
        self.user_manager.change_user(self.user.id, email="new_change_email")

        changed_user = self.user_manager.storage.find_user_by_id(self.user.id)

        self.assertIsNotNone(changed_user)
        self.assertEqual(changed_user.email, "new_change_email")

    def test_change_user_email_to_existing_email(self):
        another_user = self.user_manager.create_user("another_change_login", "another_change_email")
        self.assertIsNotNone(another_user)

        self.assertRaises(exceptions.ExistingUserError, self.user_manager.change_user, self.user.id,
                          email="another_change_email")

        changed_user = self.user_manager.storage.find_user_by_id(self.user.id)

        self.assertIsNotNone(changed_user)
        self.assertEqual(changed_user.email, "user_email")

    def test_change_user_first_name(self):
        self.user_manager.change_user(self.user.id, first_name="new_first_name")

        changed_user = self.user_manager.storage.find_user_by_id(self.user.id)

        self.assertIsNotNone(changed_user)
        self.assertEqual(changed_user.first_name, "new_first_name")

    def test_change_user_last_name(self):
        self.user_manager.change_user(self.user.id, last_name="new_last_name")

        changed_user = self.user_manager.storage.find_user_by_id(self.user.id)

        self.assertIsNotNone(changed_user)
        self.assertEqual(changed_user.last_name, "new_last_name")

    def test_change_user_date_of_birth(self):
        self.user_manager.change_user(self.user.id, date_of_birth="31-12-1999")

        changed_user = self.user_manager.storage.find_user_by_id(self.user.id)

        self.assertIsNotNone(changed_user)
        self.assertEqual(changed_user.date_of_birth.strftime("%d-%m-%Y"), "31-12-1999")

    def test_change_user_date_of_birth_to_not_valid_date(self):
        self.assertRaises(exceptions.NotValidParameterError, self.user_manager.change_user, self.user.id,
                          date_of_birth="12345")

        changed_user = self.user_manager.storage.find_user_by_id(self.user.id)

        self.assertIsNotNone(changed_user)
        self.assertIsNone(changed_user.date_of_birth)
    # endregion

    # region User deletion tests
    def test_delete_user_without_tasks_and_groups(self):
        self.user_manager.delete_user(self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.user_manager.storage.find_user_by_id, self.user.id)

    def test_delete_user_with_tasks_and_groups_that_have_only_one_user(self):
        group = self.group_manager.create_group("group", self.user.id)
        task = self.task_manager.create_task("task", self.user.id)

        self.user_manager.delete_user(self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.user_manager.storage.find_user_by_id, self.user.id)
        self.assertRaises(exceptions.NotFoundObjectError, self.group_manager.storage.find_group_by_id, group.id)
        self.assertRaises(exceptions.NotFoundObjectError, self.task_manager.storage.find_task_by_id, task.id)

    def test_delete_user_with_tasks_and_groups_that_have_several_users(self):
        second_user = self.user_manager.create_user("login", "email")
        group = self.group_manager.create_group("group", self.user.id)
        task = self.task_manager.create_task("task", self.user.id)

        self.group_manager.add_user_to_group(group.id, self.user.id, second_user.login)
        self.task_manager.add_user_to_task(task.id, self.user.id, second_user.login)

        self.user_manager.delete_user(self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.user_manager.storage.find_user_by_id, self.user.id)

        group = self.storage.find_group_by_id(group.id)
        task = self.storage.find_task_by_id(task.id)

        self.assertIsNotNone(group)
        self.assertIsNotNone(task)
        self.assertTrue(self.user.id not in group.users_list)
        self.assertTrue(self.user.id not in task.users_list)
        self.assertTrue(second_user.id in group.users_list)
        self.assertTrue(second_user.id in task.users_list)

    # endregion

    def tearDown(self):
        self.storage.clear()

        shutil.rmtree(self.temp_dir, ignore_errors=True)
        enable_logger()


if __name__ == '__main__':
    unittest.main()
