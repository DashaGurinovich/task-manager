import os
import shutil
import unittest

from dateutil.relativedelta import relativedelta

from task_manager_lib import exceptions
from task_manager_lib.db.json.manager import JsonDataManager
from task_manager_lib.db.manager import DataManager
from task_manager_lib.entities.task import Status, Priority
from task_manager_lib.func import group as group_func, task as task_func, user as user_func
from task_manager_lib.func.connector import DataConnector
from task_manager_lib.logger import disable_logger, enable_logger


class TestTaskFunctions(unittest.TestCase):
    def setUp(self):
        disable_logger()

        # Create temporary dir to store tests data
        self.temp_dir = os.path.join(os.path.abspath(os.curdir), 'temp_dir')
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir, ignore_errors=True)
        os.mkdir(self.temp_dir)

        self.storage = DataManager(JsonDataManager(self.temp_dir))

        self.group_manager = group_func.GroupManager(self.storage)
        self.user_manager = user_func.UserManager(self.storage)
        self.task_manager = task_func.TaskManager(self.storage)
        self.obj_getter = DataConnector(self.storage)

        self.user = self.user_manager.create_user("task_user_login", "task_user_email")
        self.task = self.task_manager.create_task("task_name", self.user.id)

    # region Task creation tests
    def test_create_task(self):
        new_task = self.task_manager.create_task("task_name", self.user.id)

        found_user = self.storage.find_user_by_id(self.user.id)
        found_task = self.storage.find_task_by_id(new_task.id)
        user_tasks = self.obj_getter.get_user_tasks(self.user.id)

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.name, "task_name")
        self.assertTrue(found_user.id in found_task.users_list)
        self.assertTrue(found_task in user_tasks)

    def test_create_task_with_extra_info(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, deadline="15-15 20-06-2019",
                                                 status=2, priority=2)

        found_user = self.storage.find_user_by_id(self.user.id)
        found_task = self.storage.find_task_by_id(new_task.id)
        user_tasks = self.obj_getter.get_user_tasks(self.user.id)
        group_tasks = self.obj_getter.get_group_tasks(group.id)

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.name, "task_name")
        self.assertEqual(found_task.deadline.strftime("%H-%M %d-%m-%Y"), "15-15 20-06-2019")
        self.assertEqual(found_task.status, Status.FINISHED)
        self.assertEqual(found_task.priority, Priority.AVERAGE)

        self.assertTrue(found_user.id in found_task.users_list)
        self.assertTrue(found_task in user_tasks)
        self.assertTrue(found_task in group_tasks)

    def test_create_task_with_not_valid_status(self):
        tasks_number = len(self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.create_task, "task_name", self.user.id,
                          status="118")

        new_tasks_number = len(self.obj_getter.get_user_tasks(self.user.id))
        self.assertEqual(new_tasks_number, tasks_number)

    def test_create_task_with_not_valid_priority(self):
        tasks_number = len(self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.create_task, "task_name", self.user.id,
                          priority="118")

        new_tasks_number = len(self.obj_getter.get_user_tasks(self.user.id))
        self.assertEqual(new_tasks_number, tasks_number)

    def test_create_task_with_not_valid_deadline(self):
        tasks_number = len(self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.create_task, "task_name", self.user.id,
                          deadline="118")

        new_tasks_number = len(self.obj_getter.get_user_tasks(self.user.id))
        self.assertEqual(new_tasks_number, tasks_number)

    def test_create_task_with_not_existing_user(self):
        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.create_task, "task_name", None)
    # endregion

    # region Make task a periodic task tests
    def test_make_task_periodic(self):
        periodic_task = self.task_manager.make_task_periodic(self.task.id, self.user.id, period_start_date="13-09-2019",
                                                             period=relativedelta(days=2), period_end_date="14-11-2019")

        self.assertEqual(periodic_task.id, self.task.id)
        self.assertEqual(periodic_task.period_start_date.strftime("%H-%M %d-%m-%Y"), "00-00 13-09-2019")
        self.assertEqual(periodic_task.period, relativedelta(days=2))
        self.assertEqual(periodic_task.period_end_date.strftime("%H-%M %d-%m-%Y"), "00-00 14-11-2019")

        self.assertTrue(periodic_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in periodic_task.users_list)

    def test_make_task_periodic_with_not_valid_period(self):
        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.make_task_periodic, self.task.id,
                          self.user.id, period_start_date="13-09-2019", period=2, period_end_date="14-11-2019")

        self.assertIsNone(self.task.period_start_date)
        self.assertIsNone(self.task.period)
        self.assertIsNone(self.task.period_end_date)

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in self.task.users_list)

    def test_make_task_periodic_with_not_valid_period_dates(self):
        self.assertRaises(exceptions.NotValidPeriodDatesError, self.task_manager.make_task_periodic, self.task.id,
                          self.user.id, period_start_date="13-09-2019", period=relativedelta(days=2),
                          period_end_date="14-11-2018")

        self.assertIsNone(self.task.period_start_date)
        self.assertIsNone(self.task.period)
        self.assertIsNone(self.task.period_end_date)

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in self.task.users_list)
    # endregion

    # region Task management with groups tests
    def test_add_group_to_task(self):
        group = self.group_manager.create_group("group", self.user.id)
        task = self.task_manager.add_group_to_task(self.task.id, self.user.id, group.id)

        self.assertTrue(self.task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(group.id in task.groups_list)

    def test_add_task_group_to_task(self):
        group = self.group_manager.create_group("group", self.user.id)
        new_task = self.task_manager.create_task("task", self.user.id, group.id)

        self.assertRaises(exceptions.ObjectAlreadyInListError, self.task_manager.add_group_to_task, new_task.id,
                          self.user.id, group.id)

        self.assertTrue(new_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(group.id in new_task.groups_list)

    def test_delete_group_from_task(self):
        group = self.group_manager.create_group("group", self.user.id)

        task = self.task_manager.add_group_to_task(self.task.id, self.user.id, group.id)

        self.assertTrue(task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(group.id in task.groups_list)

        self.task_manager.delete_group_from_task(self.task.id, self.user.id, group.id)

        self.assertTrue(self.task not in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(group.id not in self.task.groups_list)

    def test_delete_not_task_group_from_task(self):
        group = self.group_manager.create_group("group", self.user.id)

        self.assertRaises(exceptions.ObjectNotInListError, self.task_manager.delete_group_from_task, self.task.id,
                          self.user.id, group.id)

        self.assertTrue(self.task not in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(group.id not in self.task.groups_list)

    def test_delete_not_existing_group_from_task(self):
        groups_number = len(self.task.groups_list)

        self.assertRaises(exceptions.NotFoundObjectError, self.task_manager.delete_group_from_task, self.task.id,
                          self.user.id, "wrong_id")

        new_groups_number = len(self.task.groups_list)
        self.assertEqual(groups_number, new_groups_number)
    # endregion

    # region Task management with groups tests
    def test_add_user_to_task(self):
        second_user = self.user_manager.create_user("second_user_login", "second_user_email")

        task = self.task_manager.add_user_to_task(self.task.id, self.user.id, second_user.login)

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in task.users_list)
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(second_user.id))
        self.assertTrue(second_user.id in task.users_list)

    def test_add_task_user_to_task(self):
        self.assertRaises(exceptions.ObjectAlreadyInListError, self.task_manager.add_user_to_task, self.task.id,
                          self.user.id, self.user.login)

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in self.task.users_list)

    def test_delete_user_from_task(self):
        second_user = self.user_manager.create_user("second_user_login", "second_user_email")

        task = self.task_manager.add_user_to_task(self.task.id, self.user.id, second_user.login)

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in task.users_list)
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(second_user.id))
        self.assertTrue(second_user.id in task.users_list)

        self.task_manager.delete_user_from_task(self.task.id, self.user.id, second_user.login)

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in self.task.users_list)
        self.assertTrue(self.task not in self.obj_getter.get_user_tasks(second_user.id))
        self.assertTrue(second_user.id not in self.task.users_list)

    def test_delete_only_user_from_task(self):
        self.assertRaises(exceptions.OnlyUserDeletionError, self.task_manager.delete_user_from_task, self.task.id,
                          self.user.id, self.user.login)

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in self.task.users_list)

    def test_delete_not_task_user_from_task(self):
        second_user = self.user_manager.create_user("second_user_login", "second_user_email")

        self.assertRaises(exceptions.ObjectNotInListError, self.task_manager.delete_user_from_task, self.task.id,
                          self.user.id, second_user.login)

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in self.task.users_list)
        self.assertTrue(self.task not in self.obj_getter.get_user_tasks(second_user.id))
        self.assertTrue(second_user.id not in self.task.users_list)

    def test_delete_not_existing_user_from_task(self):
        self.assertRaises(exceptions.NotFoundObjectError, self.task_manager.delete_user_from_task, self.task.id,
                          self.user.id, "wrong_login")

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in self.task.users_list)
    # endregion

    # region Task sub tasks tests
    def test_create_sub_task(self):
        sub_task = self.task_manager.create_sub_task(self.task.id, self.user.id, "sub_task")

        self.assertIsNotNone(sub_task)
        self.assertEqual(sub_task.name, "sub_task")
        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_create_sub_task_with_not_existing_main_task(self):
        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.create_sub_task, None, self.user.id,
                          "sub_task")

    def test_create_sub_task_with_not_existing_user(self):
        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.create_sub_task, self.task.id, None,
                          "sub_task")

    def test_create_sub_task_with_not_task_user(self):
        second_user = self.user_manager.create_user("second_task_user_login", "second_task_user_email")

        self.assertRaises(exceptions.PermissionsError, self.task_manager.create_sub_task, self.task.id, second_user.id,
                          "sub_task")

    def test_change_sub_task_name(self):
        sub_task = self.task_manager.create_sub_task(self.task.id, self.user.id, "sub_task")

        self.assertIsNotNone(sub_task)
        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.task_manager.change_task(sub_task.id, self.user.id, name="new_sub_task")

        sub_task = self.obj_getter.get_task_sub_tasks(self.task.id)[0]

        self.assertEqual(sub_task.name, "new_sub_task")
        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_change_sub_task_name_with_not_existing_user(self):
        sub_task = self.task_manager.create_sub_task(self.task.id, self.user.id, "sub_task")

        self.assertIsNotNone(sub_task)
        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.change_task, sub_task.id, None,
                          {"name": "new_sub_task"})

        sub_task = self.obj_getter.get_task_sub_tasks(self.task.id)[0]

        self.assertEqual(sub_task.name, "sub_task")
        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_change_sub_task_name_with_not_task_user(self):
        second_user = self.user_manager.create_user("second_task_user_login", "second_task_user_email")

        sub_task = self.task_manager.create_sub_task(self.task.id, self.user.id, "sub_task")

        self.assertIsNotNone(sub_task)
        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.PermissionsError, self.task_manager.change_task, sub_task.id, second_user.id,
                          {"name": "new_sub_task"})

        sub_task = self.obj_getter.get_task_sub_tasks(self.task.id)[0]

        self.assertEqual(sub_task.name, "sub_task")

        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_delete_sub_task(self):
        sub_task = self.task_manager.create_sub_task(self.task.id, self.user.id, "sub_task")

        self.assertIsNotNone(sub_task)
        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.task_manager.delete_task(sub_task.id, self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.storage.find_task_by_id, sub_task.id)
        self.assertTrue(sub_task not in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task not in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_delete_sub_task_with_not_existing_user(self):
        sub_task = self.task_manager.create_sub_task(self.task.id, self.user.id, "sub_task")

        self.assertIsNotNone(sub_task)
        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.delete_task, sub_task.id, None)

        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
    # endregion

    # region Task dependent tasks tests
    def test_create_dependent_task(self):
        dependent_task = self.task_manager.create_dependent_task(self.task.id, self.user.id, "dependent_task")

        self.assertIsNotNone(dependent_task)
        self.assertTrue(dependent_task.is_blocked)
        self.assertEqual(dependent_task.name, "dependent_task")
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_create_dependent_task_with_not_existing_main_task(self):
        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.create_dependent_task, None,
                          self.user.id, "dependent_task")

    def test_create_dependent_task_with_not_existing_user(self):
        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.create_dependent_task, self.task.id,
                          None, "dependent_task")

    def test_create_dependent_task_with_not_task_user(self):
        second_user = self.user_manager.create_user("second_task_user_login", "second_task_user_email")

        self.assertRaises(exceptions.PermissionsError, self.task_manager.create_dependent_task, self.task.id,
                          second_user.id, "dependent_task")

    def test_change_dependent_task_name(self):
        dependent_task = self.task_manager.create_dependent_task(self.task.id, self.user.id, "dependent_task")

        self.assertIsNotNone(dependent_task)
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.task_manager.change_task(dependent_task.id, self.user.id, name="new_dependent_task")

        dependent_task = self.obj_getter.get_task_dependent_tasks(self.task.id)[0]

        self.assertEqual(dependent_task.name, "new_dependent_task")
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_change_dependent_task_name_with_not_existing_user(self):
        dependent_task = self.task_manager.create_dependent_task(self.task.id, self.user.id, "dependent_task")

        self.assertIsNotNone(dependent_task)
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.change_task, dependent_task.id, None,
                          {"name": "new_dependent_task"})

        dependent_task = self.obj_getter.get_task_dependent_tasks(self.task.id)[0]

        self.assertEqual(dependent_task.name, "dependent_task")
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_change_dependent_task_name_with_not_task_user(self):
        second_user = self.user_manager.create_user("second_task_user_login", "second_task_user_email")

        dependent_task = self.task_manager.create_dependent_task(self.task.id, self.user.id, "dependent_task")

        self.assertIsNotNone(dependent_task)
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.PermissionsError, self.task_manager.change_task, dependent_task.id, second_user.id,
                          {"name": "new_dependent_task"})

        dependent_task = self.obj_getter.get_task_dependent_tasks(self.task.id)[0]

        self.assertEqual(dependent_task.name, "dependent_task")
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_delete_dependent_task(self):
        dependent_task = self.task_manager.create_dependent_task(self.task.id, self.user.id, "dependent_task")

        self.assertIsNotNone(dependent_task)
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.task_manager.delete_task(dependent_task.id, self.user.id)

        self.assertIsNotNone(dependent_task)
        self.assertTrue(dependent_task not in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task not in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

    def test_delete_dependent_task_with_not_existing_user(self):
        dependent_task = self.task_manager.create_dependent_task(self.task.id, self.user.id, "dependent_task")

        self.assertIsNotNone(dependent_task)
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.delete_task, dependent_task.id, None)

        self.assertIsNotNone(dependent_task)
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
    # endregion

    # region Task change tests

    # region Change name tests
    def test_change_task_name(self):
        self.task_manager.change_task(self.task.id, self.user.id, name="new_task_name")

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.name, "new_task_name")
        self.assertEqual(user_task.name, "new_task_name")

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    def test_change_task_name_with_not_existing_user(self):
        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.change_task, self.task.id, None,
                          name="new_task_name")

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        found_task = self.storage.find_task_by_id(self.task.id)

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.name, "task_name")
        self.assertEqual(user_task.name, "task_name")

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    def test_change_task_name_with_not_task_user(self):
        another_user = self.user_manager.create_user("another_login", "another_email")

        self.assertRaises(exceptions.PermissionsError, self.task_manager.change_task, self.task.id, another_user.id,
                          name="new_task_name")

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.name, "task_name")
        self.assertEqual(user_task.name, "task_name")

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    # endregion

    # region Change definition tests
    def test_change_task_definition(self):
        self.task_manager.change_task(self.task.id, self.user.id, definition="new_task_definition")

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.definition, "new_task_definition")
        self.assertEqual(user_task.definition, "new_task_definition")

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    def test_change_task_definition_with_not_existing_user(self):

        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.change_task, self.task.id, None,
                          definition="new_task_definition")

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.definition, "")
        self.assertEqual(user_task.definition, "")

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)
    # endregion

    # region Change deadline tests
    def test_change_task_deadline(self):
        self.task_manager.change_task(self.task.id, self.user.id, deadline="0-0 15-06-2019")

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 15-06-2019")
        self.assertEqual(user_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 15-06-2019")

        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    def test_change_task_deadline_with_none(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, deadline="0-0 20-05-2019")

        self.task_manager.change_task(new_task.id, self.user.id, deadline=None)

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertIsNone(found_task.deadline)
        self.assertIsNone(user_task.deadline)
        self.assertIsNone(group_task.deadline)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_deadline_with_not_existing_user(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, deadline="0-0 20-05-2019")

        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.change_task, self.task.id, None,
                          deadline="0-0 15-06-2019")

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 20-05-2019")
        self.assertEqual(user_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 20-05-2019")
        self.assertEqual(group_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 20-05-2019")

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_deadline_with_not_valid_deadline(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, deadline="0-0 20-05-2019")

        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, new_task.id, self.user.id,
                          deadline="123")

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 20-05-2019")
        self.assertEqual(user_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 20-05-2019")
        self.assertEqual(group_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 20-05-2019")

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_deadline_with_deadline_less_then_period_end_date(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, deadline="0-0 20-06-2019")

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="31-5-2019",
                                                             period=relativedelta(hours=48),
                                                             period_end_date="0-0 15-06-2019")

        self.task_manager.change_task(periodic_task.id, self.user.id, deadline="0-0 10-06-2019")

        found_task = self.storage.find_task_by_id(periodic_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 10-06-2019")
        self.assertEqual(user_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 10-06-2019")
        self.assertEqual(group_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 10-06-2019")
        self.assertEqual(found_task.period_end_date.strftime("%H-%M %d-%m-%Y"), "00-00 10-06-2019")
        self.assertEqual(user_task.period_end_date.strftime("%H-%M %d-%m-%Y"), "00-00 10-06-2019")
        self.assertEqual(group_task.period_end_date.strftime("%H-%M %d-%m-%Y"), "00-00 10-06-2019")

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_deadline_with_deadline_less_then_current_date(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, deadline="0-0 20-06-2019")

        self.task_manager.change_task(new_task.id, self.user.id, deadline="0-0 10-06-2016")

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 10-06-2016")
        self.assertEqual(user_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 10-06-2016")
        self.assertEqual(group_task.deadline.strftime("%H-%M %d-%m-%Y"), "00-00 10-06-2016")
        self.assertEqual(found_task.status, Status.NOT_RELEVANT)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)
    # endregion

    # region Change period start date tests
    def test_change_task_period_start_date(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id)

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="28-5-2019",
                                                             period=relativedelta(hours=12))

        self.task_manager.change_task(periodic_task.id, self.user.id, period_start_date="20-48 31-05-2019")

        found_task = self.storage.find_task_by_id(periodic_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.period_start_date.strftime("%H-%M %d-%m-%Y"), "20-48 31-05-2019")
        self.assertEqual(user_task.period_start_date.strftime("%H-%M %d-%m-%Y"), "20-48 31-05-2019")
        self.assertEqual(group_task.period_start_date.strftime("%H-%M %d-%m-%Y"), "20-48 31-05-2019")

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_period_start_date_with_none(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id)

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="28-5-2019",
                                                             period=relativedelta(hours=12),
                                                             period_end_date="29-6-2019")

        self.task_manager.change_task(periodic_task.id, self.user.id, period_start_date=None)

        found_task = self.storage.find_task_by_id(periodic_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertIsNone(found_task.period_start_date)
        self.assertIsNone(user_task.period_start_date)
        self.assertIsNone(group_task.period_start_date)
        self.assertIsNone(found_task.period)
        self.assertIsNone(user_task.period)
        self.assertIsNone(group_task.period)
        self.assertIsNone(found_task.period_end_date)
        self.assertIsNone(user_task.period_end_date)
        self.assertIsNone(group_task.period_end_date)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_period_start_date_with_not_valid_date(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id)

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="28-5-2019",
                                                             period=relativedelta(hours=12),
                                                             period_end_date="29-6-2019")

        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, periodic_task.id,
                          self.user.id, period_start_date=1)

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.period_start_date.strftime("%H-%M %d-%m-%Y"), "00-00 28-05-2019")
        self.assertEqual(user_task.period_start_date.strftime("%H-%M %d-%m-%Y"), "00-00 28-05-2019")
        self.assertEqual(group_task.period_start_date.strftime("%H-%M %d-%m-%Y"), "00-00 28-05-2019")

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)
    # endregion

    # region Change period tests
    def test_change_task_period(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id)

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="31-5-2019",
                                                             period=relativedelta(hours=48),
                                                             period_end_date="0-0 15-06-2019")

        self.task_manager.change_task(periodic_task.id, self.user.id, period=relativedelta(days=+3))

        found_task = self.storage.find_task_by_id(periodic_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.period, relativedelta(days=+3))
        self.assertEqual(user_task.period, relativedelta(days=+3))
        self.assertEqual(group_task.period, relativedelta(days=+3))

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_period_with_not_valid_period(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id)

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="28-5-2019",
                                                             period=relativedelta(hours=12))

        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, periodic_task.id,
                          self.user.id, period="-48")
        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, periodic_task.id,
                          self.user.id, period="asd")

        found_task = self.storage.find_task_by_id(periodic_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.period, relativedelta(hours=12))
        self.assertEqual(user_task.period, relativedelta(hours=12))
        self.assertEqual(group_task.period, relativedelta(hours=12))

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_period_with_not_existing_user(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id)

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="28-5-2019",
                                                             period=relativedelta(hours=12))

        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.change_task, periodic_task.id, None,
                          period=relativedelta(hours=48))

        found_task = self.storage.find_task_by_id(periodic_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.period, relativedelta(hours=12))
        self.assertEqual(user_task.period, relativedelta(hours=12))
        self.assertEqual(group_task.period, relativedelta(hours=12))

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)
    # endregion

    # region Change period end date tests
    def test_change_task_period_end_date(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id)

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="28-5-2019",
                                                             period=relativedelta(hours=12),
                                                             period_end_date="0-0 20-06-2019")

        self.task_manager.change_task(periodic_task.id, self.user.id, period_end_date="20-48 31-05-2019")

        found_task = self.storage.find_task_by_id(periodic_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.period_end_date.strftime("%H-%M %d-%m-%Y"), "20-48 31-05-2019")
        self.assertEqual(user_task.period_end_date.strftime("%H-%M %d-%m-%Y"), "20-48 31-05-2019")
        self.assertEqual(group_task.period_end_date.strftime("%H-%M %d-%m-%Y"), "20-48 31-05-2019")

    def test_change_task_period_end_date_with_none(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id)

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="28-5-2019",
                                                             period=relativedelta(hours=12),
                                                             period_end_date="0-0 20-06-2019")

        self.task_manager.change_task(periodic_task.id, self.user.id, period_end_date=None)

        found_task = self.storage.find_task_by_id(periodic_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertIsNone(found_task.period_end_date)
        self.assertIsNone(user_task.period_end_date)
        self.assertIsNone(group_task.period_end_date)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_period_end_date_with_none_period_start_date(self):
        self.assertRaises(exceptions.NotExistingPeriodStartDateError, self.task_manager.change_task, self.task.id,
                          self.user.id, period_end_date="20-48 31-05-2019")

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertIsNone(found_task.period_end_date)
        self.assertIsNone(user_task.period_end_date)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    def test_change_task_period_end_date_with_none_period(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id)

        periodic_task = self.task_manager.make_task_periodic(new_task.id, self.user.id, period_start_date="28-5-2019",
                                                             period=relativedelta(hours=2))

        self.task_manager.change_task(periodic_task.id, self.user.id, period=None)

        self.assertRaises(exceptions.NotExistingPeriodError, self.task_manager.change_task, periodic_task.id,
                          self.user.id, period_end_date="20-48 31-05-2019")

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertIsNone(found_task.period_end_date)
        self.assertIsNone(user_task.period_end_date)
        self.assertIsNone(group_task.period_end_date)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)
    # endregion

    # region Change status tests
    def test_change_task_status(self):
        self.task_manager.change_task(self.task.id, self.user.id, status="1")

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.status, Status.IN_PROCESS)
        self.assertEqual(user_task.status, Status.IN_PROCESS)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    def test_change_task_status_with_not_valid_status(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, status=1)

        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, new_task.id, self.user.id,
                          status="123")
        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, new_task.id, self.user.id,
                          status=-4)

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.status, Status.IN_PROCESS)
        self.assertEqual(user_task.status, Status.IN_PROCESS)
        self.assertEqual(group_task.status, Status.IN_PROCESS)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_status_with_status_finished(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, status=1)

        self.task_manager.change_task(new_task.id, self.user.id, status="2")

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.status, Status.FINISHED)
        self.assertEqual(user_task.status, Status.FINISHED)
        self.assertEqual(group_task.status, Status.FINISHED)
        self.assertEqual(group_task.completion_percentage, 100)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_status_with_status_finished_with_dependent_task(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, status=1)
        dependent_task = self.task_manager.create_dependent_task(new_task.id, self.user.id, "dep_task")

        self.assertRaises(exceptions.BlockedTaskChangeError, self.task_manager.change_task, dependent_task.id,
                          self.user.id, status="2")
        self.assertEqual(dependent_task.status, Status.CREATED)

        self.task_manager.change_task(new_task.id, self.user.id, status="2")

        dependent_task = self.storage.find_task_by_id(dependent_task.id)
        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-2]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.task_manager.change_task(dependent_task.id, self.user.id, status="2")
        dependent_task = self.storage.find_task_by_id(dependent_task.id)
        self.assertEqual(dependent_task.status, Status.FINISHED)

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.status, Status.FINISHED)
        self.assertEqual(user_task.status, Status.FINISHED)
        self.assertEqual(group_task.status, Status.FINISHED)
        self.assertEqual(found_task.completion_percentage, 100)

        self.assertIsNone(dependent_task.parent)
        self.assertFalse(dependent_task.is_blocked)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

        self.assertTrue(dependent_task not in self.obj_getter.get_task_dependent_tasks(found_task.id))
    # endregion

    # region Change priority tests
    def test_change_task_priority_with_not_valid_priority(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, priority=1)

        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, new_task.id, self.user.id,
                          priority="123")
        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, new_task.id, self.user.id,
                          priority=-4)

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.priority, Priority.LOW)
        self.assertEqual(user_task.priority, Priority.LOW)
        self.assertEqual(group_task.priority, Priority.LOW)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)

    def test_change_task_priority(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        new_task = self.task_manager.create_task("task_name", self.user.id, group.id, priority=1)

        self.task_manager.change_task(new_task.id, self.user.id, priority="2")

        found_task = self.storage.find_task_by_id(new_task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[-1]
        group_task = self.obj_getter.get_group_tasks(group.id)[-1]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.priority, Priority.AVERAGE)
        self.assertEqual(user_task.priority, Priority.AVERAGE)
        self.assertEqual(group_task.priority, Priority.AVERAGE)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(found_task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(group.id in found_task.groups_list)
    # endregion

    # region Change percentage tests
    def test_change_task_completion_percentage(self):
        self.task_manager.change_task(self.task.id, self.user.id, percentage="95.5")

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.completion_percentage, 95.5)
        self.assertEqual(user_task.completion_percentage, 95.5)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    def test_change_task_completion_percentage_with_not_valid_percentage(self):
        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, self.task.id, self.user.id,
                          percentage="123")
        self.assertRaises(exceptions.NotValidParameterError, self.task_manager.change_task, self.task.id, self.user.id,
                          percentage=-4)

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.completion_percentage, 0)
        self.assertEqual(user_task.completion_percentage, 0)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    def test_change_task_completion_percentage_with_100_percentage(self):
        self.task_manager.change_task(self.task.id, self.user.id, percentage="100")

        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.completion_percentage, 100)
        self.assertEqual(user_task.completion_percentage, 100)
        self.assertEqual(found_task.status, Status.FINISHED)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.user.id in found_task.users_list)

    def test_change_task_completion_percentage_with_100_percentage_with_dependent_task(self):
        dependent_task = self.task_manager.create_dependent_task(self.task.id, self.user.id, "dep_task")

        self.assertRaises(exceptions.BlockedTaskChangeError, self.task_manager.change_task,
                          dependent_task.id, self.user.id, percentage="2")
        self.assertEqual(dependent_task.completion_percentage, 0)
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))

        self.task_manager.change_task(self.task.id, self.user.id, percentage="100")

        dependent_task = self.task_manager.storage.find_task_by_id(dependent_task.id)
        found_task = self.storage.find_task_by_id(self.task.id)

        user_task = self.obj_getter.get_user_tasks(self.user.id)[0]

        self.task_manager.change_task(dependent_task.id, self.user.id, percentage="2")
        dependent_task = self.task_manager.storage.find_task_by_id(dependent_task.id)
        self.assertEqual(dependent_task.completion_percentage, 2)

        self.assertIsNotNone(found_task)
        self.assertEqual(found_task.completion_percentage, 100)
        self.assertEqual(user_task.completion_percentage, 100)
        self.assertEqual(found_task.status, Status.FINISHED)

        self.assertEqual(len(self.obj_getter.get_task_dependent_tasks(self.task.id)), 0)
        self.assertIsNone(dependent_task.parent)

        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(dependent_task not in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(self.user.id in found_task.users_list)
    # endregion

    # endregion

    # region Task deletion tests
    def test_delete_task(self):
        self.assertIsNotNone(self.task, "Error in the task adding")
        self.assertTrue(self.user.id in self.task.users_list)
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.task_manager.delete_task(self.task.id, self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.task_manager.storage.find_task_by_id, self.task.id)
        self.assertTrue(self.task not in self.obj_getter.get_user_tasks(self.user.id))

    def test_delete_task_with_not_existing_user(self):
        self.assertIsNotNone(self.task, "Error in the task adding")
        self.assertTrue(self.user.id in self.task.users_list)
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.assertRaises(exceptions.NotExistingObjectError, self.task_manager.delete_task, self.task.id, None)

        found_task = self.storage.find_task_by_id(self.task.id)

        self.assertIsNotNone(found_task)
        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))

    def test_delete_task_with_not_task_user(self):
        another_user = self.user_manager.create_user("another_login", "another_email")

        self.assertIsNotNone(self.task, "Error in the task adding")
        self.assertTrue(self.user.id in self.task.users_list)
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(another_user.id not in self.task.users_list)
        self.assertTrue(self.task not in self.obj_getter.get_user_tasks(another_user.id))

        self.assertRaises(exceptions.PermissionsError, self.task_manager.delete_task, self.task.id, another_user.id)

        found_task = self.storage.find_task_by_id(self.task.id)

        self.assertIsNotNone(found_task)
        self.assertTrue(self.user.id in self.task.users_list)
        self.assertTrue(found_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(another_user.id not in found_task.users_list)
        self.assertTrue(found_task not in self.obj_getter.get_user_tasks(another_user.id))

    def test_delete_task_with_groups(self):
        group = self.group_manager.create_group("group_name", self.user.id)
        second_group = self.group_manager.create_group("second_group_name", self.user.id)

        self.task_manager.add_group_to_task(self.task.id, self.user.id, group.id)
        self.task_manager.add_group_to_task(self.task.id, self.user.id, second_group.id)

        found_task = self.storage.find_task_by_id(self.task.id)

        self.assertTrue(self.user.id in found_task.users_list)
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(group.id in found_task.groups_list)
        self.assertTrue(self.task in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(second_group.id in found_task.groups_list)
        self.assertTrue(self.task in self.obj_getter.get_group_tasks(second_group.id))

        self.task_manager.delete_task(self.task.id, self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.task_manager.storage.find_task_by_id, self.task.id)

        self.assertTrue(self.task not in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task not in self.obj_getter.get_group_tasks(group.id))
        self.assertTrue(self.task not in self.obj_getter.get_group_tasks(second_group.id))

    def test_delete_task_with_users(self):
        second_user = self.user_manager.create_user("second_task_user_login", "second_task_user_email")

        task = self.task_manager.add_user_to_task(self.task.id, self.user.id, second_user.login)

        self.assertTrue(self.user.id in task.users_list)
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(second_user.id in task.users_list)
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(second_user.id))

        self.task_manager.delete_task(self.task.id, self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.task_manager.storage.find_task_by_id, self.task.id)

        self.assertTrue(self.task not in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task not in self.obj_getter.get_user_tasks(second_user.id))

    def test_delete_task_with_sub_tasks(self):
        sub_task = self.task_manager.create_sub_task(self.task.id, self.user.id, "sub_task")

        self.assertIsNotNone(self.task, "Error in the task adding")
        self.assertTrue(sub_task in self.obj_getter.get_task_sub_tasks(self.task.id))
        self.assertTrue(sub_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.task_manager.delete_task(self.task.id, self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.task_manager.storage.find_task_by_id, self.task.id)
        self.assertRaises(exceptions.NotFoundObjectError, self.task_manager.storage.find_task_by_id, sub_task.id)

        self.assertTrue(sub_task not in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task not in self.obj_getter.get_user_tasks(self.user.id))

    def test_delete_task_with_dependent_tasks(self):
        dependent_task = self.task_manager.create_dependent_task(self.task.id, self.user.id, "sub_task")

        self.assertIsNotNone(self.task, "Error in the task adding")
        self.assertTrue(dependent_task in self.obj_getter.get_task_dependent_tasks(self.task.id))
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task in self.obj_getter.get_user_tasks(self.user.id))

        self.task_manager.delete_task(self.task.id, self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.task_manager.storage.find_task_by_id, self.task.id)
        dependent_task = self.task_manager.storage.find_task_by_id(dependent_task.id)

        self.assertIsNotNone(dependent_task)
        self.assertFalse(dependent_task.is_blocked)
        self.assertTrue(dependent_task in self.obj_getter.get_user_tasks(self.user.id))
        self.assertTrue(self.task not in self.obj_getter.get_user_tasks(self.user.id))

    # endregion

    def tearDown(self):
        self.storage.clear()

        shutil.rmtree(self.temp_dir, ignore_errors=True)
        enable_logger()


if __name__ == '__main__':
    unittest.main()
