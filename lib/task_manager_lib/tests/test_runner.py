import unittest
from task_manager_lib.tests.test_db_func import TestDatabaseManager
from task_manager_lib.tests.test_group_func import TestGroupFunctions
from task_manager_lib.tests.test_task_func import TestTaskFunctions
from task_manager_lib.tests.test_user_func import TestUserFunctions


def get_test_suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(TestDatabaseManager))
    suite.addTests(unittest.makeSuite(TestGroupFunctions))
    suite.addTests(unittest.makeSuite(TestTaskFunctions))
    suite.addTests(unittest.makeSuite(TestUserFunctions))
    return suite

