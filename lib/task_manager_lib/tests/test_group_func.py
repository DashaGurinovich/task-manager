import os
import shutil
import unittest

from task_manager_lib import exceptions
from task_manager_lib.db.json.manager import JsonDataManager
from task_manager_lib.db.manager import DataManager
from task_manager_lib.func import group as group_func, task as task_func, user as user_func
from task_manager_lib.func.connector import DataConnector
from task_manager_lib.logger import disable_logger, enable_logger


class TestGroupFunctions(unittest.TestCase):
    def setUp(self):
        disable_logger()

        # Create temporary dir to store tests data
        self.temp_dir = os.path.join(os.path.abspath(os.curdir), 'temp_dir')
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir, ignore_errors=True)
        os.mkdir(self.temp_dir)

        self.storage = DataManager(JsonDataManager(self.temp_dir))

        self.group_manager = group_func.GroupManager(self.storage)
        self.user_manager = user_func.UserManager(self.storage)
        self.task_manager = task_func.TaskManager(self.storage)
        self.obj_getter = DataConnector(self.storage)

        self.user = self.user_manager.create_user("group_user_login", "group_user_email")
        self.group = self.group_manager.create_group("group_name", self.user.id)

    # region Group creation tests
    def test_create_group(self):
        group = self.group_manager.create_group("group", self.user.id)
        user_groups = self.obj_getter.get_user_groups(self.user.id)

        self.assertIsNotNone(group)
        self.assertEqual(self.group.name, "group_name")
        self.assertTrue(self.user.id in self.group.users_list)
        self.assertTrue(group in user_groups)

    def test_create_group_with_not_existing_user(self):
        self.assertRaises(exceptions.NotExistingObjectError, self.group_manager.create_group, "group_name", None)

    def test_create_group_with_not_user_id(self):
        self.assertRaises(TypeError, self.group_manager.create_group, "group_name", 1)

    # endregion

    # region Group change tests
    def test_change_group_name(self):
        self.group_manager.change_group(self.group.id, self.user.id, new_name="new_group_name")
        group = self.group_manager.storage.find_group_by_id(self.group.id)
        group_from_user = self.obj_getter.get_user_groups(self.user.id)[0]

        self.assertIsNotNone(group)
        self.assertEqual(group.name, "new_group_name")
        self.assertEqual(group_from_user.name, "new_group_name")

    def test_change_group_name_with_not_group_user(self):
        second_user = self.user_manager.create_user("another_group_user_login", "another_group_user_email")

        self.assertRaises(exceptions.PermissionsError, self.group_manager.change_group, self.group.id, second_user.id,
                          new_name="new_group_name")

    def test_change_group_name_with_not_existing_user(self):
        self.assertRaises(exceptions.NotExistingObjectError, self.group_manager.change_group, self.group.id, None,
                          new_name="new_group_name")

    def test_change_name_with_not_group_id(self):
        self.assertRaises(TypeError, self.group_manager.change_group, self.user.id, self.user.id,
                          new_name="new_group_name")
    # endregion

    # region Group deletion tests
    def test_delete_group_without_tasks(self):
        self.group_manager.delete_group(self.group.id, self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.group_manager.storage.find_group_by_id, self.group.id)
        self.assertTrue(self.group not in self.obj_getter.get_user_groups(self.user.id))

    def test_delete_group_with_tasks(self):
        task = self.task_manager.create_task("task", self.user.id)
        self.group_manager.delete_group(self.group.id, self.user.id)

        self.assertRaises(exceptions.NotFoundObjectError, self.group_manager.storage.find_group_by_id, self.group.id)

        task_after_del = self.group_manager.storage.find_task_by_id(task.id)

        self.assertTrue(self.group not in self.obj_getter.get_user_groups(self.user.id))
        self.assertTrue(self.group.id not in task_after_del.groups_list)

    def test_delete_group_with_not_group_user(self):
        second_user = self.user_manager.create_user("another_group_user_login", "another_group_user_email")

        self.assertRaises(exceptions.PermissionsError, self.group_manager.delete_group, self.group.id, second_user.id)

    def test_delete_not_existing_group(self):
        self.assertRaises(exceptions.NotExistingObjectError, self.group_manager.delete_group, None, self.user.id)
    # endregion

    # region Group management with users tests
    def test_add_user_to_group(self):
        second_user = self.user_manager.create_user("second_user_login", "second_user_email")
        group = self.group_manager.add_user_to_group(self.group.id, self.user.id, second_user.login)

        self.assertTrue(self.group in self.obj_getter.get_user_groups(self.user.id))
        self.assertTrue(self.group in self.obj_getter.get_user_groups(second_user.id))
        self.assertTrue(self.user.id in group.users_list)
        self.assertTrue(second_user.id in group.users_list)

    def test_add_group_user_to_group(self):
        self.assertRaises(exceptions.ObjectAlreadyInListError, self.group_manager.add_user_to_group, self.group.id,
                          self.user.id, self.user.login)

        self.assertTrue(self.group in self.obj_getter.get_user_groups(self.user.id))
        self.assertTrue(self.user.id in self.group.users_list)

    def test_delete_user_from_group(self):
        second_user = self.user_manager.create_user("second_user_login", "second_user_email")

        group = self.group_manager.add_user_to_group(self.group.id, self.user.id, second_user.login)

        self.assertTrue(self.group in self.obj_getter.get_user_groups(self.user.id))
        self.assertTrue(self.group in self.obj_getter.get_user_groups(second_user.id))
        self.assertTrue(self.user.id in group.users_list)
        self.assertTrue(second_user.id in group.users_list)

        self.group_manager.delete_user_from_group(self.group.id, self.user.id, second_user.login)

        self.assertTrue(self.group in self.obj_getter.get_user_groups(self.user.id))
        self.assertTrue(self.group not in self.obj_getter.get_user_groups(second_user.id))
        self.assertTrue(self.user.id in self.group.users_list)
        self.assertTrue(second_user.id not in self.group.users_list)

    def test_delete_only_user_from_group(self):
        self.assertRaises(exceptions.OnlyUserDeletionError, self.group_manager.delete_user_from_group, self.group.id,
                          self.user.id, self.user.login)

        self.assertTrue(self.group in self.obj_getter.get_user_groups(self.user.id))
        self.assertTrue(self.user.id in self.group.users_list)

    def test_delete_not_group_user_from_group(self):
        second_user = self.user_manager.create_user("second_user_login", "second_user_email")

        self.assertRaises(exceptions.ObjectNotInListError, self.group_manager.delete_user_from_group, self.group.id,
                          self.user.id, second_user.login)

        self.assertTrue(self.group in self.obj_getter.get_user_groups(self.user.id))
        self.assertTrue(self.group not in self.obj_getter.get_user_groups(second_user.id))
        self.assertTrue(self.user.id in self.group.users_list)
        self.assertTrue(second_user.id not in self.group.users_list)
    # endregion

    def tearDown(self):
        self.storage.clear()

        shutil.rmtree(self.temp_dir, ignore_errors=True)
        enable_logger()


if __name__ == '__main__':
    unittest.main()
