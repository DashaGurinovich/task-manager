import os
import shutil
import unittest

from task_manager_lib.db.json.manager import JsonDataManager
from task_manager_lib.db.manager import DataManager
from task_manager_lib.entities import group, task, user
from task_manager_lib.exceptions import NotFoundObjectError
from task_manager_lib.logger import disable_logger, enable_logger


class TestDatabaseManager(unittest.TestCase):
    def setUp(self):
        disable_logger()

        # Create temporary dir to store tests data
        self.temp_dir = os.path.join(os.path.abspath(os.curdir), 'temp_dir')
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir, ignore_errors=True)
        os.mkdir(self.temp_dir)

        storage = JsonDataManager(self.temp_dir)

        self.storage = DataManager(storage)

        self.group = group.Group("self.group name")
        self.groups_number = len(self.storage.get_groups_from_db())

        self.task = task.Task("self.task name")
        self.tasks_number = len(self.storage.get_tasks_from_db())

        self.user = user.User("login", "email")
        self.users_number = len(self.storage.get_users_from_db())

    # region Group func tests
    def test_add_group_to_database(self):
        self.storage.add_group_to_db(self.group)
        new_groups_number = len(self.storage.get_groups_from_db())

        self.assertEqual(new_groups_number - self.groups_number, 1)

    def test_change_group_name_in_database(self):
        object_id = self.group.id
        self.storage.add_group_to_db(self.group)

        self.group.name = "new self.group name"
        self.storage.change_group_in_db(self.group)

        groups = self.storage.get_groups_from_db()
        new_groups_number = len(groups)
        self.assertEqual(new_groups_number - self.groups_number, 1, "Error in self.group adding to the database")

        has_been_found = False
        for g in groups:
            if g.id == object_id:
                has_been_found = True
                self.assertEqual(g.name, "new self.group name", "The name hasn't been changed")

        self.assertTrue(has_been_found, "The self.group hasn't been found")

    def test_delete_group_from_database(self):
        group_id = self.group.id
        self.storage.add_group_to_db(self.group)

        groups = self.storage.get_groups_from_db()
        users_number_after_adding = len(groups)
        self.assertEqual(users_number_after_adding - self.groups_number, 1, "Error in self.group adding to the "
                                                                            "database")
        self.storage.delete_group_from_db(group_id)

        new_groups = self.storage.get_groups_from_db()
        users_number_after_deleting = len(new_groups)
        self.assertEqual(users_number_after_deleting, self.groups_number, "The self.group wasn't deleted")

    def test_find_group_by_id(self):
        group_id = self.group.id
        self.storage.add_group_to_db(self.group)

        groups = self.storage.get_groups_from_db()
        users_number_after_adding = len(groups)
        self.assertEqual(users_number_after_adding - self.groups_number, 1, "Error in self.group adding to the "
                                                                            "database")

        found_group = self.storage.find_group_by_id(group_id)
        self.assertIsNotNone(found_group)

    def test_find_not_existing_group_by_id(self):
        self.assertRaises(NotFoundObjectError, self.storage.find_group_by_id, '12452')

    # endregion

    # region Task db func tests
    def test_add_task_to_database(self):
        self.storage.add_task_to_db(self.task)
        new_tasks_number = len(self.storage.get_tasks_from_db())
        self.assertEqual(new_tasks_number - self.tasks_number, 1)

    def test_change_task_name_in_database(self):
        object_id = self.task.id
        self.storage.add_task_to_db(self.task)

        self.task.name = "new self.task name"
        self.storage.change_task_in_db(self.task)
        tasks = self.storage.get_tasks_from_db()

        new_tasks_number = len(tasks)
        self.assertEqual(new_tasks_number - self.tasks_number, 1, "Error in self.task adding to the database")

        has_been_found = False
        for t in tasks:
            if t.id == object_id:
                has_been_found = True
                self.assertEqual(t.name, "new self.task name", "The name hasn't been changed")

        self.assertTrue(has_been_found, "The self.task hasn't been found")

    def test_delete_task_from_database(self):
        task_id = self.task.id
        self.storage.add_task_to_db(self.task)

        tasks = self.storage.get_tasks_from_db()
        tasks_number_after_adding = len(tasks)
        self.assertEqual(tasks_number_after_adding - self.tasks_number, 1, "Error in self.task adding to the database")
        self.storage.delete_task_from_db(task_id)
        new_tasks = self.storage.get_tasks_from_db()
        users_number_after_deleting = len(new_tasks)
        self.assertEqual(users_number_after_deleting, self.tasks_number, "The self.task wasn't deleted")

    def test_find_task_by_id(self):
        task_id = self.task.id
        self.storage.add_task_to_db(self.task)
        tasks = self.storage.get_tasks_from_db()
        tasks_number_after_adding = len(tasks)
        self.assertEqual(tasks_number_after_adding - self.tasks_number, 1, "Error in self.task adding to the database")

        found_task = self.storage.find_task_by_id(task_id)
        self.assertIsNotNone(found_task)

    def test_find_not_existing_task_by_id(self):
        self.assertRaises(NotFoundObjectError, self.storage.find_task_by_id, '12354')

    # endregion

    # region User db func tests
    def test_add_user_to_database(self):
        self.storage.add_user_to_db(self.user)
        new_users_number = len(self.storage.get_users_from_db())

        self.assertEqual(new_users_number - self.users_number, 1)

    def test_change_user_login_in_database(self):
        object_id = self.user.id
        self.storage.add_user_to_db(self.user)

        self.user.login = "new login"
        self.storage.change_user_in_db(self.user)

        users = self.storage.get_users_from_db()
        new_users_number = len(users)
        self.assertEqual(new_users_number - self.users_number, 1, "Error in user adding to the database")

        has_been_found = False
        for u in users:
            if u.id == object_id:
                has_been_found = True
                self.assertEqual(u.login, "new login", "The login hasn't been changed")
        self.assertTrue(has_been_found, "The user hasn't been found")

    def test_delete_user_from_database(self):
        user_id = self.user.id
        self.storage.add_user_to_db(self.user)

        users = self.storage.get_users_from_db()
        users_number_after_adding = len(users)
        self.assertEqual(users_number_after_adding - self.users_number, 1, "Error in user adding to the database")

        self.storage.delete_user_from_db(user_id)
        new_users = self.storage.get_users_from_db()
        users_number_after_deleting = len(new_users)
        self.assertEqual(users_number_after_deleting, self.users_number, "The user wasn't deleted")

    def test_find_user_by_id(self):
        user_id = self.user.id
        self.storage.add_user_to_db(self.user)

        users = self.storage.get_users_from_db()
        users_number_after_adding = len(users)
        self.assertEqual(users_number_after_adding - self.users_number, 1, "Error in user adding to the database")

        found_user = self.storage.find_user_by_id(user_id)
        self.assertIsNotNone(found_user)

    def test_find_user_by_login(self):
        self.storage.add_user_to_db(self.user)

        users = self.storage.get_users_from_db()
        users_number_after_adding = len(users)
        self.assertEqual(users_number_after_adding - self.users_number, 1, "Error in user adding to the database")

        found_user = self.storage.find_user_by_login(self.user.login)
        self.assertIsNotNone(found_user)

    def test_find_user_by_email(self):
        self.storage.add_user_to_db(self.user)

        users = self.storage.get_users_from_db()
        users_number_after_adding = len(users)
        self.assertEqual(users_number_after_adding - self.users_number, 1, "Error in user adding to the database")

        found_user = self.storage.find_user_by_email(self.user.email)
        self.assertIsNotNone(found_user)

    def test_find_not_existing_user(self):
        self.assertRaises(NotFoundObjectError, self.storage.find_user_by_id, '123')
        self.assertRaises(NotFoundObjectError, self.storage.find_user_by_login, 'lll')
        self.assertRaises(NotFoundObjectError, self.storage.find_user_by_email, 'eee')
    # endregion

    def tearDown(self):
        self.storage.clear()

        shutil.rmtree(self.temp_dir, ignore_errors=True)
        enable_logger()


if __name__ == '__main__':
    unittest.main()
