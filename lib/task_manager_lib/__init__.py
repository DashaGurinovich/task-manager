"""This library is created to help managing the tasks.

    The tasks library has its own entities like every library. The main entities are group, task and user.

    ---TASK---
    Task entity is a task that a user wants to complete. The task necessarily has a name, it can have a definition,
    percentage of completion when the task was started. The task also has such characteristics as priority and status.
    The priorities are NO_PRIORITY, LOW, AVERAGE and HIGH. The user can select the priority that he thinks suites
    the task more. The statuses are CREATED (used when the task was created and the completion of it wasn't started),
    IN_PROCESS (used when the task completion of started), FINISHED (used when the task was completed) and NOT_RELEVANT
    (used when the task completion isn't actual any more). Task also knows when it was created and finished,

    Tasks can have sub tasks and dependent tasks. Sub tasks are the tasks that should be completed to
    complete the main task. When the main task is finished, it means that all sub tasks are finished too.
    Dependent tasks are the tasks the completion of that can't be started while the main task isn't completed.
    For example, you can't read a book if you haven't get it. So the main task is to get a book. The dependent task
    is to read a book.

    Task can be periodic. What does it mean? It means that the user should complete the task periodically (every day,
    every six hours). For example, go the gym every two days at 7 p.m.

    ---GROUP---
    Group entity is a group that unites some tasks. The task can be in one group, in several groups or in no groups
    at all. The only group characteristic is its name. For example, you can store work tasks in the group with the
    name Work.

    ---User---
    User entity is a user that works with tasks and their groups. The user has two necessary parameters: login and
    email. These parameters should be unique. That means that you can create two users with the same logins or emails.
    The user has such optional parameters as first name, last name and the date of birth.

    ---EXCEPTIONS
    The library also has its own exceptions that are used throughout all library.

    ---What can this library do?
    With the help of this library you can create, update and delete groups, tasks and users. Each entity has its own
    manager to perform actions. These managers are represented by the classes GroupManager, TaskManager and UserManager.
     The data will be stored in the storage. By default the library uses json-storage, but you can pass your storage to
    entities managers to save data. For example:

    >>> import os
    >>> import lib.task_manager_lib.db.json
    >>> import lib.task_manager_lib.func.user

    >>> path = os.path.expanduser('~/.TaskManager/data')
    >>> storage = task_manager_lib.db.json.manager.JsonDataManager(path)
    >>> user_manager = task_manager_lib.func.user.UserManager(storage)
    >>> user = user_manager.create_user('login', 'email')

    The result:
    2018-06-10 18:40:32,998 | INFO : The user with login 'login' and email 'email' was created under
    9c821ae2-6cc4-11e8-a860-80a5895bd4b9 id

    The library has the logging system. The logger writes when the function enters and finishes on the DEBUG level,
    when the error was occupied on the ERROR or the CRITICAL level (depends on the error kind). The logger also writes
    other information (for example, task was created, the user login was changed and so on) on the INFO or WARNING
    level.

    Packages:
    db -- includes methods for working with databases (storages)
    entities -- includes the classes that represent main app entities (Group, Task, User)
    func -- includes the main library logic
    tests -- includes the unit-tests that cover main app possibilities

    Modules:
    exceptions -- includes the app exceptions
    logger -- includes the functions that work with the app logger
"""