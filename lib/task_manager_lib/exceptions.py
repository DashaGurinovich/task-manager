"""This module includes the app exceptions"""


class BlockingException(Exception):
    """ Parent class for the exceptions that can be blocked. The exception gets blocked
    when it was written to the logger for the first time. That is used to avoid the same exception
    be written in the logger for several times
    """

    def __init__(self, message):
        self.is_blocked = False
        super().__init__(message)


class PathNotFoundError(Exception):
    """ Raises when the written path hasn't been found"""

    def __init__(self, message):
        super().__init__(message)


class NotFileTypeError(Exception):
    """ Raises when the passed file isn't of the File type"""

    def __init__(self, path):
        super().__init__("{} isn't a file".format(path))


class SettingsNotFoundError(Exception):
    """ Raises when the settings file hasn't been found"""

    def __init__(self):
        super().__init__("The settings hasn't been found. The default settings path is '~/.TaskManager/settings.ini'. "
                         "You can use functions from tasks.task_manager_lib.settings to create them.")


class LoggerConfigNotFoundError(Exception):
    """ Raises when the logger config file hasn't been found"""

    def __init__(self):
        super().__init__("The logger config hasn't been found")


class NotExistingObjectError(Exception):
    """ Raises when the object doesn't exist"""

    def __init__(self, object_name):
        super().__init__("The {} object doesn't exist".format(object_name))


class NotFoundObjectError(BlockingException):
    """ Raises when the object wasn't found"""

    def __init__(self, object_atr, object_name):
        self.object_name = object_name
        super().__init__("The object with id (login or email) '{}' wasn't found".format(object_atr))


class ObjectAlreadyInListError(Exception):
    """ Raises when the object is already in the other object list"""

    def __init__(self, object_name, container_object_name):
        self.object_name = object_name
        super().__init__("The {} object is already in {} list".format(object_name, container_object_name))


class ObjectNotInListError(Exception):
    """ Raises when the object is not in the other object list"""

    def __init__(self, object_name, container_object_name):
        self.object_name = object_name
        super().__init__("The {} object isn't in {} list".format(object_name, container_object_name))


class PermissionsError(Exception):
    """ Raises when the user doesn't have the permissions to perform actions
    with the object
    """

    def __init__(self, user_login):
        super().__init__("The user with login '{}' can't change or delete the object".format(user_login))


class ExistingUserError(BlockingException):
    """ Raises when there's already the user with the written login or email"""

    def __init__(self, login, email):
        super().__init__('The user with login "{}" or email "{}" already exists'.format(login, email))


class NotExistingPeriodStartDateError(BlockingException):
    """ Raises when the actions with period or period end date are performed
    but there's not period start date
    """

    def __init__(self):
        super().__init__("Can't add period or period end date if there's no period start date")


class NotExistingPeriodError(BlockingException):
    """ Raises when the actions with period end date are performed but there's not period"""

    def __init__(self):
        super().__init__("Can't add period end date if there's no period")


class NotValidPeriodDatesError(BlockingException):
    """ Raises when the period end date is sooner than start period date"""

    def __init__(self, period_start_date, period_end_date):
        super().__init__("The period start date '{}' is later than period end date '{}'".format(period_start_date,
                                                                                                period_end_date))


class NotValidParameterError(BlockingException):
    """ Raises when the written parameter isn't valid"""

    def __init__(self, val, par_name):
        super().__init__("{} is not valid {}".format(val, par_name))


class SelfSubTaskError(Exception):
    """ Raises when the sub task parent is the task itself"""

    def __init__(self, val, par_name):
        super().__init__("The task can't be sub task of itself".format(val, par_name))


class BlockedTaskChangeError(BlockingException):
    """ Raises when the blocked task status or percentage of completion is trying to be changed"""

    def __init__(self):
        super().__init__("The blocked task status or percentage can't be changed")


class OnlyUserDeletionError(Exception):
    """ Raises when the only user is getting removed from the object"""

    def __init__(self, object_name):
        super().__init__("Can't delete the only user from {}".format(object_name))


class NotPassedParameterError(Exception):
    """ Raises when none storage parameters was passed to the function"""

    def __init__(self):
        super().__init__("The storage or the storage_path wasn't passed to the constructor")
