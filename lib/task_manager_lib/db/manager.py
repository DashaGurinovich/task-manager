"""This module includes class DataManager that's a generalized class for different kinds of databases.
It's used to avoid lots of changing in the code when the database origin is changed"""

from task_manager_lib.exceptions import NotFoundObjectError
from task_manager_lib.logger import write_log


class DataManager:
    """Performs CRUD actions with the database
    DataManager class is a generalized class that's used in the library to work with the storage.
    The storage passed to the DataManager should contain such methods as get_*_from_db(), add_*_to_db(new_object),
    change_*_in_db(changed_object), delete_*_from_db(object_id) where * is group, task and user. Otherwise
    the AttributeError will be raised while trying to use some DataManager functions.

    Fields:
    storage -- the object of the some storage class
    """

    def __init__(self, storage):
        """Initializes the object"""

        self.storage = storage

    @write_log
    def clear(self):
        """Deletes all objects from the db"""

        return self.storage.clear()

    # region Task db funcs
    @write_log
    def get_tasks_from_db(self):
        """ Returns all tasks that now are stored in the database """

        return self.storage.get_tasks_from_db()

    @write_log
    def add_task_to_db(self, new_task):
        """ Adds Task object to the database

        Keyword arguments:
        new_task -- task to add to the database
        """
        return self.storage.add_task_to_db(new_task)

    @write_log
    def change_task_in_db(self, changed_task):
        """ Changes Task object in the database

        Keyword arguments:
        changed_task -- task to change in the database
        """
        return self.storage.change_task_in_db(changed_task)

    @write_log
    def delete_task_from_db(self, task_id):
        """ Deletes Task object from the database

        Keyword arguments:
        task_id -- id of the task to delete from the database
        """
        return self.storage.delete_task_from_db(task_id)

    @write_log
    def find_task_by_id(self, task_id):
        """ Finds Task object by the id in the database and returns it,
            otherwise writes error in the LOGGER

        Keyword arguments:
        task_id -- id of the task to find in the database
        """
        tasks = self.storage.get_tasks_from_db()
        for t in tasks:
            if str(t.id) == task_id or task_id == t.id:
                return t

        raise NotFoundObjectError(task_id, 'Task')
    # endregion

    # region Group db funcs
    @write_log
    def get_groups_from_db(self):
        """ Returns all groups that now are stored in the database """
        return self.storage.get_groups_from_db()

    @write_log
    def add_group_to_db(self, new_group):
        """ Adds Group object to the database

        Keyword arguments:
        new_group -- group to add to the database
        """
        return self.storage.add_group_to_db(new_group)

    @write_log
    def change_group_in_db(self, changed_group):
        """ Changes Group object in the database

        Keyword arguments:
        changed_group -- group to change in the database
        """
        self.storage.change_group_in_db(changed_group)

    @write_log
    def delete_group_from_db(self, group_id):
        """ Deletes Group object from the database

        Keyword arguments:
        group_id -- id of the group to delete from the database
        """
        self.storage.delete_group_from_db(group_id)

    @write_log
    def find_group_by_id(self, group_id):
        """ Finds Group object by the id in the database and returns it,
        otherwise writes error in the LOGGER

        Keyword arguments:
        group_id -- id of the group to find in the database
        """
        groups = self.storage.get_groups_from_db()

        for gr in groups:
            if str(gr.id) == group_id or gr.id == group_id:
                return gr

        raise NotFoundObjectError(group_id, 'Group')

    # endregion

    # region User db funcs
    @write_log
    def get_users_from_db(self):
        """ Returns all users that now are stored in the database """
        return self.storage.get_users_from_db()

    @write_log
    def add_user_to_db(self, new_user):
        """ Adds User object to the database

        Keyword arguments:
        new_user -- user to add to the database
        """
        return self.storage.add_user_to_db(new_user)

    @write_log
    def change_user_in_db(self, changed_user):
        """ Changes User object in the database

        Keyword arguments:
        changed_user -- user to change in the database
        """
        return self.storage.change_user_in_db(changed_user)

    @write_log
    def delete_user_from_db(self, user_id):
        """ Deletes User object from the database

        Keyword arguments:
        user_id -- id of the user to delete from the database
        """
        return self.storage.delete_user_from_db(user_id)

    @write_log
    def find_user_by_id(self, user_id):
        """ Finds User object by the id in the database and returns it,
            otherwise writes error in the LOGGER

        Keyword arguments:
        user_id -- id of the user to find in the database
        """
        users = self.storage.get_users_from_db()

        for u in users:
            if u.id == user_id or str(u.id) == user_id:
                return u

        raise NotFoundObjectError(user_id, 'User')

    @write_log
    def find_user_by_login(self, user_login):
        """ Finds User object by the login in the database and returns it,
            otherwise writes error in the LOGGER

        Keyword arguments:
        user_login -- login of the user to find in the database
        """
        all_users = self.storage.get_users_from_db()

        for u in all_users:
            if u.login == user_login:
                return u

        raise NotFoundObjectError(user_login, 'User')

    @write_log
    def find_user_by_email(self, user_email):
        """ Finds User object by the email in the database and returns it,
            otherwise writes error in the LOGGER

        Keyword arguments:
        user_email -- email of the user to find in the database
        """
        all_users = self.storage.get_users_from_db()

        for u in all_users:
            if u.email == user_email:
                return u

        raise NotFoundObjectError(user_email, 'User')

    # endregion
