"""This package includes modules for working with data storage

    DataManager class is a generalized class that's used in the library to work with the storage.
    If you won't pass a storage in the class object initialization, DataManager will use the JsonDataManager
    as the default storage. The storage passed to the DataManager should contain such methods as get_*_from_db(),
    add_*_to_db(new_object), change_*_in_db(changed_object), delete_*_from_db(object_id) where * is group, task and
    user. Otherwise the AttributeError will be raised while trying to use some DataManager functions.

    JsonDataManager implements the above methods working with the json-files.

    Packages:
    json -- works with json-database

    Modules:
    manager -- includes class DataManager that's a generalized class for different kinds of databases
"""
