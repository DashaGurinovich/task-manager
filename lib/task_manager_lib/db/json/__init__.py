"""This module includes different objects to perform actions with JSON-database

    JsonDataManager implements all necessary methods with the help of json_functions to be used in DataManager class
    With the help of this class, the objects of these types can be added to json-file, updated in json-file,
    deleted from json-file and found in json-file.

    Modules:
    json_functions -- includes CRUD methods for json-files
    manager -- includes JsonDataManager class with CRUD methods for Group, Task and User objects for json-files
"""