"""This module includes JsonDataManager class that performs CRUD actions with the entities"""

import os

from task_manager_lib.db.json import json_functions
from task_manager_lib.exceptions import PathNotFoundError


class JsonDataManager:
    """This class performs CRUD actions with Group, Task and User objects.

    This classes implements all necessary methods to be used in DataManager class
    With the help of this class, the objects of these types can be added to json-file, updated in json-file,
    deleted from json-file and found in json-file.
    """

    def __init__(self, storage_path):
        """Initializes the object

        storage_path -- path where the data will be stored

        Raises:
            PathNotFoundError -- when passed storage path doesn't exist
        """

        if not os.path.exists(storage_path):
            raise PathNotFoundError("{} isn't valid path".format(storage_path))

        self.storage_path = storage_path

        self.tasks_file = os.path.join(self.storage_path, 'tasksDB.json')
        self.groups_file = os.path.join(self.storage_path, 'groupsDB.json')
        self.users_file = os.path.join(self.storage_path, 'usersDB.json')

    def clear(self):
        """Deletes all objects from the db"""

        if os.path.isfile(self.tasks_file):
            os.remove(self.tasks_file)

        if os.path.isfile(self.groups_file):
            os.remove(self.groups_file)

        if os.path.isfile(self.users_file):
            os.remove(self.users_file)

    # region Task db funcs
    def get_tasks_from_db(self):
        """ Returns all tasks that now are stored in the database """

        return json_functions.get_objects_from_json_file(self.tasks_file)

    def add_task_to_db(self, new_task):
        """ Adds Task object to the database

        Keyword arguments:
        new_task -- task to add to the database
        """

        json_functions.add_object_to_json_file(self.tasks_file, new_task)

    def change_task_in_db(self, changed_task):
        """ Changes Task object in the database

        Keyword arguments:
        changed_task -- task to change in the database
        """

        json_functions.change_object_in_json_file(self.tasks_file, changed_task)

    def delete_task_from_db(self, task_id):
        """ Deletes Task object from the database

        Keyword arguments:
        task_id -- id of the task to delete from the database
        """

        json_functions.remove_object_from_json_file(self.tasks_file, task_id)
    # endregion

    # region Group db funcs
    def get_groups_from_db(self):
        """ Returns all groups that now are stored in the database """

        return json_functions.get_objects_from_json_file(self.groups_file)

    def add_group_to_db(self, new_group):
        """ Adds Group object to the database

        Keyword arguments:
        new_group -- group to add to the database
        """

        json_functions.add_object_to_json_file(self.groups_file, new_group)

    def change_group_in_db(self, changed_group):
        """ Changes Group object in the database, otherwise writes error in the LOGGER

        Keyword arguments:
        changed_group -- group to change in the database
        """

        json_functions.change_object_in_json_file(self.groups_file, changed_group)

    def delete_group_from_db(self, group_id):
        """ Deletes Group object from the database

        Keyword arguments:
        group_id -- id of the group to delete from the database
        """

        json_functions.remove_object_from_json_file(self.groups_file, group_id)

    # endregion

    # region User db funcs
    def get_users_from_db(self):
        """ Returns all users that now are stored in the database """

        return json_functions.get_objects_from_json_file(self.users_file)

    def add_user_to_db(self, new_user):
        """ Adds User object to the database

        Keyword arguments:
        new_user -- user to add to the database
        """

        json_functions.add_object_to_json_file(self.users_file, new_user)

    def change_user_in_db(self, changed_user):
        """ Changes User object in the database

        Keyword arguments:
        changed_user -- user to change in the database
        """

        json_functions.change_object_in_json_file(self.users_file, changed_user)

    def delete_user_from_db(self, user_id):
        """ Deletes User object from the database

        Keyword arguments:
        user_id -- id of the user to delete from the database
        """

        json_functions.remove_object_from_json_file(self.users_file, user_id)

    # endregion
