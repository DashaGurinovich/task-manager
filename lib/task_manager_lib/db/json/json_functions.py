"""This module has functions to perform CRUD actions with objects in json-files"""

import os.path

import jsonpickle


def add_object_to_json_file(file_name, adding_object):
    """ Adds object to the database

    Keyword arguments:

    file_name -- file to add data to
    adding_object -- object to add to the database
    """

    # If file exists, open it,
    # decode data into the container with jsonpickle,
    if os.path.isfile(file_name):
        with open(file_name, 'r') as file:
            container = jsonpickle.decode(file.read())
    else:  # otherwise
        container = []  # container is empty

    with open(file_name, 'w') as file:
        container.append(adding_object)  # Add new object to the container
        file.write(jsonpickle.encode(container))  # Write again to the file


def change_object_in_json_file(file_name, changing_object):
    """ Changes object in the database

    Keyword arguments:

    file_name -- file to change data in
    changing_object -- object to change in the database
    """
    # If file exists,
    # decode data into the container,
    if os.path.isfile(file_name):
        with open(file_name, 'r') as file:
            container = jsonpickle.decode(file.read())
    # else return
    else:
        return

    # Find object in the container
    found_object = False
    for index in range(len(container)):
        if container[index].id == changing_object.id:
            # If found, change it
            container[index] = changing_object
            found_object = True

    # If object was found, rewrite the file
    if found_object:
        with open(file_name, 'w') as file:
            file.write(jsonpickle.encode(container))


def remove_object_from_json_file(file_name, object_id):
    """ Removes object from the database

    Keyword arguments:

    file_name -- file to delete data from
    object_id -- id of the object to delete from the database
    """
    # If file exists,
    # decode data into the container,
    if os.path.isfile(file_name):
        with open(file_name, 'r') as file:
            container = jsonpickle.decode(file.read())
    # else return
    else:
        return

    # Find object in the container
    found_object = False
    for element in container:
        if element.id == object_id:
            # If found, remove it
            container.remove(element)
            found_object = True

    # If object was found, rewrite the file
    if found_object:
        with open(file_name, 'w') as file:
            file.write(jsonpickle.encode(container))


def get_objects_from_json_file(file_name):
    """ Returns all objects from the database

    Keyword arguments:

    file_name -- file to get data from
    """
    # If file exists,
    # decode data into the container and return it
    if os.path.isfile(file_name):
        with open(file_name, 'r') as file:
            return jsonpickle.decode(file.read())
    else:
        return []
