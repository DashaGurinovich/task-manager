"""This module includes the decorator that writes debug and error logs, and the function
 that returns the app logger"""

import logging
from functools import wraps

from task_manager_lib import exceptions

LIB_NAME = 'taskManager'


def get_logger():
    """Returns the task manager logger"""

    # Get logger
    logger = logging.getLogger(LIB_NAME)
    return logger


def disable_logger():
    """Disables the app logger"""
    logging.getLogger(LIB_NAME).disabled = True


def enable_logger():
    """Enables the app logger"""
    logging.getLogger(LIB_NAME).disabled = False


def write_log(func):
    """Decorator that writes when the func entered and exited,
     writes the exceptions to the app logger

    Keyword arguments:
    func -- function to be decorated
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        logger = logging.getLogger('taskManager')

        logger.debug("Entered the {} function".format(func.__name__))

        try:
            result = func(*args, **kwargs)

        except exceptions.BlockingException as e:
            if not e.is_blocked:
                # Write the exception in the logger if it's not blocked
                logger.error(e)

                # Block the exception, if the logger writes it for the first time
                e.is_blocked = True

            logger.debug("The {} function exited after the error".format(func.__name__))
            raise

        except exceptions.SettingsNotFoundError as e:
            logger.critical(e)
            logger.debug("The {} function exited after the error".format(func.__name__))
            raise

        except Exception as e:
            logger.error(e)
            logger.debug("The {} function exited after the error".format(func.__name__))
            raise

        logger.debug("Finished the execution of the {} function".format(func.__name__))
        return result

    return wrapper
