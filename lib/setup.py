from setuptools import setup
from setuptools import find_packages


with open("README.md", "r") as fh:
    long_description = fh.read()


setup(
    name='Task_manager_lib',
    version='1.6',
    description='The Tasks library. Created to help managing the tasks',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='Darya Gurinovich',
    url='https://bitbucket.org/DashaGurinovich/task-manager',
    packages=find_packages(),
    install_requires=["jsonpickle", 'python-dateutil'],
    test_suite="task_manager_lib.tests.test_runner.get_test_suite",
    include_package_data=True
)
