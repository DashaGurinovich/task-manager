#Tasks Manager

## Description
This app is created to help managing the tasks. You can create, update, complete the tasks, union them into the groups.
 You can even create and complete the tasks together with your friends. Have fun exploring it :)

## How to install?
####From the bitbucket.org
+ The project requires jsonpickle and python-dateutil. To use the app install jsonpickle and python-dateutil:
~~~
user$ pip3 install jsonpickle
user$ pip3 install python-dateutil
~~~
+ Clone the project from the git repository:
~~~
user$ git clone https://username@bitbucket.org/DashaGurinovich/task-manager.git
~~~
+ Go to the cloned project folder 'task-manager':
~~~
user$ cd task-manager
~~~
+ And install the app:
~~~
user$ python3 setup.py install
~~~

## How to use?

###Data Storage
The data will be stored in the storage. You can use the default json-storage or you can pass your storage.

####Default storage
If you want to use the default json-storage, the only thing you need to do is to pass the path where to store 
the data.

For example:
~~~
>>> import os
>>> import task_manager_lib.func.task
>>> import task_manager_lib.func.user

>>> path = os.path.expanduser('~/.TaskManager/data')

>>> user_manager = task_manager_lib.func.user.UserManager(storage_path=path)
>>> user = user_manager.create_user('login', 'email')

>>> task_manager = task_manager_lib.func.task.TaskManager(storage_path=path)
>>> task = task_manager.create_task('New task', user.id, priority=2)
~~~ 

####Custom storage
You need to pass DataManager object to the entities managers to save data. The DataManager object should get the 
storage that implements such methods as clear(), get_object_from_db(), add_object_to_db(new_object), 
change_object_in_db(changed_object), delete_object_from_db(object_id) where object is group, task and user. 
Otherwise the AttributeError will be raised while trying to perform some managers functions. 
You can use JsonDataManager that implements all these methods from 'task_manager_lib.db.json.manager'. 

For example:
~~~
>>> import os
>>> import task_manager_lib.db
>>> import task_manager_lib.db.json
>>> import task_manager_lib.func.task
>>> import task_manager_lib.func.user

>>> path = os.path.expanduser('~/.TaskManager/data')
>>> json_storage = task_manager_lib.db.json.manager.JsonDataManager(path)
>>> storage = task_manager_lib.db.manager.DataManager(json_storage)

>>> user_manager = task_manager_lib.func.user.UserManager(storage=storage)
>>> user = user_manager.create_user('login', 'email')

>>> task_manager = task_manager_lib.func.task.TaskManager(storage=storage)
>>> task = task_manager.create_task('New task', user.id, priority=2)
~~~ 

###The logger
The functions to work with the logger are in 'task_manager_lib.logger' module. If you want to use the logger, you need to 
initialize it at first. The logger name is 'taskManager'. You can configure it with the help of 'logging' module.

For example:
~~~
>>> import logging
>>> import os
>>> import task_manager_lib.logger

>>> logger = task_manager_lib.logger.get_logger()
>>> logger.setLevel(logging.INFO)

>>> console_handler = logging.StreamHandler()
>>> console_handler.setLevel(logging.INFO)

>>> logger.addHandler(console_handler)
~~~ 
After the logger initialization you can feel free to use the logger.

###The Usage
You can import this library with import statement in any python environment:
~~~
>>> import task_manager_lib
>>> import task_manager_lib.func.task
>>> import task_manager_lib.func.user

>>> path = os.path.expanduser('~/.TaskManager/data')

>>> user_manager = task_manager_lib.func.user.UserManager(storage_path=path)
>>> user = user_manager.create_user("Login", "email@sample.com")

>>> task_manager = task_manager_lib.func.task.TaskManager(storage_path=path)
>>> task = task_manager.create_task("Task", user.id)
~~~

## How to test it?
If you want to run tests, go to the cloned project folder 'task-manager':
~~~
user$ cd task-manager
~~~ 
and use the following statement:
~~~
user$ python3 setup.py test
~~~

####Author: Darya Gurinovich
####Version: 1.6